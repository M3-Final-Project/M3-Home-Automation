#!/bin/bash

configfile='/etc/m3config'
source "$configfile"

echo "M3-Home Bootup Skript started!"

## Bluetooth
if [ "$bluetooth" = "1" ]; then
	echo "" && echo "Starting Bluetooth..."
	modprobe btusb
fi

if [ "$server" = "1" ]; then
	echo "" && echo "Starting Server..."
	systemctl start lighttpd
	systemctl start mosquitto
	sleep 3
	cd /www/pages/m3server
	/www/pages/m3server/eventMngr &
	node /www/pages/m3server/server_api.js $ip $port
fi
