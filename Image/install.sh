#!/bin/bash

cd `dirname $0`
cd ..
FOLDER=$(pwd)
echo "Your git folder is at:"
echo $FOLDER

# Configure timezone
echo -e "\nConfigure Timezone"
cp /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Client
echo -e "\nInstalling Client-API"
cd $FOLDER
cd API/Client-API
cp lib* /usr/lib/
cp Client-API /usr/bin/
chmod +x /usr/bin/Client-API

# mosquitto
echo -e "\nInstalling mosquitto"
cd $FOLDER
cd API/mosquitto-1.4.2
make all
make install
cp lib/libmosquitto.so.1 /usr/lib/libmosquitto.so.1

# WebApp
echo -e "\nInstalling WebApp"
rm -rf /www/pages/*
cd $FOLDER
cd Frontend/WebApp
cp -r * /www/pages/
mkdir /www/pages/m3server

# Copy m3server files
echo -e "\nInstalling m3server files"
cd $FOLDER
cd Image
cp -r m3server/* /www/pages/m3server/

# Server
echo -e "\nInstalling server"
cd $FOLDER
cd API/Server
g++ eventMngr.cpp -o eventMngr
cp server_api.js /www/pages/m3server/
cp eventMngr /www/pages/m3server/

# Coffee
echo -e "\nInstalling python-twitter"
cd /tmp
git clone git://github.com/bear/python-twitter.git
cd python-twitter
sed -i 's/sudo//g' Makefile
make env
pip install python-twitter
cd ..
rm -r python-twitter

# SDO
echo -e "\nInstalling btusb kernel module"
cd $FOLDER
cd Image
opkg install kernel-module-btusb_3.8-r0_clanton.ipk
modprobe btusb

#Service
echo -e "\nInstalling service"
systemctl disable lighttpd
systemctl disable mosquitto

cd $FOLDER
cd Image/Service
cp bootup.sh /usr/bin/m3bootup.sh
cp config /etc/m3config
cp m3home.service /lib/systemd/system/
cd /etc/systemd/system
ln -s /lib/systemd/system/m3home.service m3home.service
systemctl daemon-reload
systemctl enable m3home.service
