#!/bin/bash

cd `dirname $0`
cd ..
FOLDER=$(pwd)
echo "Your git folder is at:"
echo $FOLDER

# Stop m3home
echo -e "\nShutting down m3home.service"
systemctl stop m3home.service

# Client
echo -e "\nInstalling Client-API"
cd $FOLDER
cd API/Client-API
cp lib* /usr/lib/
cp Client-API /usr/bin/
chmod +x /usr/bin/Client-API

# WebApp
echo -e "\nInstalling WebApp"
cp -r /www/pages/m3server /tmp
rm -rf /www/pages/*
cd $FOLDER
cd Frontend/WebApp
cp -r * /www/pages/
mv /tmp/m3server /www/pages/

# Copy m3server files
echo -e "\nInstalling m3server files"
cd $FOLDER
cd Image
cp -r m3server/* /www/pages/m3server/

# Server
echo -e "\nInstalling server"
cd $FOLDER
cd API/Server
g++ eventMngr.cpp -o eventMngr
cp server_api.js /www/pages/m3server/
cp eventMngr /www/pages/m3server/

# Start m3home service
echo -e "\nStarting m3home.service"
systemctl start m3home.service
