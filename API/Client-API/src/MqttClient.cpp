//============================================================================
// Name        : MqttClient.cpp
// Author      : Christian Geldermann
// Version     :
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <mosquittopp.h>
extern "C"
{
#include <sys/socket.h>
#include <sys/eventfd.h>
#include <netinet/in.h>
#include <strings.h>
#include <unistd.h>
#include <cstring>
}
#include <memory>
#include <thread>
#include <mutex>

// Define program parameters
//#define ID "test"
//#define HOST "192.168.1.79"
#define MQTT_PORT 1883
#define KEEP_ALIVE 3600
#define READ_BUFFER_SIZE 1024
#define LISTEN_PORT 1884

//#define DBG

using namespace std;

class MqttClient final : public mosquittopp::mosquittopp
{
private:
	static uint64_t instanceCount;
	bool listenSketchConn = true;
	int sketchConnSock = 0;
	int sketchDataSock = -1;
	int eventSock = 0;
	sockaddr_in sketchConnAddr;

	std::mutex sketchDataSockMut;
	std::thread * sketchListenThread = 0;
public:
	MqttClient(std::string id, std::string host);
	virtual ~MqttClient();

	void ListenSketchConnect();
	void ListenSketchData();
	void SendToSketch(std::string data);
	void MessageReceiveCB(std::string received);

	//virtual void on_connect(int rc);
	//virtual void on_disconnect();
	//virtual void on_publish(uint16_t mid);
	virtual void on_message(const struct mosquitto_message *message);
	//virtual void on_subscribe(uint16_t mid, int qos_count, const uint8_t *granted_qos);
	//virtual void on_unsubscribe(uint16_t mid);
	virtual void on_error() {cout << "ERROR" << endl;}
};

// Init counter for number of class instances
uint64_t MqttClient::instanceCount = 0;

MqttClient::MqttClient(std::string id, std::string host) : mosquittopp(id.c_str())
{
	// Initialize library if neccessary
	if(instanceCount <= 0)
	{
		lib_init();
	}

	// Connect to server
	connect(host.c_str(), MQTT_PORT, KEEP_ALIVE);

	int sockoptData = 1;

	// Create socket for connections from sketch
	sketchConnSock = ::socket(AF_INET, (int)SOCK_STREAM, 0);
	setsockopt(sketchConnSock, SOL_SOCKET, SO_REUSEADDR, &sockoptData, sizeof(int));

	if(sketchConnSock < 0)
	{
		cout << "Error opening socket for listening for sketch connection requests" << endl;
		exit(1);
	}
	else
	{
		bzero(&sketchConnAddr, sizeof(sketchConnAddr));
		sketchConnAddr.sin_family = AF_INET;
		sketchConnAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		sketchConnAddr.sin_port = htons(LISTEN_PORT);

		if(!bind(sketchConnSock, &((sockaddr &) sketchConnAddr), sizeof(sketchConnAddr)))
		{
			if(listen(sketchConnSock, 1) != 0)
			{
				cout << "Error starting listening for connection requests from sketch" << endl;
				exit(1);
			}
		}
		else
		{
			cout << "Error binding socket for listening for sketch connection requests" << endl;
			exit(1);
		}
	}

	// Increase counter for number of class instances
	++instanceCount;

	// Init event socket
	eventSock = eventfd(0, EFD_NONBLOCK);

	// Start listening
	sketchListenThread = new std::thread(&MqttClient::ListenSketchConnect, this);
	//ListenSketchConnect();
}

MqttClient::~MqttClient()
{
	// Decrease counter for number of class instances
	--instanceCount;

	// Clean up library if neccessary
	if(instanceCount > 0)
	{
		lib_cleanup();
	}

	listenSketchConn = false;
	eventfd_write(eventSock, (eventfd_t) 1);

	sketchListenThread->join();
	delete sketchListenThread;
}

void MqttClient::ListenSketchConnect()
{
#ifdef DBG
	cout << "Listening for sketch connecitons ..." << endl;
#endif

	// Decl. select variables
	int res;
	fd_set readset;

	while(listenSketchConn)
	{
		// Init select
		FD_ZERO(&readset);
		FD_SET(sketchConnSock, &readset);
		FD_SET(eventSock, &readset);

		res = select(sketchConnSock + 1, &readset, NULL, NULL, NULL);

		if(res > 0)
		{
			if(FD_ISSET(sketchConnSock, &readset))
			{
				{
					std::unique_lock<std::mutex> lock(sketchDataSockMut);

					sketchDataSock = accept(sketchConnSock, (sockaddr *) NULL, NULL);
				}

				SendToSketch("Hallo Sketch");
				ListenSketchData();
			}

			if(FD_ISSET(eventSock, &readset))
			{
				eventfd_t d;
				eventfd_read(eventSock, &d);
			}
		}
		else cout << "Didn't select anything from sketch but should" << endl;
	}
}

void MqttClient::ListenSketchData()
{
#ifdef DBG
	cout << "Listening for thread data" << endl;
#endif

	// Decls / Inits
	char * buf = new char[READ_BUFFER_SIZE];
	bool fdValid = true;

	std::string received;

	while(fdValid)
	{
		int fd = 0;

		// Check whether socket is valid
		{
			std::unique_lock<std::mutex> lock(sketchDataSockMut);

			if(sketchDataSock < 0)
			{
				cout << "Data socket invalid while receiving" << endl;
				return;
			}

			fd = sketchDataSock;
		}

		// Read whole message
		while(true)
		{
#ifdef DBG
			cout << "receiving from sketch ..." << endl;
#endif
			size_t const recBytes = recv(fd, buf, READ_BUFFER_SIZE, 0);

			if(recBytes > 0)
			{
				received.append(buf, recBytes);

				if(recBytes < READ_BUFFER_SIZE) break;
			}
			else if(recBytes < 0)
			{
				if(errno != EAGAIN) cout << "Error while reading data transferred by sketch" << endl;
				received.clear();
				break;
			}
			else
			{
				cout << "Sketch disconnected" << endl;
				fdValid = false;
				break;
			}
		}

		// If any data received ...
		if(!received.empty())
		{
			while(received.length() > sizeof(uint32_t))
			{
				uint32_t size;

				memcpy(&size, received.c_str(), sizeof(uint32_t));

				// If message complete ...
				if(received.length() >= size + sizeof(uint32_t))
				{
					// Extract message
					std::string msg = received.substr(sizeof(uint32_t), size);

					// Call callback
					MessageReceiveCB(msg);

					// Remove message from received
					received = received.substr(size + sizeof(uint32_t));
				}
				else break;
			}
		}
		else if(fdValid) cout << "Empty message received from sketch" << endl;
	}

#ifdef DBG
	cout << "End listening for thread data" << endl;
#endif
}

void MqttClient::MessageReceiveCB(std::string received)
{
	// Process received data
	if(received[0] == 'p')
	{

		// Determine position of first space char
		size_t spacePos = received.find(' ');

		if(spacePos != std::string::npos)
		{
			// Extract channel and message parts
			std::string topic = received.substr(1, spacePos - 1);
			std::string message = received.substr(spacePos + 1);

#ifdef DBG
			cout << "publishing ... " << topic << "; " << message << endl;
#endif

			// Puclish message to channel
			publish(NULL, topic.c_str(), (uint32_t) message.length(), (uint8_t *) message.c_str(), 1, false);
		}
		else cout << "Received publish command corrupted" << endl;
	}
	else if(received[0] == 's')
	{
		// Check whether subscription channel is defined ...
		if(received.length() > 1)
		{
			received = received.substr(1);

#ifdef DBG
			cout << "Subscribing: " << received << ";" <<endl;
#endif

			// Subscribe channel
			subscribe(NULL, received.c_str(), 1);
		}
		else cout << "Channel for subscription undefined" << endl;
	}
	else cout << "Received data unprocessable" << endl;
}

void MqttClient::SendToSketch(std::string data)
{
	int fd = 0;

	{
		std::unique_lock<std::mutex> lock(sketchDataSockMut);

		fd = sketchDataSock;

		if(sketchDataSock < 0)
		{
			cout << "Data socket invalid while sending" << endl;

			return;
		}
	}

	uint32_t const size = data.length();
	data = std::string((char const *) &size, sizeof(size)) + data;

	send(fd, data.c_str(), data.length(), 0);
}

void MqttClient::on_message(mosquitto_message const * message)
{
	std::string const msg = std::string("m") + message->topic + " " + std::string((char const *) message->payload, message->payloadlen);

#ifdef DBG
	cout << "Received from broker: " << msg << endl;
#endif

	SendToSketch(msg);
}

int main(int argc, const char* argv[])
{
	std::string host;
	std::string id;

	if(argc > 2)
	{
		id = std::string(argv[2]);
	}
	else
	{
		time_t t;
		time(&t);

		srand((unsigned int) t);

		char id_str[11] = {'\0'};

		for(int i = 0; i < 10; ++i)
		{
			id_str[i] = (char) rand() % 52 + 48;

			if(id_str[i] > 57) id_str[i] += 8;

			if(id_str[i] > 90) id_str[i] += 7;
		}

		id = string(id_str);
	}

	if(argc > 1)
	{
		host = std::string(argv[1]);
	}
	else
	{
		cout << "Too few arguments" << endl;
		exit(1);
	}

	MqttClient client(id, host);

	while (true) client.loop(-1);

	return 0;
}
