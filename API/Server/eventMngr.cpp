#include <fstream>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <cstring>
#include <unistd.h>
#include <time.h>
#include <map>
#include <cassert>

using namespace std;

vector<string> events;
vector<string> divider;
vector<string> conditions;
vector<string> actions;
bool doorClosed=0;
map<string, bool> m;

void mapinit(){
	m["s1"]=0;
	m["s2"]=0;
	m["s3"]=0;
	m["a1"]=0;
}

//Vector split
vector<string> &split(const string &s, char delim, vector<string> &elems) {		//string parsen
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

//Super split
void sSplit (vector<string> &vec){
	int cndL = vec.size();
	for(int j = 0; j < cndL; j++){
		if(strchr(vec[j].c_str(),'>')){
			split (vec[j], '>', vec);
			vec.push_back(">");
		}
		else if(strchr(vec[j].c_str(),'<')){
			split (vec[j], '<', vec);
			vec.push_back("<");
		}
		else if(strchr(vec[j].c_str(),'=')){
			split (vec[j], '=', vec);
			vec.push_back("=");
		}
	}
	vec.erase (vec.begin(),vec.begin()+cndL);
}

//Converting URLs
string convert(string str){
		if(str == "coffee") return "coffee/make_coffee.html";
		else if(str == "volume") return "sensor/volume.html";
		else if(str == "brightness") return "sensor/brightness.html"; 
		else if(str == "temperature") return "sensor/temperature.html"; 
		else if(str == "door") return "sdo/state.html";
		else if(str == "smartilator") return "smartilator/power.html";
		else if(str == "light") return "ambilight/color.html";
		else if(str == "time") return "time";
		cout << "Error in rule" << endl;
		return "";
}

//executing commands
void doActions(){
	for(int i = 0; i < actions.size();i+=3){
		string subcommand= convert(actions[i]);
		string command = "mosquitto_pub -t "+subcommand.substr(0,subcommand.size()-5)+" -m "+actions[i+1];
		//cout << command << endl;
		system(command.c_str());
	}
}

//checking conditions
bool check (int j){
	//const char *x = conditions[j].c_str();
	//ifstream condi(x);
	string conCond=convert(conditions[j]);
	if(conCond!=""){
		if (conCond == "time"){
			time_t now = time(0);
			char *date = ctime(&now);
			string sdate(date);
			string zdate =sdate.substr(11,sdate.size());
			zdate = zdate.substr(0,5);
			string stime="";
			for(int i= 0; i<zdate.size();i++){
				if(zdate[i] != ':' )
				stime+=zdate[i];
			}
			if(stime == conditions[j+1]){
				return 1;
			}
		}
		else{
			ifstream condi(convert(conditions[j]).c_str());
			string cond;
			getline (condi, cond);
			if(conditions[j+2] == "<"){
				if(cond < conditions[j+1]){
					return 1;
				}
			}
			else if(conditions[j+2] == ">"){
				if(cond > conditions[j+1]){
					return 1;
				}
			}
			else if(conditions[j+2] == "="){
			    if(conditions[j] == "light" && conditions[j+1] == "1" && cond != "000000"){
			        return 1;
			    }
			    else if(conditions[j] == "light" && conditions[j+1] == "0" && cond == "000000"){
			        return 1;
			    }
				if(cond == conditions[j+1]){
					return 1;
				}
			}
		}
	}
	return 0;
}

bool smartiCheck(int i){
	ifstream condi("sensor/temperature.html");
	string cond;
	getline (condi, cond);
	if(divider[i] == "1"){
		if (divider[i+1] <= cond){
			if( m["s"+i] == 0){
				m["s"+i]=1;
				return 1;
			}
		}
		m["s"+i]= 0;
	}
	return 0;
}

void smartiActions(int i){
	string command = "mosquitto_pub -t smartilator/power -m " + divider[i];
	//cout << command << endl;
	system(command.c_str());
}

bool ambiCheck(){
	if(divider[0]!="1" && divider[1] != "1" && divider[4]!="1"){
		return 0;
	}
	bool check1 = false;
	bool check2 = false;
	bool check3 = false;
	if(divider[0] == "1"){
		
		ifstream door("sdo/state.html");
		string cond;
		getline (door, cond);
		if (cond == "1"){
			if( m["a1"] == 0){
				check1 = true;
			}
		}
		else if (cond == "0"){
			m["a1"] = 0;
			return 0;
		}
	}
	else{
		check1= true;
	}
	if(divider[1]== "1"){
		time_t now = time(0);
		char *date = ctime(&now);
		string sdate(date);
		string zdate =sdate.substr(11,sdate.size());
		zdate = zdate.substr(0,5);
		string stime="";
		for(int i= 0; i<zdate.size();i++){
			if(zdate[i] != ':' )
			stime+=zdate[i];
		}
		if(divider[2] <= stime && stime <= divider[3]){
			if (m["a1"] == 0){
				check2 = true;
			}
		}
		else{
			m["a1"] = 0;
			return 0;
		}
	}
	else{
		check2 = true;
	}
	if(divider[4] == "1"){
		ifstream brightness ("sensor/brightness.html");
		string cond1;
		getline (brightness, cond1);
		if(cond1 < divider[5]){
			if(m["a1"]==0){
				check3 = true;
			}
		}
		else{
			m["a1"]=0;
			return 0;
		}
	}
	else{
		check3 = true;
	}
	if(check1 && check2 && check3){
		m["a1"]=1;
		return 1;
	}
	return 0;
}

void ambiActions(){
	string command = "mosquitto_pub -t ambilight/color -m ffffff";
	//cout << command << endl;
	system(command.c_str());
}

void program(){
	ifstream rules("rules.html");
	string line="";
	events.erase (events.begin(),events.end());
	while(getline(rules, line)){		//auslesen der rules.html
		events.push_back(line);
	}
	if(events.size()>0){
		for(int i = 0; i < events.size(); i++){
			split (events[i], '?', divider);
			if(divider.size()>2){
				split (divider[2], '!', conditions);
				if(divider.size()>3){
					if(m.find(divider[0])== m.end()){
						m[divider[0]] = 0;
					}
					split (divider[3], '!', actions);
					sSplit(conditions);
					int j = 0;
					bool activate = true;
					while(conditions.size() > j && activate != false){ //checking Conditions
						activate = check(j);
						j = j+3;
					}
					if (activate){
						if(m[divider[0]] == 0){
							m[divider[0]] = 1;
							sSplit(actions);
							doActions();
						}
					}
					else{
						m[divider[0]] = 0;
					}
				}
				else{
					cout << "Error, only 2 '?' in rule id ";
					cout << divider[0] << endl;
				}
			}
			else{
				cout << "Error, only 1 '?' in rule id ";
				cout << divider[0] << endl;
			}
			divider.erase (divider.begin(),divider.end());
			conditions.erase (conditions.begin(),conditions.end());
			actions.erase (actions.begin(),actions.end());
		}
	}
}

void smartiProgram(){
	events.erase (events.begin(),events.end());
	string line="";
	ifstream rules1("smartilator/system_rule1.html");
	while(getline(rules1, line)){		//auslesen der system_rule1.html
		events.push_back(line);
	}
	ifstream rules2("smartilator/system_rule2.html");
	while(getline(rules2, line)){		//auslesen der system_rule2.html
		events.push_back(line);
	}
	ifstream rules3("smartilator/system_rule3.html");
	while(getline(rules3, line)){		//auslesen der system_rule3.html
		events.push_back(line);
	}
	for(int i = 0; i < events.size(); i++){
		split (events[i], '?', divider);
	}
	int i = 0;
	bool abc [3] = {0,0,0};
	for (int j = 0; j < divider.size(); j+=3){
		abc[i] = smartiCheck(j);
		i++;
	}
	if(abc[0] == 1 && (divider[1] > divider[4] || abc[1] == 0) && (divider[1] > divider[7] || abc[2] == 0)){
		smartiActions(2);
	}
	else if(abc[1] == 1 && (divider[4] > divider[7] || abc[2] == 0)){
		smartiActions(5);
	}
	else if(abc[2]== 1){
		smartiActions(8);
	}
	
	divider.erase (divider.begin(),divider.end());
}

void ambiProgram(){
	//events.erase (events.begin(),events.end());
	events.clear();
	ifstream door("ambilight/system_rule_door.html");
	string line="";
	getline(door, line);		//auslesen der ambilight/system_rule_door.html
	events.push_back(line);
	divider.push_back(line);
	ifstream time("ambilight/system_rule_time.html");
	getline(time, line);	//auslesen der ambilight/system_rule_time.html
	events.push_back(line);
	ifstream brightness("ambilight/system_rule_brightness.html");
	getline(brightness, line);		//auslesen der ambilight/system_rule_brightness.html
	events.push_back(line);
	for(int i = 1; i < events.size(); i++){
		split (events[i], '?', divider);
	}
	bool activate = false;
	activate = ambiCheck();
	if (activate){
		ambiActions();
	}
	divider.erase (divider.begin(),divider.end());
}

int main(){
	mapinit();
	while(1){
		smartiProgram();
		ambiProgram();
		program();
		sleep(2);
	}
}