var http = require('http');
var querystring = require('querystring');
var utils = require('util');
var crypto = require('crypto');
sys = require('sys');
fs = require('fs');
spawn = require('child_process').spawn;
exec = require('child_process').exec;

//creates a write-stream later used to change the content of the rules.html
var file = fs.createWriteStream('rules.html', {
    flags: 'a',
    encoding: 'utf8'
});

//handles error being thrown because of wrong param1 in http request
function posterror (){
    throw new Error('First parameter of http request wrong. Try param1=pub, param1=password or param1=rule');
    }

var count = 6;
//subscribes to all topics of the MQTT-server and writes them in matching .html-files
mqtt_sub = spawn('mosquitto_sub', ['-t', '#', '-v']);
mqtt_sub.stdout.on('data', function(data){
    if(data.toString() == 'coffee/init 1\n'){
	count = 0;
	var coffee_access_token_key = fs.readFileSync('coffee/access_token_key.html');
	access_token_key = exec('mosquitto_pub -t coffee/access_token_key -m "' + coffee_access_token_key.toString().substr(0,coffee_access_token_key.length-1) + '"');
	var coffee_access_token_secret = fs.readFileSync('coffee/access_token_secret.html');
	access_token_secret = exec('mosquitto_pub -t coffee/access_token_secret -m "' + coffee_access_token_secret.toString().substr(0,coffee_access_token_secret.length-1)+ '"');
	var coffee_consumer_key = fs.readFileSync('coffee/consumer_key.html');
	consumer_key = exec('mosquitto_pub -t coffee/consumer_key -m "' + coffee_consumer_key.toString().substr(0,coffee_consumer_key.length-1)+ '"');
	var coffee_consumer_secret = fs.readFileSync('coffee/consumer_secret.html');
	consumer_secret = exec('mosquitto_pub -t coffee/consumer_secret -m "' + coffee_consumer_secret.toString().substr(0,coffee_consumer_secret.length-1)+ '"');
	var coffee_message = fs.readFileSync('coffee/message.html');
	message = exec('mosquitto_pub -t coffee/message -m "' + coffee_message.toString().substr(0,coffee_message.length-1)+ '"');
	var coffee_oauth_key = fs.readFileSync('coffee/oauth_key.html');
	oauth_key = exec('mosquitto_pub -t coffee/oauth_key -m "' + coffee_oauth_key.toString().substr(0,coffee_oauth_key.length-1)+ '"');
    }
    else if(count == 6 ){
	var tmp_string = data.toString().split(' ');
	/*var content = '';
	  for(i=1; i<tmp_string.length-1;i++){
	  content += tmp_string[i] + ' ';
	  }
	  content += tmp_string[tmp_string.length-1];
	  console.log(content);*/
	var tmp_string2 = tmp_string[1].split('\n');
	if(tmp_string[0] != 'coffee/message'){
	    sub_update_html = exec('echo "' + tmp_string2[0] + '" > ' + tmp_string[0] + '.html');
	}
	}
    else if(count < 6){
	count++;
    }
});
/*
function encrypt(text){
  var cipher = crypto.createCipher('aes-256-cbc','d6F3Efeq')
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

var pswd = encrypt(process.argv[4])
exec('echo ' + pswd + ' > password.md5');
*/
//Start the http-Server
http.createServer(function (req, res) {
    //decoding of messages is only supported with http post
    if(req.method == 'GET'){
        var get_file = req.url.substring(1, req.url.length);
	fs.readFile(get_file, function(err,data){
	    try{
		    res.end(data.toString());
		    }
		catch(error){
		    res.end(err.toString());
		    }
	});
    }
    //handler for post-requests
    else if(req.method == 'POST'){
	//get the full message from chunks
	var fullBody ='';

	req.on('data', function(chunk){
	       fullBody += chunk.toString();
	});
	req.on('end', function() {
	   
	    //send confirmation after receiving the message
	    res.writeHead(200, "OK", {'Content-Type': 'text/html'});

	    var decodedBody = querystring.parse(fullBody);

	    //different cases, depending on the first parameter of the http post
	    //publishing topics

	    if(decodedBody.param1 == 'pub'){
		//spawn a child process invoking the command line client for mosquitto
		mqtt_pub = exec('mosquitto_pub -t '+ decodedBody.param2 + ' -m "'+decodedBody.param3 + '"');
		tmp_dir = decodedBody.param2.split('/');
		topic_dir = '';
		for(i = 0; i < tmp_dir.length-1; i++){
		    topic_dir += tmp_dir[i] + '/';
		    }
		topic_dir = exec('mkdir -p ' + topic_dir);
		mqtt_html = exec('echo ' + decodedBody.param3 + ' > ' + decodedBody.param2 + '.html');
	    }
	    
	    //this scetion allows adding and deleting of rules. Editing of rules is archieved by sending a combination of adding- and deleting-requests.
	    else if(decodedBody.param1 == 'rule'){
		if(decodedBody.param2 == 'add'){
		    file.write(decodedBody.param3 + '\n');
		}
		else if(decodedBody.param2 == 'delete'){
		    var rules_file = fs.readFileSync('rules.html').toString().split('\n');
		    var tmp = '';
		    /*search through the lines of the file for the one starting
		      with the matching id, given by param3
		     */
		    for(i = 0; i < rules_file.length; i++){
			single_rule = rules_file[i].split('?');
			if(single_rule[0] != decodedBody.param3 && single_rule[0] != ''){
			    tmp = tmp + rules_file[i] + '\n';
			}
		    }
		    fs.writeFile('rules.html',tmp);
		}
	    }
	    else if(decodedBody.param1 == 'password'){
	    }
	    //throw an error if there is no defined case for the param1 of the http request
	    else {
		try{
		    posterror();
		}
		catch(err){
		    console.log('ERROR:',err);
		}
	    }
	    res.end();
	});
    }
	
//listen for http requests
}).listen(process.argv[3], process.argv[2]);
console.log('Server running at http://' + process.argv[2] + ':' + process.argv[3]);
