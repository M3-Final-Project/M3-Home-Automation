extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>

  // Vom Galileo nicht unterstützt
  //#include <sys/eventfd.h>
}

#include <string>
#include <iostream>
#include <functional>
#include <fstream>
#include <csignal>
// Von der IDE nicht unterstützt
//#include <thread>

#define HOST "localhost"
#define PORT 1884
#define READ_BUFFER_SIZE 1024

#define API_PATH "/usr/bin/Client-API"
#define API_PROC_NAME "Client-API"
#define BROKER_HOST "192.168.1.79"
#define AUTO_START_API
#define KILL_API
#define TEST_NETWORK

using namespace std;

int getProcIdByName(std::string procName)
{
  int pid = -1;

  // Open directory "/porc"
  DIR *dp = opendir("/proc");

  if (dp != 0)
  {
    dirent *dir;

    while (pid < 0 && (dir = readdir(dp)))
    {
      // Skip non numeric directories
      int id = atoi(dir->d_name);
      if (id > 0)
      {
        std::string cmdPath = std::string("/proc/") + dir->d_name + "/cmdline";
        std::ifstream cmdFile(cmdPath.c_str());
        std::string cmdLine;
        getline(cmdFile, cmdLine);

        if (!cmdLine.empty())
        {
          // Keep first cmdline item which contains the program path
          size_t pos = cmdLine.find('\0');

          if (pos != std::string::npos) cmdLine = cmdLine.substr(0, pos);

          if (procName == cmdLine) pid = id;
        }
      }
    }
  }

  closedir(dp);

  return pid;
}

class MqttClient
{
  private:
    int sock = 0;
    int eventSock = 0;
    sockaddr_in serv_addr;
    hostent * server;
    bool listening = true;

    pthread_t listenThread;

    void SendData(std::string data);
    static void * Listen(void * caller);
  public:
    MqttClient();
    ~MqttClient();

    void Publish(std::string topic, std::string message);
    void Subscribe(std::string topic);

    virtual void MessageReceivedCB(std::string topic, std::string message);
};

MqttClient::MqttClient()
{
#ifdef AUTO_START_API
  cout << "Auto start:" << endl;

#ifdef TEST_NETWORK
  {
    // Send a ping to our Broker
    std::string ping("ping -c 1 -W 2 ");
    ping += BROKER_HOST;
    ping += " >/dev/null";

    while (system(ping.c_str()) != 0) {
      cout << "\t Could not reach " << BROKER_HOST << endl;
      cout << "\t Did you plug the Ethernet cable into the device?" << endl;
      sleep(3);
    }
  }
#endif

#ifdef KILL_API
  // Kill API process
  {
    int const apiPid = getProcIdByName(API_PROC_NAME);

    if (apiPid > 0)
    {
      cout << "\tKilling process: " << apiPid << endl;
      kill(apiPid, SIGTERM);
      cout << "\tAPI process killed" << endl;
    }
    else cout << "\tAPI process not found (kill)" << endl;
  }
#endif
  if (getProcIdByName(API_PROC_NAME) < 0)
  {
    // Auto start API
    pid_t pid = fork();

    if (pid == -1) // On error ...
    {
      cout << "\tFork failed" << endl;
      exit(1);
    }
    else if (pid > 0) // For parent process ...
    {
      cout << "\tAPI process id: " << pid << endl;

      delay(500);
    }
    else // For the child process ...
    {
      cout << "\tStarting API" << endl;
      int res = execl(API_PATH, API_PROC_NAME, BROKER_HOST, (char*) 0);

      if (res < 0)
      {
        cout << "\tExec failed with: " << strerror(errno) << endl;
        exit(1);
      }
      else exit(0);
    }
  }
#endif

  // Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);

  if (sock < 0)
  {
    cout << "Socket could not be created" << endl;
    exit(1);
  }

  // Initialize server data
  server = gethostbyname(HOST);

  if (server == NULL)
  {
    cout << "Host could not be resolved" << endl;
    exit(1);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
  serv_addr.sin_port = htons(PORT);

  // Connect to proxy
  if (connect(sock, (sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    cout << "Error while connecting" << endl;
    exit(1);
  }

  // Init event socket (Vom Galileo nicht unterstützt)
  //eventSock = eventfd(0, EFD_NONBLOCK);

  // Start listening thread
  pthread_create(&listenThread, NULL, MqttClient::Listen, this);
}

MqttClient::~MqttClient()
{
  // Disconnect from proxy
  close(sock);

  listening = false;
  pthread_join(listenThread, NULL);
  // vom Galileo nicht unterstützt
  //eventfd_write(eventSock, (eventfd_t) 1);
}

void MqttClient::SendData(std::string data)
{
#ifdef DBG
  Serial.println((std::string("Sent: ") + data).c_str());
#endif

  uint32_t const size = data.length();
  data = std::string((char const *) &size, sizeof(size)) + data;

  send(sock, data.c_str(), data.length(), 0);
}

void MqttClient::Publish(std::string topic, std::string message)
{
  // Construct data to send to proxy
  std::string const data = "p" + topic + " " + message;

  // Send data to proxy
  SendData(data);
}

void MqttClient::Subscribe(std::string topic)
{
  // Constrcuct data to send to proxy
  std::string const data = "s" + topic;

  // Send to proxy
  SendData(data);
}

void * MqttClient::Listen(void * c)
{
  MqttClient * caller = (MqttClient *) c;
  char * const buf = new char[READ_BUFFER_SIZE];
  //int res;
  //fd_set readset;

  while (caller->listening)
  {
    /* Vom Galileo nicht unterstützt
    FD_ZERO(&readset);
    FD_SET(sock, &readset);
    FD_SET(eventSock, &readset);

    res = select(sock + 1, &readset);

    if(res > 0)
    {
      if(FD_ISSET(sock, &readset))
      {
        //TODO Daten lesen
      }

      if(FD_ISSET(eventSock, &readset))
      {
        eventfd_t d;
        eventfd_read(eventSock, &d);
      }
    }
    //*/

    std::string received;

    while (true)
    {
      size_t const recBytes = recv(caller->sock, buf, READ_BUFFER_SIZE, 0);

      if (recBytes > 0)
      {
        received.append(buf, recBytes);

        if (recBytes < READ_BUFFER_SIZE) break;
      }
      else if (recBytes < 0)
      {
        if (errno != EAGAIN) cout << "Error while reading data from socket" << endl;
        received.clear();
        break;
      }
      else
      {
        cout << "Read buffer empty" << endl;
        cout << "API disconnected" << endl;
        break;
      }
    }

#ifdef DBG
    Serial.println("Message received: ");
    Serial.println(received.c_str());
#endif

    // If any data was received ...
    if (!received.empty())
    {
      // While more messages are received ...
      while (received.length() > sizeof(uint32_t))
      {
        uint32_t size;

        memcpy(&size, received.c_str(), sizeof(uint32_t));

        // If message complete ...
        if (received.length() >= size + sizeof(uint32_t))
        {
          std::string msg = received.substr(sizeof(uint32_t), size);

          // Use the message
          if (msg[0] == 'm')
          {
            size_t spacePos = msg.find(' ');

            if (spacePos != std::string::npos)
            {
              // Call callback for received message
              caller->MessageReceivedCB(msg.substr(1, spacePos - 1), msg.substr(spacePos + 1));
            }
            else
            {
              Serial.println("Received message corrupted");
              received.clear();
            }
          }
          /*
          else
          {
            Serial.println("Received data unprocessable");
            Serial.print("Data: ");
            Serial.println(received.c_str());
            received.clear();
          }
          //*/

          received = received.substr(size + sizeof(uint32_t));
        }
        else break;
      }
    }
  }
}

//// !!!!! AB HIER KOMMEN DIE FUNKTIONEN, DIE IHR EDITIEREN SOLLT !!!!!

void MqttClient::MessageReceivedCB(std::string topic, std::string message)
{
  Serial.println(((std::string)"Received: " + message + " for topic: " + topic).c_str());
}

MqttClient mqttClient;

void setup()
{
  Serial.begin(9600);

  Serial.println("Program started");

  mqttClient.Subscribe("test");

  Serial.println("Subscribed");

  mqttClient.Publish("test", "Hallo Welt!");

  Serial.println("Published");
}

void loop()
{
  delay(100);
}
