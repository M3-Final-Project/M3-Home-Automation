extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>

  // Vom Galileo nicht unterstützt
  //#include <sys/eventfd.h>
}

#include <string>
#include <iostream>
#include <functional>
#include <fstream>
#include <csignal>
// Von der IDE nicht unterstützt
//#include <thread>

#define HOST "localhost"
#define PORT 1884
#define READ_BUFFER_SIZE 1024

#define API_PATH "/usr/bin/Client-API"
#define API_PROC_NAME "Client-API"
#define BROKER_HOST "192.168.1.79"
#define AUTO_START_API
#define KILL_API
#define TEST_NETWORK

using namespace std;

int getProcIdByName(std::string procName)
{
  int pid = -1;

  // Open directory "/porc"
  DIR *dp = opendir("/proc");

  if (dp != 0)
  {
    dirent *dir;

    while (pid < 0 && (dir = readdir(dp)))
    {
      // Skip non numeric directories
      int id = atoi(dir->d_name);
      if (id > 0)
      {
        std::string cmdPath = std::string("/proc/") + dir->d_name + "/cmdline";
        std::ifstream cmdFile(cmdPath.c_str());
        std::string cmdLine;
        getline(cmdFile, cmdLine);

        if (!cmdLine.empty())
        {
          // Keep first cmdline item which contains the program path
          size_t pos = cmdLine.find('\0');

          if (pos != std::string::npos) cmdLine = cmdLine.substr(0, pos);

          if (procName == cmdLine) pid = id;
        }
      }
    }
  }

  closedir(dp);

  return pid;
}

class MqttClient
{
  private:
    int sock = 0;
    int eventSock = 0;
    sockaddr_in serv_addr;
    hostent * server;
    bool listening = true;

    pthread_t listenThread;

    void SendData(std::string data);
    static void * Listen(void * caller);
  public:
    MqttClient();
    ~MqttClient();

    void Publish(std::string topic, std::string message);
    void Subscribe(std::string topic);

    virtual void MessageReceivedCB(std::string topic, std::string message);
};

MqttClient::MqttClient()
{
#ifdef AUTO_START_API
  cout << "Auto start:" << endl;

#ifdef TEST_NETWORK
  {
    // Send a ping to our Broker
    std::string ping("ping -c 1 -W 2 ");
    ping += BROKER_HOST;
    ping += " >/dev/null";

    while (system(ping.c_str()) != 0) {
      cout << "\t Could not reach " << BROKER_HOST << endl;
      cout << "\t Did you plug the Ethernet cable into the device?" << endl;
      sleep(3);
    }
  }
#endif

#ifdef KILL_API
  // Kill API process
  {
    int const apiPid = getProcIdByName(API_PROC_NAME);

    if (apiPid > 0)
    {
      cout << "\tKilling process: " << apiPid << endl;
      kill(apiPid, SIGTERM);
      cout << "\tAPI process killed" << endl;
    }
    else cout << "\tAPI process not found (kill)" << endl;
  }
#endif
  if (getProcIdByName(API_PROC_NAME) < 0)
  {
    // Auto start API
    pid_t pid = fork();

    if (pid == -1) // On error ...
    {
      cout << "\tFork failed" << endl;
      exit(1);
    }
    else if (pid > 0) // For parent process ...
    {
      cout << "\tAPI process id: " << pid << endl;

      delay(500);
    }
    else // For the child process ...
    {
      cout << "\tStarting API" << endl;
      int res = execl(API_PATH, API_PROC_NAME, BROKER_HOST, (char*) 0);

      if (res < 0)
      {
        cout << "\tExec failed with: " << strerror(errno) << endl;
        exit(1);
      }
      else exit(0);
    }
  }
#endif

  // Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);

  if (sock < 0)
  {
    cout << "Socket could not be created" << endl;
    exit(1);
  }

  // Initialize server data
  server = gethostbyname(HOST);

  if (server == NULL)
  {
    cout << "Host could not be resolved" << endl;
    exit(1);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
  serv_addr.sin_port = htons(PORT);

  // Connect to proxy
  if (connect(sock, (sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    cout << "Error while connecting" << endl;
    exit(1);
  }

  // Init event socket (Vom Galileo nicht unterstützt)
  //eventSock = eventfd(0, EFD_NONBLOCK);

  // Start listening thread
  pthread_create(&listenThread, NULL, MqttClient::Listen, this);
}

MqttClient::~MqttClient()
{
  // Disconnect from proxy
  close(sock);

  listening = false;
  pthread_join(listenThread, NULL);
  // vom Galileo nicht unterstützt
  //eventfd_write(eventSock, (eventfd_t) 1);
}

void MqttClient::SendData(std::string data)
{
#ifdef DBG
  Serial.println((std::string("Sent: ") + data).c_str());
#endif

  uint32_t const size = data.length();
  data = std::string((char const *) &size, sizeof(size)) + data;

  send(sock, data.c_str(), data.length(), 0);
}

void MqttClient::Publish(std::string topic, std::string message)
{
  // Construct data to send to proxy
  std::string const data = "p" + topic + " " + message;

  // Send data to proxy
  SendData(data);
}

void MqttClient::Subscribe(std::string topic)
{
  // Constrcuct data to send to proxy
  std::string const data = "s" + topic;

  // Send to proxy
  SendData(data);
}

void * MqttClient::Listen(void * c)
{
  MqttClient * caller = (MqttClient *) c;
  char * const buf = new char[READ_BUFFER_SIZE];
  //int res;
  //fd_set readset;

  while (caller->listening)
  {
    /* Vom Galileo nicht unterstützt
    FD_ZERO(&readset);
    FD_SET(sock, &readset);
    FD_SET(eventSock, &readset);

    res = select(sock + 1, &readset);

    if(res > 0)
    {
      if(FD_ISSET(sock, &readset))
      {
        //TODO Daten lesen
      }

      if(FD_ISSET(eventSock, &readset))
      {
        eventfd_t d;
        eventfd_read(eventSock, &d);
      }
    }
    //*/

    std::string received;

    while (true)
    {
      size_t const recBytes = recv(caller->sock, buf, READ_BUFFER_SIZE, 0);

      if (recBytes > 0)
      {
        received.append(buf, recBytes);

        if (recBytes < READ_BUFFER_SIZE) break;
      }
      else if (recBytes < 0)
      {
        if (errno != EAGAIN) cout << "Error while reading data from socket" << endl;
        received.clear();
        break;
      }
      else
      {
        cout << "Read buffer empty" << endl;
        cout << "API disconnected" << endl;
        break;
      }
    }

#ifdef DBG
    Serial.println("Message received: ");
    Serial.println(received.c_str());
#endif

    // If any data was received ...
    if (!received.empty())
    {
      // While more messages are received ...
      while (received.length() > sizeof(uint32_t))
      {
        uint32_t size;

        memcpy(&size, received.c_str(), sizeof(uint32_t));

        // If message complete ...
        if (received.length() >= size + sizeof(uint32_t))
        {
          std::string msg = received.substr(sizeof(uint32_t), size);

          // Use the message
          if (msg[0] == 'm')
          {
            size_t spacePos = msg.find(' ');

            if (spacePos != std::string::npos)
            {
              // Call callback for received message
              caller->MessageReceivedCB(msg.substr(1, spacePos - 1), msg.substr(spacePos + 1));
            }
            else
            {
              Serial.println("Received message corrupted");
              received.clear();
            }
          }
          /*
          else
          {
            Serial.println("Received data unprocessable");
            Serial.print("Data: ");
            Serial.println(received.c_str());
            received.clear();
          }
          //*/

          received = received.substr(size + sizeof(uint32_t));
        }
        else break;
      }
    }
  }
}

//// !!!!! AB HIER KOMMEN DIE FUNKTIONEN, DIE IHR EDITIEREN SOLLT !!!!!

#include <sstream>

#define LOOP_TIMEOUT_MS 100
#define MEASUREMENT_REPEATS 3

enum sensors
{
  SNESORS_TEMPERATURE = 0,
  SENSIRS_BRIGHTNESS,
  SENSORS_VOLUME,
  SENSORS_COUNT
};

////  Headers  ////
float getTemp();
float getBrightness();
float getVolume();

////  Declarations  ////
// Sensor information
uint32_t interval[SENSORS_COUNT] = {2000, 2000, 2000};
suseconds_t lastUpdate[SENSORS_COUNT] = {0, 0, 0};
float (* sensorGet[SENSORS_COUNT])() = {getTemp, getBrightness, getVolume};
std::string topics[SENSORS_COUNT] = {"sensor/temperature", "sensor/brightness", "sensor/volume"};
uint32_t numGets[SENSORS_COUNT] = {0, 0, 0};
float sumGets[SENSORS_COUNT] = {0, 0, 0};

time_t nextUpdate;
pthread_mutex_t mut;

void MqttClient::MessageReceivedCB(std::string topic, std::string message)
{
  //Serial.println(((std::string)"Received: " + message + " for topic: " + topic).c_str());
}

uint64_t getUSec()
{
  return millis();//t.tv_usec + 1000 * t.tv_sec;
}

MqttClient mqttClient;

float measureVoltage(int pin)
{
  uint32_t result = 0;
  
  for(unsigned int i = 0; i < MEASUREMENT_REPEATS; ++i)
  {
    result += analogRead(pin);
  }
  
  return result / MEASUREMENT_REPEATS;
}

// Metering of sensor values
float getTemp()
{
  float t = 1024.0 - measureVoltage(A1);
  //return t;

  if(t <= 493)
  {
    // Calculate temperature
    float d = (493 - t) / 5 + 22.5;
    
    // Round in 0.5 distance
    return roundf(d * 2) / 2;
  }
  else if(t <= 500) return 23.0;
  else if(t <= 507) return 23.5;
  else if(t <= 512) return 24.0;
  else
  {
    // Calculate temperature
    float d = (t - 513) / 5 + 24.5;
    
    // Round in 0.5 distance
    return roundf(d * 2) / 2;
  }
}

float getBrightness()
{
  float U2 = measureVoltage(A0) * (5.0 / 1024);
  float R2 = 47 * (U2/(5.0 - U2));
  
  return 1/R2 * 200;
}

float getVolume()
{
  return rand() % 30 + 35;
}

// Main
void setup()
{
  pthread_mutex_init(&mut, NULL);
  
  Serial.begin(9600);
  
  Serial.println("Program started");
  
  uint64_t t = getUSec();
  
  pthread_mutex_lock(&mut);
  
  // Init lastUpdate[]
  for(int i = 0; i < SENSORS_COUNT; ++i)
  {
    lastUpdate[i] = t;
  }
  
  // Init random number generator
  time_t t2;
  time(&t2);
  srand((unsigned int) t2);
  
  pthread_mutex_unlock(&mut);
}

int setUpTime = 0;

void loop()
{
  if(setUpTime < 10) delay(100);
  
  // Current timestamp
  uint64_t now = getUSec();
  //time_t now = time(NULL);
  
  
  // Lock mutex
  pthread_mutex_lock(&mut);
  
  Serial.println("...");
  
  // For every sensor ...
  for(int i = 0; i < SENSORS_COUNT; ++i)
  {
    // If sensor active ...
    if(interval[i] > 0)
    {
      // Determine one more sensor value
      ++numGets[i];
      sumGets[i] += sensorGet[i]();
      
      // If sensor values should be sent ...
      if(lastUpdate[i] + interval[i] < now)
      {
        // Calculate average sensor value
        float avgValue = roundf(sumGets[i] / numGets[i]);
        
        // Convert avg. sensor value to readable string
        stringstream converter;
        converter << avgValue;
        
        // Publish avg. value string to mqtt
        mqttClient.Publish(topics[i], converter.str());
        
        Serial.print("Published ");
        Serial.println(converter.str().c_str());
        
        // Reset sensor value
        numGets[i] = 0;
        sumGets[i] = 0;
        
        lastUpdate[i] = now;
        
        delay(50);
      }
    }
  }
  
  // Unlock mutex
  pthread_mutex_unlock(&mut);
  
  delay(500);
}

