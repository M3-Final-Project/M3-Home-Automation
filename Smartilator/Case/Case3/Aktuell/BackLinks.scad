// translate([177.3,95,122])rotate([90,180,0]) color("red") scale([25.4,25.4,25.4]) import("Galileo_2.stl", convexity=3);
//vierteilen

rotate([90,0,0])translate([55,50,-110])aufstecker();
 
difference(){
        kiste();
    for ( i = [0 : 11] ){
        translate(v = [9.375+i*18.75, 0, 5])Schlitz(55,5); translate(v = [9.375+i*18.75, 0, 65])Schlitz(55,5);
        translate(v = [18.75+i*18.75, 0, 5])Schlitz(25,5); translate(v = [18.75+i*18.75, 0, 35])Schlitz(55,5);translate(v = [18.75+i*18.75, 0, 95])Schlitz(25,5);
        
        }
        
      translate(v = [235, 0, 5])Schlitz(55,5); translate(v = [235, 0, 65])Schlitz(55,5);
        
    }


    translate(v = [5, 67, 15])verschlussFEM();
    //translate(v = [5, 67, 15])verschlussMALE();
    translate(v = [5, 67, 90])verschlussFEM();
    //translate(v = [5, 67, 90])verschlussMALE();
    
module verschlussFEM(){
    difference(){
        cube([5,15,25]);
        translate(v = [0, 0, 10])cube([5,10,10]);
        translate(v = [0, 5, 5])cube([5,5,10]);
    }
}
module verschlussMALE(){

        translate(v = [0, -5, 10])cube([5,15,5]);
        translate(v = [0, 5, 5])cube([5,5,10]);
    
}


module kiste(){
    //abgerundet
    difference (){
        cube([250,110,130], false);
        rotate([180,0,0]) rundung(250,5);
        translate(v = [0, 0, 130]) rotate([90,0,0]) rundung(250,5);
        translate(v = [0, 0, 130]) rotate([90,90,270]) rundung(250,5);
        translate(v = [250, 0, 130]) rotate([90,90,0]) rundung(250,5);
        rotate([270,0,90]) rundung(250,5);
        translate(v = [250, 0, 130]) rotate([90,0,90]) rundung(250,5);
        translate(v = [250, 0, 0]) rotate([180,0,90]) rundung(250,5);
        translate(v = [0, 0, 130]) rotate([0,0,90]) rundung(250,5);
        translate(v = [250, 110, 0]) rotate([0,180,0]) rundung(250,5);
        translate(v = [0, 110, 130]) rotate([0,90,90]) rundung(250,5);
        translate(v = [250, 110, 130]) rotate([90,90,90]) rundung(250,5);
        translate(v = [250, 110, 130]) rotate([90,0,180]) rundung(250,5);
        translate(v = [5, 5, 5]) cube([240,100,120], false);
        translate(v=[40,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[40,85,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[75,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[75,85,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[110,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[110,85,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[145,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[145,85,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[180,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[180,85,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[215,50,-1]) cylinder(h = 7, d=30, $fn=30);
        translate(v=[215,85,-1]) cylinder(h = 7, d=30, $fn=30);        
        translate(v=[15,35,-1]) cylinder(h = 7, d=7, $fn=30); //schrauben
        translate(v=[235,35,-1]) cylinder(h = 7, d=7, $fn=30);
        translate(v=[-1,-1,-1])cube([252,68,141]);
        translate(v=[129,-1,-1])cube([130,120,140]);
        //translate(v=[-1,-1,-1])cube([130,120,140]);
        }
}
module rundung(lange, radius) {
    translate(v = [-1, -radius, -radius]) difference (){
        cube([lange+2,radius*2,radius*2], false);
        translate(v = [-2, 0, 0]) rotate([0,90,0]) cylinder(h = lange+4, d=radius*2, $fn=30); 
    }
}

module Schlitz(lange, breite) {
     translate(v = [0, -1, 0])union (){
        translate(v = [0, 0, breite])cube([breite, 7, lange-breite], false);
        translate(v = [breite/2, 0, breite]) rotate([90,90,180]) cylinder(h = 7, d=breite, $fn=30); 
        translate(v = [breite/2, 0, lange]) rotate([90,90,180]) cylinder(h = 7, d=breite, $fn=30); 
    }
}

module aufstecker(){

    translate(v=[4.3,4.3,0]) cylinder(h = 20, d=4, $fn=30);
    translate(v=[4.3,67.8,0]) cylinder(h = 20, d=4, $fn=30);
    //translate(v=[117.95,4.3,0]) cylinder(h = 20, d=4, $fn=30);
    //translate(v=[117.95,67.8,0]) cylinder(h = 20, d=4, $fn=30);
    }

