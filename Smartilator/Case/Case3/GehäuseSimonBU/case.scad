  color("red") translate(v = [50, 25, 15]) scale([25.4,25.4,25.4]) import("Galileo_2.stl", convexity=3);
//vierteilen

module kiste(){
    difference (){
        cube([194,110,102], false);
        rotate([180,0,0]) rundung(194,5);
        translate(v = [0, 0, 102]) rotate([90,0,0]) rundung(194,5);
        translate(v = [0, 0, 102]) rotate([90,90,270]) rundung(194,5);
        translate(v = [194, 0, 102]) rotate([90,90,0]) rundung(194,5);
        rotate([270,0,90]) rundung(194,5);
        translate(v = [194, 0, 102]) rotate([90,0,90]) rundung(194,5);
        translate(v = [194, 0, 0]) rotate([180,0,90]) rundung(194,5);
        translate(v = [0, 0, 102]) rotate([0,0,90]) rundung(194,5);
        translate(v = [194, 110, 0]) rotate([0,180,0]) rundung(194,5);
        translate(v = [0, 110, 102]) rotate([0,90,90]) rundung(194,5);
        translate(v = [194, 110, 102]) rotate([90,90,90]) rundung(194,5);
        translate(v = [194, 110, 102]) rotate([90,0,180]) rundung(194,5);
        translate(v = [5, 5, 5]) cube([184,100,92], false);
        translate(v=[28,50,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[28,85,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[62,50,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[62,85,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[96,50,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[96,85,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[130,50,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[130,85,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[165,50,-1]) cylinder(h = 7, d=30, $fn=10);
        translate(v=[165,85,-1]) cylinder(h = 7, d=30, $fn=10);
        
        translate(v=[15,35,-1]) cylinder(h = 7, d=7, $fn=10); //schrauben
        translate(v=[179,35,-1]) cylinder(h = 7, d=7, $fn=10);
        }
}


difference(){
        kiste();
        translate(v = [10, 0, 2.5])Schlitz(92,5);
        translate(v = [20, 0, 2.5])Schlitz(92,5);
        translate(v = [30, 0, 2.5])Schlitz(92,5);
        translate(v = [40, 0, 2.5])Schlitz(92,5);
        translate(v = [50, 0, 2.5])Schlitz(92,5);
        translate(v = [60, 0, 2.5])Schlitz(92,5);
        translate(v = [70, 0, 2.5])Schlitz(92,5);
        translate(v = [80, 0, 2.5])Schlitz(92,5);
        translate(v = [90, 0, 2.5])Schlitz(92,5);
        translate(v = [100, 0, 2.5])Schlitz(92,5);
        translate(v = [110, 0, 2.5])Schlitz(92,5);
        translate(v = [120, 0, 2.5])Schlitz(92,5);
        translate(v = [130, 0, 2.5])Schlitz(92,5);
        translate(v = [140, 0, 2.5])Schlitz(92,5);
        translate(v = [150, 0, 2.5])Schlitz(92,5);
        translate(v = [160, 0, 2.5])Schlitz(92,5);
        translate(v = [170, 0, 2.5])Schlitz(92,5);
        translate(v = [180, 0, 2.5])Schlitz(92,5);
    }

//color("blue") translate(v = [194, 110, 102]) rotate([90,0,180]) rundung(194,5);
 

module rundung(lange, radius) {
    translate(v = [-1, -radius, -radius]) difference (){
        cube([lange+2,radius*2,radius*2], false);
        translate(v = [-2, 0, 0]) rotate([0,90,0]) cylinder(h = lange+4, d=radius*2, $fn=10); 
    }
}

module Schlitz(lange, breite) {
     translate(v = [0, -1, 0])union (){
        translate(v = [0, 0, breite])cube([breite, 7, lange-breite], false);
        translate(v = [breite/2, 0, breite]) rotate([90,90,180]) cylinder(h = 7, d=breite, $fn=10); 
        translate(v = [breite/2, 0, lange]) rotate([90,90,180]) cylinder(h = 7, d=breite, $fn=10); 
    }
}
