//##################### SETTINGS #####################
//The max. Size (for your printer) is length+31 an height+4!

//Size
length = 184;
height = 92;
depth = 25;

//Hole Settings
radius = 40; //Larger is better (more air), but be careful of your fingers
numHoleX = 2; //Number of Holes on x-Axis
numHoleY = 1; //Number of Holes on y-Axis


//####################### CODE #######################
module fanContainer(){
    difference(){
        union(){
            rotate([0,90,0])
            cylinder(h=length+31, r=9.9, center = true);
            cube([length+10,depth+5,height+4], center = true); //main body     
        }
        translate([0,-depth/2-1,0])
        holes();
        translate([0,2,0])
        cube([length+1,depth+6,height+1], center = true);
        translate([0,0,-4])
        cube([length+26,12,12], center = true); //hole for the cables
        translate([0,depth/2+2,0])
        cube([length+11,5,height+11], center = true);
    }
}

module holes(){
    rotate([90,0,0])
    translate([(-length/2)-(length/(numHoleX*2)),(-height/2)-(height/(numHoleY*2)),0])
        for( j = [ 1 : numHoleY]){
            for( i = [ 1 : numHoleX]){
                translate([i*length/numHoleX,j*height/numHoleY, 0])
                cylinder(h = 5, r = radius, center = true, $fs = 0.1);
            }
    }
}
rotate([90,0,0])
fanContainer();