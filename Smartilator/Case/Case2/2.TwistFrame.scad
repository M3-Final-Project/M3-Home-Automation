//##################### SETTINGS #####################

length = 184;
height = 92;
depth = 25;


//####################### CODE #######################
module semicircle(){
    difference(){
        cylinder(h=5, r=15);
        translate([-15,-15,-1])
        cube([30,15,15]);   
    }
}

module twistFrame(){
    difference(){
        union(){
            translate([0,0,-height/4-18]){
            cylinder(h=22, r=10, center = true); //twist foot
            translate([0,0,6])
            semicircle();
            }
                difference(){
                    cube([length+31,depth+10,height/2+15], center = true); //main body
                    translate([0,0,10])
                    cube([length+16,depth+15,height/2+15], center = true); //inner difference cube
                    translate([0,0,-7])
                    cube([length+19.5,depth+15,height/2-7], center = true); //for the cable cover
                    cube([length+26,10,height/2+12], center = true); //cable channel
                    translate([0,0,height/4])
                    rotate([0,90,0])
                    cylinder(h=length+32, r=10.1, center = true); //
                    }
        }
        translate([0,0,-height/2])
        cylinder(h=200, r=7, center = true);
    }
}
rotate([90,0,0])
twistFrame();