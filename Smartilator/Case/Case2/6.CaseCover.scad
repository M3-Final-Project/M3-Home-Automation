
module nl(){ //Hook Counterpart
    translate([0,1.75,0])
    cube([2,4.5,4.5]);
    translate([2,0,0])
    cube([3,8,10]);
}
module nl2(){ //Hook Counterpart Mirrored
    translate([3,3.75,0])
    cube([2,4.5,4.5]);
    translate([0,2,0])
    cube([3,8,10]);
}

module caseCover(){
    difference(){
        union(){
            translate([0, 2, 0])
            cube([134, 170, 3]); //Plate
            translate([2,40,-10])
            nl();
            translate([2,80,-10])
            nl();
            translate([2,120,-10])
            nl(); 
            translate([127,38,-10])
            nl2(); 
            translate([127,78,-10])
            nl2();
            translate([127,118,-10])
            nl2();
        }
        translate([65,64,-1])
        cylinder(h=30, r=10);
        translate([-2.5+65,-15+64,-3+69-70])
        cube([5,8,10]); //Holder counterpart
        translate([-2.5+65,9+64,-3+69-70])
        cube([5,8,10]); //Holder counterpart
    }
}

rotate([180,0,0])
translate([-130/2, -170/2, 0])
caseCover();