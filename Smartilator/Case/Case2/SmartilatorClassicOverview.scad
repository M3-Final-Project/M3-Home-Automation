//##################### SETTINGS #####################
//The max. Size (for your printer) is length+31 an height+4!
// This file is just for Overview, not for printing

//Size
length = 184;
height = 92;
depth = 25;

//Hole Settings
radius = 40; //Larger is better (more air), but be careful of your fingers
numHoleX = 2; //Number of Holes on x-Axis
numHoleY = 1; //Number of Holes on y-Axis


//####################### CODE #######################
module fanContainer(){
    difference(){
        union(){
            rotate([0,90,0])
            cylinder(h=length+31, r=9.9, center = true);
            cube([length+10,depth+5,height+4], center = true); //main body     
        }
        translate([0,-depth/2-1,0])
        holes();
        translate([0,2,0])
        cube([length+1,depth+6,height+1], center = true);
        translate([0,0,-4])
        cube([length+26,12,12], center = true); //hole for the cables
        translate([0,depth/2+2,0])
        cube([length+11,5,height+11], center = true);
    }
}

module holes(){
    rotate([90,0,0])
    translate([(-length/2)-(length/(numHoleX*2)),(-height/2)-(height/(numHoleY*2)),0])
        for( j = [ 1 : numHoleY]){
            for( i = [ 1 : numHoleX]){
                translate([i*length/numHoleX,j*height/numHoleY, 0])
                cylinder(h = 5, r = radius, center = true, $fs = 0.1);
            }
    }
}

module semicircle(){
    difference(){
        cylinder(h=5, r=15);
        translate([-15,-15,-1])
        cube([30,15,15]);   
    }
}

module twistFrame(){
    difference(){
        union(){
            translate([0,0,-height/4-18]){
            cylinder(h=22, r=10, center = true); //twist foot
            translate([0,0,6])
            semicircle();
            }
                difference(){
                    cube([length+31,depth+10,height/2+15], center = true); //main body
                    translate([0,0,10])
                    cube([length+16,depth+15,height/2+15], center = true); //inner difference cube
                    translate([0,0,-7])
                    cube([length+19.5,depth+15,height/2-7], center = true); //for the cable cover
                    cube([length+26,10,height/2+12], center = true); //cable channel
                    translate([0,0,height/4])
                    rotate([0,90,0])
                    cylinder(h=length+32, r=10.1, center = true); //twisting cylinder
                    }
        }
        translate([0,0,-height/2])
        cylinder(h=200, r=7, center = true); //cable hole to Case
    }
}

module fanCover(){
    difference(){
        cube([length+10,3,height+4], center = true);
        holes();     
    }
}

module twistFrontPlate(){
    difference(){ 
        translate([0,0,-7])
        cube([length+19,depth+15,height/2-7.5], center = true);
        cube([length+16,depth+16,height/2], center = true);
    }
}

module cableCylinder(){
    difference(){
        union(){
            difference(){
                cylinder(h=20, r=18);              translate([0,0,15]) //Outer Cylinder
                cylinder(h=10, r=16); //Basement
            }
            translate([-2.5,-17,10])
            cube([5,8,10]); //Stop-Cube
            translate([-2.5,-17,-3])
            cube([5,7,10]); //Holder 1
            translate([-2.5,9,-3])
            cube([5.5,7,10]); //Holder 2
        }
        translate([0,0,-10])
        cylinder(h=31, r=10.5);
    }
}

module nl(){ //Hook Counterpart
    translate([0,1.75,0])
    cube([2,4.5,4.5]);
    translate([2,0,0])
    cube([3,8,10]);
}
module nl2(){ //Hook Counterpart Mirrored
    translate([3,3.75,0])
    cube([2,4.5,4.5]);
    translate([0,2,0])
    cube([3,8,10]);
}

module caseCover(){
    translate([0,-2,0])
    difference(){
        union(){
            translate([0, 2, 0])
            cube([134, 172, 3]); //Plate
            translate([2,40,-10])
            nl();
            translate([2,80,-10])
            nl();
            translate([2,120,-10])
            nl(); 
            translate([127,38,-10])
            nl2(); 
            translate([127,78,-10])
            nl2();
            translate([127,118,-10])
            nl2();
        }
        translate([65,64,-1])
        cylinder(h=30, r=10);
        translate([-2.5+65,-15+64,-3+69-70])
        cube([5,8,10]); //Holder counterpart
        translate([-2.5+65,9+64,-3+69-70])
        cube([5,8,10]); //Holder counterpart
    }
}

module l(){ //Hook
    difference(){
        union(){
            cube([4,12,8]);
            translate([0,10,0])
            cube([4,6,15]);
        }
        translate([-1,6,-1])
        cube([5,11,3]);
    }
}

module sides() {
    difference(){
        union(){
            cube([4, 170, 40]);
            translate([130, 0, 0])
            cube([4, 170, 40]);
        }
    }
    cube([130, 2, 40]);
    translate([0, 170, 0])
    cube([134, 2, 40]);
}

module power() {
    translate([24.5, -1, 15])
    cube([20, 7, 12]);
}
module foot() {
    translate([54, 47, 2]) { 
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([117, 47, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([54, 160, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([117, 160, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
}

module differenceSide() {
    difference() {
        sides();
        power(); 
    }
}

module case(){
    difference(){
        union(){
            cube([130, 170, 2]);
            differenceSide();
            foot();
        }
        translate([1,40,30])
        l();
        translate([129,40,30])
        l();
        translate([1,80,30])
        l();
        translate([129,80,30])
        l();
        translate([1,120,30])
        l();
        translate([129,120,30])
        l();
    }
}

translate([0,0,height/2-height/4])
twistFrame();

translate([0,0,height/2])
fanContainer();

translate([0,depth,height/2])
fanCover();

translate([0,-depth*2,height/2-height/4])
twistFrontPlate();

translate([0,0,-50])
cableCylinder();

translate([-130/2, -170/2+21, -70])
caseCover();

translate([-130/2, -170/2+21, -130])
case();