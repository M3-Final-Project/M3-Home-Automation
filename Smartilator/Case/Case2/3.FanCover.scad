//##################### SETTINGS #####################
//The max. Size (for your printer) is length+31 an height+4!
//Rendering time (F6) can be more than 5 minutes (depends on numbers of holes)

//Size
length = 184;
height = 92;
depth = 25;

//Hole Settings
radius = 40; //Larger is better (more air), but be careful of your fingers
numHoleX = 2; //Number of Holes on x-Axis
numHoleY = 1;

//####################### CODE #######################
module fanCover(){
    difference(){
        cube([length+10,3,height+4], center = true);
        holes();     
    }
}
module holes(){
    rotate([90,0,0])
    translate([(-length/2)-(length/(numHoleX*2)),(-height/2)-(height/(numHoleY*2)),0])
        for( j = [ 1 : numHoleY]){
            for( i = [ 1 : numHoleX]){
                translate([i*length/numHoleX,j*height/numHoleY, 0])
                cylinder(h = 5, r = radius, center = true, $fs = 0.1);
            }
    }
}
//projection(cut=true) //uncomment this, if you want to lasercut the plate
rotate([90,0,0])
fanCover();