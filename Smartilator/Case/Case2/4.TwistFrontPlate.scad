//##################### SETTINGS #####################

length = 184;
height = 92;
depth = 25;


//####################### CODE #######################

module twistFrontPlate(){
    difference(){ 
        translate([0,0,-7])
        cube([length+19,depth+15,height/2-7.5], center = true);
        cube([length+16,depth+16,height/2], center = true);
    }
}
twistFrontPlate();