######################## Notes #######################
- Rendering Time for file 1 and 3 may be longer than 5 minutes (depends on the number of holes).
- The max. size of one part is your length+31, depth+15 and height+11 millimeters. Notice, that a 3D Printer isn't able to print too big cases!
- There is a Overview file. This file is not for printing.
- File no. 3 is laser cuttable. In this case uncomment the marked line and export it to a file format, that your lasercutter supports
- Mofifying the case is allowed, if you understand the code. This may be helpful: http://www.openscad.org/cheatsheet/index.html

########## How to print (export) this case ###########

1. Install OpenSCAD (http://www.openscad.org/)
2. Install Autodesk Meshmixer (http://www.123dapp.com/meshmixer)
3. Open a .scad file
4. Enter your sizes (in millimeters) and your preferred settings and press F5 (fast rendering)
5. If this case looks okay, press F6 (normal rendering)
6. Export the file as .stl (recommended)

Step 7-10 are only for file 1, 2, 5, 6 and 7 and only needed, if your 3D Printer doesn't have a support material feature:
7. Import the part into Autodesk Meshmixer
8. Click on "Print"
9. Select your 3D Printer and if needed, move the case to an optimal position (rotate/move to platform in the transform submenu)
8. Add support material with Meshmixer
10. Click on "Send to Printer", your 3D Printer Software should open or export the file for your Printer Software

11. Print it and enjoy
12. Put the printed parts together. (You will need glue for part 3)

