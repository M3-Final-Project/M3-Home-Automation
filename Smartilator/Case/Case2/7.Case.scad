module l(){ //Hook
    difference(){
        union(){
            cube([4,12,8]);
            translate([0,10,0])
            cube([4,6,15]);
        }
        translate([-1,6,-1])
        cube([5,11,3]);
    }
}

module sides() {
    difference(){
        union(){
            cube([4, 170, 40]);
            translate([130, 0, 0])
            cube([4, 170, 40]);
        }
    }
    cube([130, 2, 40]);
    translate([0, 170, 0])
    cube([134, 2, 40]);
}

module power() {
    translate([24.5, -1, 15])
    cube([20, 7, 12]);
}
module foot() {
    translate([54, 47, 2]) { 
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([117, 47, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([54, 160, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
    translate([117, 160, 2]) {
        cylinder(h=12,r=5,$fs=0.1);
        cylinder(h=20,r=1.5,$fs=0.1);
    }
}

module differenceSide() {
    difference() {
        sides();
        power(); 
    }
}

module case(){
    difference(){
        union(){
            cube([130, 170, 2]);
            differenceSide();
            foot();
        }
        translate([1,40,30])
        l();
        translate([129,40,30])
        l();
        translate([1,80,30])
        l();
        translate([129,80,30])
        l();
        translate([1,120,30])
        l();
        translate([129,120,30])
        l();
    }
}
translate([-65,-85,-20])
case();