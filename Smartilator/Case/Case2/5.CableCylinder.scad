module cableCylinder(){
    difference(){
        union(){
            difference(){
                cylinder(h=20, r=18);              translate([0,0,15]) //Outer Cylinder
                cylinder(h=10, r=16); //Basement
            }
            translate([-2.5,-17,10])
            cube([5,8,10]); //Stop-Cube
            translate([-2.5,-17,-3])
            cube([5,7,10]); //Holder 1
            translate([-2.5,9,-3])
            cube([5.5,7,10]); //Holder 2
        }
        translate([0,0,-10])
        cylinder(h=31, r=10.5);
    }
}

rotate([180,0,0])
cableCylinder();