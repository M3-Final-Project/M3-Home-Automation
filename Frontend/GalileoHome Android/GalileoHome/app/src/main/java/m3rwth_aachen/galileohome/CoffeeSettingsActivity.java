package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class CoffeeSettingsActivity extends ActionBarActivity {

    private SharedPreferences prefs;
    private String ipAdress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coffee_settings);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.titel_coffe_settings);

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress","127.0.0.1");
        setSendListener();


        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));

        if(Build.VERSION.SDK_INT  > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);

        new update().execute("coffee/access_token_key.html","access_token_key");
        new update().execute("coffee/access_token_secret.html","access_token_secret");
        new update().execute("coffee/consumer_key.html","consumer_key");
        new update().execute("coffee/consumer_secret.html","consumer_secret");
        new update().execute("oauth_key.html","oauth_key");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(CoffeeSettingsActivity.this,SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try{
                URL serverIp = new URL("http://"+ipAdress+"/m3server/"+params[0]);
                HttpURLConnection serverConnection = (HttpURLConnection)serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                serverConnection.setConnectTimeout(1000);
                serverConnection.setReadTimeout(1000);

                try{
                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line);
                    }
                    if(total.length() != 0){
                        EditText editText;
                        switch (params[1]){
                            case "access_token_key":
                                editText = (EditText) findViewById(R.id.edit_act);
                                updateTextViews(editText,total.toString());
                                break;
                            case "access_token_secret":
                                editText = (EditText) findViewById(R.id.edit_acs);
                                updateTextViews(editText,total.toString());
                                break;
                            case "consumer_key":
                                editText = (EditText) findViewById(R.id.edit_ck);
                                updateTextViews(editText,total.toString());
                                break;
                            case "consumer_secret":
                                editText = (EditText) findViewById(R.id.edit_cs);
                                updateTextViews(editText,total.toString());
                                break;
                            case "oauth_key":
                                editText = (EditText) findViewById(R.id.edit_oauth);
                                updateTextViews(editText,total.toString());
                                break;
                        }
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            catch(MalformedURLException urlFalse){
                urlFalse.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            return "Success";
        }
    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                connection.setConnectTimeout(1000);
                connection.setReadTimeout(1000);
                EditText editText;
                String input = "";
                switch (params[1]){
                    case "access_token_key":
                        editText = (EditText) findViewById(R.id.edit_act);
                        input = editText.getText().toString();
                        break;
                    case "access_token_secret":
                        editText = (EditText) findViewById(R.id.edit_acs);
                        input = editText.getText().toString();
                        break;
                    case "consumer_key":
                        editText = (EditText) findViewById(R.id.edit_ck);
                        input = editText.getText().toString();
                        break;
                    case "consumer_secret":
                        editText = (EditText) findViewById(R.id.edit_cs);
                        input = editText.getText().toString();
                        break;
                    case "oauth_key":
                        editText = (EditText) findViewById(R.id.edit_oauth);
                        input = editText.getText().toString();
                        break;
                }
                if(input.length()!=0){
                    urlParameters = "param1=pub&param2="+params[0]+"&param3="+input;

                    connection.setRequestProperty("Content-Length", "" +
                            Integer.toString(urlParameters.getBytes().length));
                    connection.setRequestProperty("Content-Language", "en-US");

                    connection.setUseCaches (false);
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    //Send request
                    DataOutputStream wr = new DataOutputStream (
                            connection.getOutputStream ());
                    wr.writeBytes (urlParameters);
                    wr.flush ();
                    wr.close ();

                    //Get Response
                    InputStream is = connection.getInputStream();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuilder response = new StringBuilder();
                    while((line = rd.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    rd.close();
                    return response.toString();
                }


            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
                return "Success";
            }

        }
    }

    private void updateTextViews (final EditText editText, final String string){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("GalileoHome",string);
                editText.setText(string);
            }
        });
    }

    private void setSendListener(){
        Button button = (Button) findViewById(R.id.button_send_coffe_settings);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new send().execute("coffee/access_token_key","access_token_key");
                new send().execute("coffee/access_token_secret","access_token_secret");
                new send().execute("coffee/consumer_key","consumer_key");
                new send().execute("coffee/consumer_secret","consumer_secret");
                new send().execute("oauth_key","oauth_key");
                Intent intent = new Intent(CoffeeSettingsActivity.this,KaffeemaschineActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }



}
