package m3rwth_aachen.galileohome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity  {



    // UI references.
    private AutoCompleteTextView IpView;
    private EditText mPasswordView;
    private final String ipPattern = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    private SharedPreferences prefs;
    private boolean ipOk = false;
    private PostController postController;
    private boolean firstHit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        IpView = (AutoCompleteTextView) findViewById(R.id.ipAdress);
        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        IpView.setText(prefs.getString("ipAdress",""));

        postController = new PostController();



        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                return id == R.id.login || id == EditorInfo.IME_NULL;
            }
        });

        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }


        Button SignInButton = (Button) findViewById(R.id.log_in_button);
        SignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText ipField = (EditText) (findViewById(R.id.ipAdress));
                String ip = ipField.getText().toString();
                EditText passwordField = (EditText) (findViewById(R.id.ipAdress));
                String password = passwordField.getText().toString();
                if(isNetworkAvailable()){
                    if(isIpValid(ip)){
                        if(isServerOk(ip)){
                            if(isPasswordValid(ip,password)){
                                saveIpAdress(ip);
                                Intent intent = new Intent(LoginActivity.this, SmartDoorOpener.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, R.string.error_invalid_ip, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this,R.string.error_no_network_available,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean isIpValid(String ip) {
        Pattern pattern = Pattern.compile(ipPattern);
        Matcher matcher = pattern.matcher(ip);
        if(!matcher.matches()) {
            return false;
        }
        return true;
    }

    private boolean isServerOk (String ip){
        try {
            new check().execute(ip).get(2000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e){
            e.printStackTrace();
            Toast.makeText(this,R.string.error_connection,Toast.LENGTH_SHORT).show();
            return false;
        } catch (ExecutionException e){
            e.printStackTrace();
            Toast.makeText(this,R.string.error_connection,Toast.LENGTH_SHORT).show();
            return false;
        } catch (TimeoutException e){
            e.printStackTrace();
            Toast.makeText(this,R.string.error_connection,Toast.LENGTH_SHORT).show();
            return false;
        }
        if(ipOk){return true;} else return false;
    }

    private boolean isPasswordValid(String ip, String password) {
        String hash = md5(password);
        postController.sendSomething(this, ip, "param1=password&param2=" + hash);
        String result = postController.updateSomething(this,ip+"/m3server/passwordResult.html");
        if(!result.isEmpty()){
            if(result.charAt(0)=='1'){
                return true;
            }
        }

        Toast.makeText(this, R.string.error_incorrect_password, Toast.LENGTH_SHORT).show();
        return false;
    }



    private void saveIpAdress(String ip) {
            prefs.edit().putString("ipAdress", ip).apply();
    }

    private class check extends AsyncTask<String, Void, Boolean> {

        protected Boolean doInBackground(String... params) {

            try {
                HttpURLConnection.setFollowRedirects(false);
                URL serverConnection = new URL("http://"+params[0]+"/m3server/active_devices.html");
                HttpURLConnection con = (HttpURLConnection) serverConnection.openConnection();
                con.setRequestMethod("HEAD");
                con.setReadTimeout(1000);
                con.setConnectTimeout(1000);
                if(con.getResponseCode() == HttpURLConnection.HTTP_OK){
                    ipOk = true;
                    return true;
                }
                else{
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            boolean bResponse = result;
            if (bResponse) {
            } else {
                Toast.makeText(LoginActivity.this, R.string.error_not_reachable_ip, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}

