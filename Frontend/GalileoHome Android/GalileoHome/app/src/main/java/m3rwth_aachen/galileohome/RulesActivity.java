package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.software.shell.fab.ActionButton;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class RulesActivity extends ActionBarActivity {


    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private SharedPreferences prefs;
    private String ipAdress;
    private boolean updatePending;
    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    private boolean stopEverything = false;
    private boolean firstHit = false;


    private ArrayList<String> list;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();
        setAppearButtonListener();

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress", "127.0.0.1");
        updatePending = false;
        list = new ArrayList<String>();

        ListView listView = (ListView) findViewById(R.id.listView_rules);
        adapter = new ArrayAdapter<String>(this,R.layout.rule_list_overview_item,list);
        listView.setAdapter(adapter);

        setAddButtonListener();
        setDeleteButtonListener();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ActionButton actionButton = (ActionButton) findViewById(R.id.button_delete_rules);
        actionButton.setImageResource(R.drawable.ic_delete_white_24dp);

        getSupportActionBar().setTitle(R.string.title_rules);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));
        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }
        Runnable updateRunner = new Runnable() {
            public void run() {
                if(!updatePending && !stopEverything){
                    new update().execute("rules");
                }
            }
        };
        exec.scheduleAtFixedRate(updateRunner, 500, 5000, TimeUnit.MILLISECONDS);
    }

    public void onDestroy(){
        stopEverything = true;
        exec.shutdown();
        super.onDestroy();
    }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this, R.string.text_stopApp, Toast.LENGTH_LONG).show();
        }
    }



    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(RulesActivity.this, SmartDoorOpener.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 1:
                        intent = new Intent(RulesActivity.this, SmartilatorActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(RulesActivity.this, KaffeemaschineActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        intent = new Intent(RulesActivity.this, AmbilightActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        intent = new Intent(RulesActivity.this, SensorsActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;

                    case 6:
                        intent = new Intent(RulesActivity.this, Presets.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_rules);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(RulesActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    private void setDeleteButtonListener(){
        ActionButton delete = (ActionButton) findViewById(R.id.button_delete_rules);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListView listView = (ListView) findViewById(R.id.listView_rules);
                SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
                int itemCount = listView.getCount();

                for(int i=itemCount-1; i >= 0; i--){
                    if(checkedItemPositions.get(i)){
                        StringBuilder wholeId = new StringBuilder();
                        wholeId.append(listView.getAdapter().getItem(i).toString());
                        String id = wholeId.substring(4,wholeId.lastIndexOf(":"));
                        new send().execute(id);
                    }
                }
                checkedItemPositions.clear();
                //adapter.notifyDataSetChanged();
            }
        });
    }

    private void setAddButtonListener(){
        ActionButton info = (ActionButton) findViewById(R.id.button_add_rules);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RulesActivity.this, MakeRuleActivity.class);
                startActivity(intent);
            }
        });
    }

    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL serverIp = new URL("http://"+ipAdress+"/m3server/" + params[0] + ".html");
                HttpURLConnection serverConnection = (HttpURLConnection) serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                serverConnection.setConnectTimeout(1000);
                serverConnection.setReadTimeout(1000);
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                switch (params[0]) {
                    case "rules":
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            String id;
                            String name;
                            list.clear();
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                                if (total.length() != 0 && total != null) {
                                    id = total.substring(0,total.indexOf("?"));
                                    name = total.substring(total.indexOf("=")+1,total.indexOf("?",total.indexOf("=")));
                                    addItems(id+": "+name);
                                    total.delete(0,total.length());
                                }
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;


                }


            } catch (MalformedURLException urlFalse) {
                urlFalse.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            notifyList();
            return "Success";
        }
    }

    private void addItems(String id){
        final String finalId = id;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.add("ID: " + finalId);
            }
        });
    }

    private void notifyList(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://" + ipAdress + ":8080");
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "param1=rule&param2=delete&param3=" + params[0];
                Log.i("GalileoHome", urlParameters);
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setConnectTimeout(1000);
                connection.setReadTimeout(1000);

                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                Log.i("GalileoHome", urlParameters);
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if (connection != null) {
                    connection.disconnect();

                }
            }

        }
    }

    private void setAppearButtonListener (){
        final ListView listView = (ListView) findViewById(R.id.listView_rules);
        listView.setItemsCanFocus(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ActionButton button = (ActionButton) findViewById(R.id.button_delete_rules);
                if(listView.getAdapter().isEnabled(position)){
                    button.setVisibility(View.VISIBLE);
                }
                else if(!listView.getAdapter().isEnabled(position))  {
                    button.setVisibility(View.INVISIBLE);
                }
            }
        });
    }




}
