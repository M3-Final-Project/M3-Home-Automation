package m3rwth_aachen.galileohome;

/**
 * Created by X on 19.06.2015.
 */
class Rule {

    private String title;
    private String value;
    private boolean selected;
    private String internalName;


    public Rule(){
        super();
    }

    public Rule(String title, String value, String internal) {
        super();
        this.title = title;
        this.value = value;
        this.selected = false;
        this.internalName = internal;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public String getName(){
        return title;
    }

    public boolean isSelected(){
        return selected;
    }

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value=value;
    }

    public String getInternalname(){
        return internalName;
    }
}
