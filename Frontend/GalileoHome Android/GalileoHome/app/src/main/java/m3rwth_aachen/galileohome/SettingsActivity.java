package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


public class SettingsActivity extends ActionBarActivity {


    private SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_settings);

        setInfoButtonListener();
        setImpressumButtonListener();
        setLogoutButtonListener();
        setHelpButtonListener();
        setLicensesButtonListener();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));

        if(Build.VERSION.SDK_INT  > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void performLogout(){
        prefs.edit().remove("ipAdress").apply();
        prefs.edit().clear().apply();
        Intent intent = new Intent(SettingsActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void openImpressum(){
        Intent intent = new Intent(SettingsActivity.this,ImpressumActivity.class);
        startActivity(intent);
    }



    private void openInfo(){
        Intent intent = new Intent(SettingsActivity.this,InfoActivity.class);
        startActivity(intent);
    }

    private void openHelp() {
        Intent intent = new Intent(SettingsActivity.this,HelpActivity.class);
        startActivity(intent);
    }

    private void setInfoButtonListener(){
        Button info = (Button) findViewById(R.id.button_info);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfo();
            }
        });
    }

    private void openLicenses(){
        Intent intent = new Intent(SettingsActivity.this,Licensesactivity.class);
        startActivity(intent);
    }
    private void setImpressumButtonListener(){
        Button info = (Button) findViewById(R.id.button_impressum);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImpressum();
            }
        });
    }
    private void setLogoutButtonListener(){
        Button info = (Button) findViewById(R.id.button_logout);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLogout();
            }
        });
    }

    private void setHelpButtonListener(){
        Button info = (Button) findViewById(R.id.button_help);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelp();
            }
        });
    }

    private void setLicensesButtonListener(){
        Button info = (Button) findViewById(R.id.button_licenses);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLicenses();
            }
        });
    }
}
