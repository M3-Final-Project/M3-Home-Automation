package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.software.shell.fab.ActionButton;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MakeRuleActivity extends ActionBarActivity {
    private List<Rule> listIf;
    private List<Rule> listThen;
    private SharedPreferences prefs;
    private String ipAdress;
    private ListView listViewIf;
    private ListView listViewThen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_rule);

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress", "127.0.0.1");

        setUpListIf();
        setUpListThen();
        setDoneButtonListener();
        setCheckListener();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ActionButton actionButton = (ActionButton) findViewById(R.id.button_rule_ready);
        actionButton.setImageResource(R.drawable.ic_done_all_white_24dp);

        getSupportActionBar().setTitle(R.string.title_makeRule);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));
        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    private void setUpListIf(){
        listViewIf = (ListView)findViewById( R.id.listView_RuleIf);
        Resources res = getResources();
        String[] happenings = res.getStringArray(R.array.happenings);
        listIf = new ArrayList<Rule>();
        listIf.add(new Rule(happenings[0], "Time", "time="));
        listIf.add(new Rule(happenings[1], "", "door=1"));
        listIf.add(new Rule(happenings[2], "", "door=0"));
        listIf.add(new Rule(happenings[3], "", "coffe=1"));
        listIf.add(new Rule(happenings[4], "Degree", "temperature>"));
        listIf.add(new Rule(happenings[5], "Degree", "temperature<"));
        listIf.add(new Rule(happenings[6], "dB", "volume<"));
        listIf.add(new Rule(happenings[7], "dB", "volume>"));
        listIf.add(new Rule(happenings[8], "Lux", "brightness>"));
        listIf.add(new Rule(happenings[9], "Lux", "brightness<"));
        RuleAdapter ruleAdapter = new RuleAdapter(this,R.layout.rule_list_item, listIf);
        listViewIf.setAdapter(ruleAdapter);
    }

    private void setUpListThen(){
        listViewThen = (ListView)findViewById( R.id.listView_RuleThen );
        Resources res = getResources();
        String[] happens = res.getStringArray(R.array.happens);
        /*final ArrayList<String> listItems = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>( this,android.R.layout.simple_list_item_multiple_choice, happens );
        listView.setAdapter(adapter);*/

        listThen = new ArrayList<Rule>();
        listThen.add(new Rule(happens[0], "Power", "smartilator="));
        listThen.add(new Rule(happens[1], "", "door=1"));
        listThen.add(new Rule(happens[2], "", "door=0"));
        listThen.add(new Rule(happens[3], "", "coffee=1"));
        listThen.add(new Rule(happens[4], "Color", "light="));
        RuleAdapter ruleAdapter = new RuleAdapter(this,R.layout.rule_list_item, listThen);
        listViewThen.setAdapter(ruleAdapter);

    }

    private void setDoneButtonListener(){
        ActionButton done = (ActionButton) findViewById(R.id.button_rule_ready);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeSending();
                Intent intent = new Intent(MakeRuleActivity.this, RulesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private String getNameField(){
        EditText name = (EditText)findViewById(R.id.newRuleNameField);
        String fieldName = name.getText().toString();
        int random = (int) (Math.random()*500);
        if(!fieldName.isEmpty() && fieldName !=null){
            return String.valueOf(random)+"?name="+fieldName;
        }

        return String.valueOf(random)+"?name=Regel "+String.valueOf(random);
    }

    private void initializeSending(){
        String paramter = makeRule();
        String nameAndId = getNameField();
        if(paramter.lastIndexOf("?")!=-1 && !paramter.equals("error") && !paramter.isEmpty()){
             new send().execute(nameAndId+"?"+paramter);

        }

    }

    private void setCheckListener(){
        final ListView listView = (ListView)findViewById(R.id.listView_RuleIf);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckBox ctv = (CheckBox) view.getTag(R.id.ruleItemCheckbox);
                ctv.toggle();

            }
        });

    }

    private List<Rule> getCheckedItemsIf(){
        RuleAdapter ruleAdapter = (RuleAdapter) listViewIf.getAdapter();
        return ruleAdapter.getCheckedItems();
    }

    private List<Rule> getCheckedItemsThen(){
        RuleAdapter ruleAdapter = (RuleAdapter) listViewThen.getAdapter();
        return ruleAdapter.getCheckedItems();
    }

    private String makeRule(){
        List<Rule> listCheckedIf = getCheckedItemsIf();
        List<Rule> listCheckedThen = getCheckedItemsThen();
        if(!listCheckedIf.isEmpty() && !listCheckedThen.isEmpty()){
            String param = "";
            for (Rule a : listCheckedIf ){
                param = param+a.getInternalname();
                if(!a.getValue().equals("")){
                    if(a.getValue().contains(" ")){
                        String add = a.getValue().substring(0,a.getValue().lastIndexOf(" "));
                        param = param+add;
                    }else if(a.getValue().contains(":")){
                        StringBuilder add = new StringBuilder();
                        add.append(a.getValue());
                        if(add.length()==4){
                            add.insert(0,'0');
                        }
                        add.delete(add.lastIndexOf(":"),add.lastIndexOf(":")+1);
                        param = param+add;
                    }
                    else{
                        param = param+a.getValue();
                    }
                }
                param = param+"!";
            }
            param = param.substring(0,param.length()-1);
            param = param+"?";
            for (Rule a : listCheckedThen ){
                param = param+a.getInternalname();
                if(a.getValue().length()!=0){
                    if(a.getValue().contains("#")){
                        String add = a.getValue().substring(1);
                        param = param + add;
                    } else {
                        String add = a.getValue().substring(0,a.getValue().indexOf("%"));
                        param = param+add;
                    }
                }
                param = param+"!";
            }
            param = param.substring(0,param.length()-1);
            return param;
        }
        return "error";
    }


    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "param1=rule&param2=add&param3="+params[0];
                //connection.setRequestProperty("Content-Length", "" +
                        //Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                Log.i("GalileoHome","send parameters: "+urlParameters);
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                Log.i("GalileoHome",response.toString());
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();

                }
            }

        }
    }






}
