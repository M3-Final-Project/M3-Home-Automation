package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.software.shell.fab.ActionButton;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class Presets extends ActionBarActivity {


    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private SharedPreferences prefs;
    private SharedPreferences stored;
    private String ipAdress;
    private ArrayList<String> list;
    private ArrayList<String> commands;
    private ArrayAdapter<String> adapter;
    private boolean firstHit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presets);
        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();
        setListUp();
        setDeleteButtonListener();




        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress", "127.0.0.1");

        ActionButton actionButton = (ActionButton) findViewById(R.id.button_delete_preset);
        actionButton.setImageResource(R.drawable.ic_delete_white_24dp);


        setAddPresetButtonListener();
        setUpListener();
        setAppearButtonListener();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }


        getSupportActionBar().setTitle(R.string.title_presets);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));

    }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this,R.string.text_stopApp,Toast.LENGTH_LONG).show();
        }
    }

    private void setListUp(){
        list = new ArrayList<String>();
        commands = new ArrayList<String>();
        stored = this.getSharedPreferences("com.m3rwth_aachen_presets", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = stored.edit();
        int presetCount = stored.getInt("preset_count", 0);
        Log.i("galileoHome presetCount", String.valueOf(presetCount));
        int i =1;
        do{
            if(presetCount!=0){
                String presetName = stored.getString("preset_name_" + i, "err");
                list.add(presetName);
                String command = stored.getString("preset_command_"+i,"err");
                commands.add(command);
                i++;
            }

        }while(i<=presetCount);


        ListView listView = (ListView) findViewById(R.id.listView_PresetsChosen);
        adapter = new ArrayAdapter<String>(this,R.layout.rule_list_overview_item,list);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        editor.apply();

    }

    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(Presets.this, SmartDoorOpener.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 1:
                        intent = new Intent(Presets.this, SmartilatorActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(Presets.this, KaffeemaschineActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        intent = new Intent(Presets.this, AmbilightActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        intent = new Intent(Presets.this, SensorsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;

                    case 7:
                        intent = new Intent(Presets.this, RulesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_presets);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Presets.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }


    private void setAddPresetButtonListener(){
        ActionButton add = (ActionButton) findViewById(R.id.button_add_preset);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Presets.this,MakePresetActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setDeleteButtonListener(){
        ActionButton delete = (ActionButton) findViewById(R.id.button_delete_preset);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListView listView = (ListView) findViewById(R.id.listView_PresetsChosen);
                SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
                int itemCount = listView.getCount();
                if(checkedItemPositions.get(itemCount-1)){
                    adapter.remove(list.get(itemCount - 1));
                    int id = stored.getInt("preset_count",0);
                    stored.edit().remove("preset_name_" + id).apply();
                    stored.edit().remove("preset_command_" + id).apply();
                    int presetCount = stored.getInt("preset_count",0);
                    stored.edit().putInt("preset_count",presetCount-1).apply();
                    commands.remove(commands.size()-1);
                }
                checkedItemPositions.clear();
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void setUpListener(){
        ListView listView = (ListView) findViewById(R.id.listView_PresetsChosen);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                String command = commands.get(pos);
                StringBuilder pass = new StringBuilder();
                for (int i = 0; i< command.length();i++){
                    if(command.charAt(i)!='?'){
                        pass.append(command.charAt(i));
                    }
                    else {
                        new send().execute(pass.toString());
                        pass.delete(0,pass.length());
                    }
                }
                Toast.makeText(Presets.this,R.string.text_preset_activated,Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                Log.i("GalileoHome",params[0]);
                urlParameters = params[0];
                Log.i("urlParameter: ",urlParameters);
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
            }

        }
    }


    private void setAppearButtonListener (){
        final ListView listView = (ListView) findViewById(R.id.listView_PresetsChosen);
        listView.setItemsCanFocus(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ActionButton button = (ActionButton) findViewById(R.id.button_delete_preset);
                if(listView.getAdapter().isEnabled(position)){
                    button.setVisibility(View.VISIBLE);
                }
                else if(!listView.getAdapter().isEnabled(position)) {
                    button.setVisibility(View.INVISIBLE);
                }
            }
        });
    }



}
