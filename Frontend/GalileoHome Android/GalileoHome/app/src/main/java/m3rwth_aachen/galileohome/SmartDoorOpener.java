/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package m3rwth_aachen.galileohome;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


public class SmartDoorOpener extends ActionBarActivity {
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean isLocked;           //boolean if Door is locked
    private SharedPreferences prefs;
    private String ipAdress;
    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    private boolean updatePending;
    private boolean stopEverything = false;
    private Runnable updateRunner = new Runnable() {
        public void run() {
            if(!updatePending && !stopEverything){
                new update().doInBackground("/m3server/sdo/state");
            }
        }
    };
    private boolean firstHit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_door_opener);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress", "127.0.0.1");

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_door_opener);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));


        new update().execute("/m3server/ibeacon");
        setButtonListenerBeaconField();

        setLockButtonListener();
        setFilter();
        if(Build.VERSION.SDK_INT  > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }
        updatePending = false;

        exec.scheduleAtFixedRate(updateRunner, 200, 1000, TimeUnit.MILLISECONDS);



    }

    public void onDestroy(){
        stopEverything = true;
        exec.shutdown();
        super.onDestroy();
    }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this, R.string.text_stopApp, Toast.LENGTH_LONG).show();
        }
    }

    private void setButtonListenerBeaconField(){
        final EditText beaconEdit = (EditText) findViewById(R.id.editIbeaconKey);
        beaconEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            if (!beaconEdit.getText().toString().isEmpty()) {
                                new send().execute("key", beaconEdit.getText().toString());
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 1:
                        intent = new Intent(SmartDoorOpener.this, SmartilatorActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(SmartDoorOpener.this, KaffeemaschineActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        intent = new Intent(SmartDoorOpener.this, AmbilightActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        intent = new Intent(SmartDoorOpener.this, SensorsActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 6:
                        intent = new Intent(SmartDoorOpener.this, Presets.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 7:
                        intent = new Intent(SmartDoorOpener.this, RulesActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_door_opener);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(SmartDoorOpener.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setFilter(){
        ImageView imageView = (ImageView) findViewById(R.id.image_locked);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        if(Build.VERSION.SDK_INT == 21){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#2484ad"));
        }
    }


    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try{
                URL serverIp = new URL("http://"+ipAdress+params[0]+".html");
                HttpURLConnection serverConnection = (HttpURLConnection)serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                switch (params[0]){
                    case "/m3server/sdo/state":
                        final Button lockButton = (Button) findViewById(R.id.button_lock);
                        final ImageView icon = (ImageView) findViewById(R.id.image_locked);
                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.length() != 0 && total.charAt(0) == '0'){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lockButton.setText(R.string.button_locked_text);
                                        Resources res = getResources();
                                        icon.setImageDrawable(res.getDrawable(R.drawable.lockclosed100));
                                        setFilter();
                                        isLocked = true;
                                    }
                                });
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lockButton.setText(R.string.button_unlocked_text);
                                        Resources res = getResources();
                                        icon.setImageDrawable(res.getDrawable(R.drawable.lockopen100));
                                        setFilter();
                                        isLocked = false;
                                    }
                                });
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        break;
                    case "/m3server/ibeacon":

                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.length() != 0){
                                EditText editText = (EditText) findViewById(R.id.editIbeaconKey);
                                updateEditText(total.toString(), editText);
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        break;
                }


            }
            catch(MalformedURLException urlFalse){
                urlFalse.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            return "Success";
        }
    }

    private void updateEditText(final String text, final EditText editText){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                editText.setText(text);
            }
        });
    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                Log.i("GalileoHome",params[0]);
                if(params[0].equals("state")){
                    urlParameters = "param1=pub&param2=sdo/state&param3="+params[1];
                }
                else if(params[0].equals("key")){
                    urlParameters = "param1=pub&param2=ibeacon&param3="+params[1];
                }
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
                updatePending = false;
            }

        }
    }

    private void setLockButtonListener(){
        Button changeLock = (Button) findViewById(R.id.button_lock);
        changeLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePending = true;
                if(isLocked){
                    new send().execute("state","1");
                }
                else{
                    new send().execute("state","0");
                }
            }
        });
    }




}