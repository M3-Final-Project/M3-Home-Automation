package m3rwth_aachen.galileohome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by X on 03.07.2015.
 */
class PostController {
    String returnValue = "";

    public PostController(){

    }

    public void sendSomething(Activity activity, String... params){
        new send(activity).execute(params);
    }

    public String updateSomething (Activity activity, String... params){
        try {
            new update(activity).execute(params).get(2000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e){
            e.printStackTrace();
            Toast.makeText(activity,R.string.error_connection,Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e){
            e.printStackTrace();
            Toast.makeText(activity,R.string.error_connection,Toast.LENGTH_SHORT).show();
        } catch (TimeoutException e){
            e.printStackTrace();
            Toast.makeText(activity,R.string.error_connection,Toast.LENGTH_SHORT).show();
        }
        return returnValue;
    }

    private class send extends AsyncTask<String, Void, String> {

        private ProgressDialog dialog;
        private Activity activity;

        public send(Activity activity) {
            dialog = new ProgressDialog(activity);
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {

            dialog.setMessage(activity.getResources().getString(R.string.text_progress));
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+params[0]+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                urlParameters = params[1];

                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                Log.i("GalileoHome","sent: "+urlParameters);
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
            }

        }
        @Override
        protected void onPostExecute(String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    private class update extends AsyncTask<String, Void, String> {
        private ProgressDialog dialog;
        private Activity activity;

        public update(Activity activity) {
            dialog = new ProgressDialog(activity);
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(activity.getResources().getString(R.string.text_progress));
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            StringBuilder total = new StringBuilder();
            try{
                URL serverIp = new URL("http://"+params[0]);
                HttpURLConnection serverConnection = (HttpURLConnection)serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }

                        returnValue = total.toString();
                Log.i("GalileoHome","result: "+returnValue);




            }
            catch(MalformedURLException urlFalse){
                urlFalse.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            return total.toString();
        }
        @Override
        protected void onPostExecute(String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        protected void onCancelled(){
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
