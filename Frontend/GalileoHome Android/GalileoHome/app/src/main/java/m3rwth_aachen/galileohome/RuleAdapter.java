package m3rwth_aachen.galileohome;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by X on 19.06.2015.
 */
class RuleAdapter extends ArrayAdapter<Rule> {

        Context context;
        int layoutResourceId;
        private final List<Rule> list;

        public RuleAdapter(Context context, int layoutResourceId, List<Rule> data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.list=data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            RuleHolder holder = null;

            if(convertView == null)
            {
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.rule_list_item, parent, false);

                holder = new RuleHolder();
                holder.txtTitle = (TextView)convertView.findViewById(R.id.item_title);
                holder.txtSubTitle = (TextView) convertView.findViewById(R.id.item_chosen);
                holder.checkBox = (CheckBox) convertView.findViewById(R.id.ruleItemCheckbox);
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                        final int getPosition = (Integer) buttonView.getTag();
                        list.get(getPosition).setSelected(buttonView.isChecked());
                        String test = list.get(getPosition).getValue();
                        boolean ItemIsChecked = list.get(getPosition).isSelected();
                        if(!test.isEmpty()){
                            switch (test){
                                case "Time":
                                    if(ItemIsChecked){
                                        Calendar mcurrentTime = Calendar.getInstance();
                                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                                        int minute = mcurrentTime.get(Calendar.MINUTE);
                                        TimePickerDialog mTimePicker;
                                        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                                            @Override
                                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                                if(selectedMinute<10){
                                                    list.get(getPosition).setValue(selectedHour + ":" + "0" + selectedMinute);
                                                }
                                                else {
                                                    list.get(getPosition).setValue(selectedHour + ":" + selectedMinute);
                                                }

                                            }
                                        }, hour, minute, true);//Yes 24 hour time
                                        mTimePicker.setTitle(R.string.text_TimePicker);
                                        mTimePicker.show();
                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                                case "Degree":
                                    if(ItemIsChecked){
                                        RelativeLayout linearLayout = new RelativeLayout(context);
                                        final NumberPicker aNumberPicker = new NumberPicker(context);
                                        aNumberPicker.setMaxValue(40);
                                        aNumberPicker.setMinValue(0);

                                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                                        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                                        linearLayout.setLayoutParams(params);
                                        linearLayout.addView(aNumberPicker, numPicerParams);

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                        alertDialogBuilder.setTitle(R.string.text_degree);
                                        alertDialogBuilder.setView(linearLayout);
                                        aNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                                        alertDialogBuilder
                                                .setCancelable(false)
                                                .setPositiveButton(android.R.string.ok,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                list.get(getPosition).setValue(String.valueOf(aNumberPicker.getValue())+" degrees");
                                                            }
                                                        })
                                                .setNegativeButton(android.R.string.cancel,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();

                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                                case "dB":
                                    if(ItemIsChecked){
                                        RelativeLayout linearLayout = new RelativeLayout(context);
                                        final NumberPicker aNumberPicker = new NumberPicker(context);
                                        aNumberPicker.setMaxValue(80);
                                        aNumberPicker.setMinValue(0);
                                        aNumberPicker.setClickable(true);
                                        aNumberPicker.setFocusableInTouchMode(true);
                                        aNumberPicker.setFocusable(true);


                                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                                        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                                        linearLayout.setLayoutParams(params);
                                        linearLayout.addView(aNumberPicker, numPicerParams);

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                        alertDialogBuilder.setTitle(R.string.text_dB);
                                        alertDialogBuilder.setView(linearLayout);
                                        aNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                                        alertDialogBuilder
                                                .setCancelable(false)
                                                .setPositiveButton(android.R.string.ok,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                list.get(getPosition).setValue(String.valueOf(aNumberPicker.getValue())+" dB");

                                                            }
                                                        })
                                                .setNegativeButton(android.R.string.cancel,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();


                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                                case "Lux":
                                    if(ItemIsChecked){
                                        RelativeLayout linearLayout = new RelativeLayout(context);
                                        final NumberPicker aNumberPicker = new NumberPicker(context);
                                        aNumberPicker.setMaxValue(500);
                                        aNumberPicker.setMinValue(0);

                                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                                        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                                        linearLayout.setLayoutParams(params);
                                        linearLayout.addView(aNumberPicker, numPicerParams);

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                        alertDialogBuilder.setTitle(R.string.text_lux);
                                        alertDialogBuilder.setView(linearLayout);
                                        aNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                                        alertDialogBuilder
                                                .setCancelable(false)
                                                .setPositiveButton(android.R.string.ok,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                list.get(getPosition).setValue(String.valueOf(aNumberPicker.getValue())+" Lux");

                                                            }
                                                        })
                                                .setNegativeButton(android.R.string.cancel,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();


                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                                case "Power":
                                    if(ItemIsChecked){
                                        RelativeLayout linearLayout = new RelativeLayout(context);
                                        final NumberPicker aNumberPicker = new NumberPicker(context);
                                        aNumberPicker.setMaxValue(100);
                                        aNumberPicker.setMinValue(0);

                                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                                        RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                        numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

                                        linearLayout.setLayoutParams(params);
                                        linearLayout.addView(aNumberPicker, numPicerParams);

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                        alertDialogBuilder.setTitle(R.string.text_power);
                                        alertDialogBuilder.setView(linearLayout);
                                        aNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                                        alertDialogBuilder
                                                .setCancelable(false)
                                                .setPositiveButton(android.R.string.ok,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                list.get(getPosition).setValue(String.valueOf(aNumberPicker.getValue())+"%");
                                                            }
                                                        })
                                                .setNegativeButton(android.R.string.cancel,
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog,
                                                                                int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();


                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                                case "Color":
                                    if(ItemIsChecked){
                                        HSVColorPickerDialog cpd = new HSVColorPickerDialog(context, 0xFF4488CC,
                                                new HSVColorPickerDialog.OnColorSelectedListener() {
                                                    public void colorSelected(Integer color) {
                                                        list.get(getPosition).setValue(String.format("#%06X", (0xFFFFFF & color)));

                                                    }
                                                });
                                        cpd.setTitle(R.string.text_colorPicker );
                                        cpd.show();
                                    }
                                    else{
                                        list.get(getPosition).setValue(test);
                                    }
                                    break;
                            }
                        }
                    }
                });
                convertView.setTag(holder);
                convertView.setTag(R.id.item_title, holder.txtTitle);
                convertView.setTag(R.id.item_chosen, holder.txtSubTitle);
                convertView.setTag(R.id.ruleItemCheckbox, holder.checkBox);
            }
            else
            {
                holder = (RuleHolder)convertView.getTag();
            }
            holder.checkBox.setTag(position);
            holder.txtTitle.setText(list.get(position).getName());
            holder.txtSubTitle.setText(list.get(position).getValue());
            holder.checkBox.setChecked(list.get(position).isSelected());
            //Rule rule = data[position];
            //holder.txtTitle.setText(rule.title);
            //holder.txtSubTitle.setText(rule.value);

            return convertView;
        }



        public List<Rule> getCheckedItems(){
            List<Rule> checked = new ArrayList<>();
            for(int i=0; i<this.list.size(); i++){
                if(list.get(i).isSelected()){
                    checked.add(list.get(i));
                }
            }
            return checked;
        }

        static class RuleHolder
        {
            TextView txtTitle;
            TextView txtSubTitle;
            CheckBox checkBox;
        }

}
