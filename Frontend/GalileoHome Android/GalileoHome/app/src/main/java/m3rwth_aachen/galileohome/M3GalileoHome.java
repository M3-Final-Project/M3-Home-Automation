package m3rwth_aachen.galileohome;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by johannes trum on 22.06.15.
 */
public class M3GalileoHome extends Application implements BootstrapNotifier {
    private SharedPreferences prefs;
    private String ipAdress;
    private RegionBootstrap regionBootstrap;
    private String returnValue = "";

    @Override
    public void onCreate() {
        super.onCreate();

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress","127.0.0.1"); //Reads from SharedPreferences to determine ipAdress


        if(!ipAdress.equals("127.0.0.1")){
            BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);

            //Own layout for the beacons
            beaconManager.getBeaconParsers().add(new BeaconParser()
                    .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));

            try {
                new update().execute(ipAdress + "/m3server/ibeacon.html").get(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e){
                e.printStackTrace();
            } catch (ExecutionException e){
                e.printStackTrace();
            } catch (TimeoutException e){
                e.printStackTrace();
            }
            Log.i("GalileoHome","beacon id: "+returnValue);

            if(!returnValue.equals("")){
                Identifier id = Identifier.parse(returnValue);

                // Call the enter Redion on the exact match of the id
                Region region = new Region("com.example.myapp.boostrapRegion", id, null, null);
                regionBootstrap = new RegionBootstrap(this, region);
            }

        }

    }

    @Override
    public void didEnterRegion(Region region) {
        Log.e("", "Got a didEnterRegion call ENTA ENTA ENTA ENTA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        new send().execute("state","0");        //ServerCall to unlock the Door

        // This call to disable will make it so the activity below only gets launched the first time a beacon is seen (until the next time the app is launched)
        // if you want the Activity to launch every single time beacons come into view, remove this call.
        //regionBootstrap.disable();

        //Intent intent = new Intent(this, SmartDoorOpener.class);
        // IMPORTANT: in the AndroidManifest.xml definition of this activity, you must set android:launchMode="singleInstance" or you will get two instances
        // created when a user launches the activity manually and it gets launched from here.
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //this.startActivity(intent);

        //Send request to open the door
    }

    @Override
    public void didExitRegion(Region region) {
        Log.e("", "Got a exitRegion call LEAVE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        new send().execute("state","1");        //ServerCall to Lock the Door
    }

    @Override
    public void didDetermineStateForRegion(int i, Region region) {

    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                Log.i("GalileoHome",params[0]);
                if(params[0].equals("state")){
                    urlParameters = "param1=pub&param2=sdo/state&param3="+params[1];
                }
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
            }

        }
    }

    private class update extends AsyncTask<String, Void, String> {



        @Override
        protected String doInBackground(String... params) {
            StringBuilder total = new StringBuilder();
            try{
                URL serverIp = new URL("http://"+params[0]);
                HttpURLConnection serverConnection = (HttpURLConnection)serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");

                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                try{
                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line);
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }

                returnValue = total.toString();
                Log.i("GalileoHome","result: "+returnValue);




            }
            catch(MalformedURLException urlFalse){
                urlFalse.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            return total.toString();
        }

    }
}
