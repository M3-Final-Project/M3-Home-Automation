/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package m3rwth_aachen.galileohome;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class SensorsActivity extends ActionBarActivity {
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private SharedPreferences prefs;
    private String ipAdress;
    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    private boolean stopEverything = false;
    private Runnable updateRunner = new Runnable() {
        public void run() {
            if(!stopEverything){
                new update().execute("temperature");
                new update().execute("brightness");
                new update().execute("volume");
            }

        }
    };
    private boolean firstHit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);

        mDrawerList = (ListView)findViewById(R.id.navList);mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress","127.0.0.1");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_sensors);

        ImageView imageView = (ImageView) findViewById(R.id.sensorsIcon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        imageView = (ImageView) findViewById(R.id.temperatureIcon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        imageView = (ImageView) findViewById(R.id.lightIcon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        imageView = (ImageView) findViewById(R.id.soundIcon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));
        if(Build.VERSION.SDK_INT  > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }
        new update().execute("temperature");
        new update().execute("brightness");
        new update().execute("volume");
        exec.scheduleAtFixedRate(updateRunner, 200, 1000, TimeUnit.MILLISECONDS);
    }

    public void onDestroy(){
        stopEverything = true;
        exec.shutdown();
        super.onDestroy();
    }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this, R.string.text_stopApp, Toast.LENGTH_LONG).show();
        }
    }

    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(SensorsActivity.this, SmartDoorOpener.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 1:
                        intent = new Intent(SensorsActivity.this, SmartilatorActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(SensorsActivity.this, KaffeemaschineActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        intent = new Intent(SensorsActivity.this, AmbilightActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 6:
                        intent = new Intent(SensorsActivity.this, Presets.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 7:
                        intent = new Intent(SensorsActivity.this, RulesActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_sensors);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try{
                URL serverIp = new URL("http://"+ipAdress+"/m3server/sensor/"+params[0]+".html");
                HttpURLConnection serverConnection = (HttpURLConnection)serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                final TextView textView;
                switch (params[0]){
                    case "temperature":
                        textView = (TextView)findViewById(R.id.temperatureValue);
                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.length()!=0 && total!=null){
                                updateTextViews(total,textView, " °C");
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        break;
                    case "brightness":
                        textView = (TextView)findViewById(R.id.lumiosityValueSensor);
                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.length()!=0 && total!=null){
                                updateTextViews(total,textView, " Lux");

                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        break;
                    case "volume":
                        textView = (TextView)findViewById(R.id.soundValue);
                        try{
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.length()!=0 && total!=null){
                                updateTextViews(total,textView," dB");
                            }
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                        break;
                }


            }
            catch(MalformedURLException urlFalse){
                urlFalse.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }

            return "Success";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void updateTextViews(final CharSequence input, final TextView textView, final String add) {
        runOnUiThread(new Runnable() {
            public void run() {
                textView.setText(input+add);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(SensorsActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }
}