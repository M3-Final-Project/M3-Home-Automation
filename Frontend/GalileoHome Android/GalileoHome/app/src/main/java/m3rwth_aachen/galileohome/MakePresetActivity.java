package m3rwth_aachen.galileohome;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.software.shell.fab.ActionButton;

import java.util.ArrayList;
import java.util.List;


public class MakePresetActivity extends ActionBarActivity {
    private SharedPreferences prefs;
    private ListView listOptions;
    private SharedPreferences options;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_preset);


        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        options = this.getSharedPreferences("com.m3rwth_aachen_presets", Context.MODE_PRIVATE);
        setDoneButtonListener();
        setUpList();

        ActionButton actionButton = (ActionButton) findViewById(R.id.button_preset_ready);
        actionButton.setImageResource(R.drawable.ic_done_all_white_24dp);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setTitle(R.string.title_makePreset);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));
        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    private void setUpList(){
        listOptions = (ListView)findViewById( R.id.listView_Presets);
        Resources res = getResources();
        String[] happens = res.getStringArray(R.array.presetOptions);
        ArrayList<Rule> list = new ArrayList<Rule>();
        list.add(new Rule(happens[0], "Power", "smartilatorPower="));
        list.add(new Rule(happens[1], "", "door=1"));
        list.add(new Rule(happens[2], "", "door=0"));
        list.add(new Rule(happens[3], "", "coffee=1"));
        list.add(new Rule(happens[4], "Color", ""));
        RuleAdapter ruleAdapter = new RuleAdapter(this,R.layout.rule_list_item, list);
        listOptions.setAdapter(ruleAdapter);
    }



    private void setDoneButtonListener(){
        ActionButton done = (ActionButton) findViewById(R.id.button_preset_ready);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new readAndWrite().execute();
            }
        });
    }

    private class readAndWrite extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Resources res = getResources();
            String [] caseNames = res.getStringArray(R.array.preset_names);
            RuleAdapter ruleAdapter = (RuleAdapter) listOptions.getAdapter();
            List<Rule> checkedItems = ruleAdapter.getCheckedItems();
            EditText editText = (EditText) findViewById(R.id.edit_name_preset);
            boolean noMatches = true;
            String name = editText.getText().toString();
            int former = options.getInt("preset_count", 0);
            Log.i("GalileoHome",name);
            for(int i=0; i<former; i++)
                if(name.equals(options.getString("preset_name_" + i, null))){
                    noMatches = false;
                }
            if(!checkedItems.isEmpty() && name.matches("[[A-Z][a-z][0-9]]+") && noMatches){
                Intent intent = new Intent(MakePresetActivity.this, Presets.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                SharedPreferences.Editor editor = options.edit();
                editor.putInt("preset_count", former + 1).apply();
                editor.putString("preset_name_"+(former + 1), name).apply();
                Log.i("GalileoHome",String.valueOf(former+1));
                if(!checkedItems.isEmpty()) {
                    String param = "";
                    for (int i = 0; i < checkedItems.size(); i++) {
                        String which = checkedItems.get(i).getInternalname();
                        switch (which) {
                            case "smartilatorPower=":
                                String value = checkedItems.get(i).getValue();
                                param = param+"param1=pub&param2=smartilator/power&param3=" +value.substring(0,value.length()-1) +"?";
                                break;
                            case "door=1":
                                param = param+"param1=pub&param2=sdo/state&param3=1"+"?";
                                break;
                            case "door=0":
                                param = param+"param1=pub&param2=sdo/state&param3=0"+"?";
                                break;
                            case "coffee=1":
                                param = param+"param1=pub&param2=coffeemachine&param3=1"+"?";
                                break;
                            case "":
                                param = param+"param1=pub&param2=ambilight/color&param3=" + checkedItems.get(i).getValue().substring(1)+"?";
                                break;
                        }
                    }
                    Log.i("GalileoHome",param);
                    SharedPreferences.Editor edit = options.edit();
                    int z = former+1;
                    edit.putString("preset_command_"+z, param).apply();
                    edit.apply();
                }
                startActivity(intent);
                finish();
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MakePresetActivity.this, R.string.toast_text_no_name, Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return "Success";
        }
    }



}
