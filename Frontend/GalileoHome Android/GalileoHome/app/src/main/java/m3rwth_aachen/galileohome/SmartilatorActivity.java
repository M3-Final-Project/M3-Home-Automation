/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package m3rwth_aachen.galileohome;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class SmartilatorActivity extends ActionBarActivity {
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    private SharedPreferences prefs;
    private String ipAdress;
    private boolean updatePending;
    private boolean firstStart1;
    private boolean firstStart2;
    private boolean firstStart3;
    private boolean stopEverything = false;
    private Runnable updateRunner = new Runnable() {
        public void run() {
            if(!updatePending && !stopEverything){
                new update().execute("power");
                new update().execute("system_rule1");
                new update().execute("system_rule2");
                new update().execute("system_rule3");
                //Log.i("GalileoHome", "update update update!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }
    };
    private boolean firstHit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smartilator);

        mDrawerList = (ListView)findViewById(R.id.navList);mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();
        setPowerButtonListener();
        setFilters();
        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress","127.0.0.1");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_smartilator);

        ImageView imageView = (ImageView) findViewById(R.id.smartilatorIcon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));
        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }
        firstStart1 = true;
        firstStart2 = true;
        firstStart3 = true;





        setSwitchClickListener1();
        setSwitchClickListener2();
        setSwitchClickListener3();

        new update().execute("power");
        new update().execute("system_rule1");
        new update().execute("system_rule2");
        new update().execute("system_rule3");
        setButtonListenerPowerField();
        setPowerLevelClickListener();

        updatePending = false;
        exec.scheduleAtFixedRate(updateRunner, 200, 3000, TimeUnit.MILLISECONDS);

    }

    public void onDestroy(){
        stopEverything = true;
        exec.shutdown();
        super.onDestroy();

    }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this, R.string.text_stopApp, Toast.LENGTH_LONG).show();
        }
    }


    private void setFilters(){
        EditText edit = (EditText) findViewById(R.id.currentPowerLevel);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("0","100")});
        edit = (EditText) findViewById(R.id.fromDegreeValue1);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("-40","60")});
        edit = (EditText) findViewById(R.id.fromDegreeValue2);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("-40","60")});
        edit = (EditText) findViewById(R.id.fromDegreeValue3);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("-40","60")});
        edit = (EditText) findViewById(R.id.PowerValue1);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("0","100")});
        edit = (EditText) findViewById(R.id.PowerValue2);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("0","100")});
        edit = (EditText) findViewById(R.id.PowerValue3);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("0", "100")});
    }



    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(SmartilatorActivity.this, SmartDoorOpener.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(SmartilatorActivity.this, KaffeemaschineActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 3:
                        intent = new Intent(SmartilatorActivity.this, AmbilightActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        intent = new Intent(SmartilatorActivity.this, SensorsActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;

                    case 6:
                        intent = new Intent(SmartilatorActivity.this, Presets.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                    case 7:
                        intent = new Intent(SmartilatorActivity.this, RulesActivity.class);
                        stopEverything = true;
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_smartilator);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(SmartilatorActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL serverIp = new URL("http://"+ipAdress+"/m3server/smartilator/" + params[0] + ".html");
                HttpURLConnection serverConnection = (HttpURLConnection) serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                final EditText editText;
                switch (params[0]) {
                    case "power":
                        editText = (EditText) findViewById(R.id.currentPowerLevel);
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if (total.length() != 0 && total != null) {
                                //total.append("%");
                                updateTextViews(total, editText);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule1":

                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                    total.append(line);
                            }
                            if(total.charAt(0)=='0'){
                                updateToggles(false, 1);
                            }
                            if (total.length() != 0 && total.charAt(0) == '1') {
                                if(total.indexOf("?")!=-1){
                                    String partFrom = total.substring(2,(total.lastIndexOf("?")));
                                    String partTo = total.substring(total.lastIndexOf("?")+1,total.length());
                                    updateSystemRuleFields(partFrom, 1,1);
                                    updateSystemRuleFields(partTo, 1,2);
                                }
                            }
                            else{
                                updateToggles(false, 1);
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule2":

                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.charAt(0)=='0'){
                                updateToggles(false, 2);
                            }
                            else if (total.length() != 0 && total.charAt(0) == '1') {
                                if(total.indexOf("?")!=-1){
                                    String partFrom = total.substring(2,(total.lastIndexOf("?")));
                                    String partTo = total.substring(total.lastIndexOf("?")+1,total.length());
                                    updateSystemRuleFields(partFrom, 2,1);
                                    updateSystemRuleFields(partTo, 2,2);
                                }
                            }
                            else{
                                updateToggles(false, 2);
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule3":

                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if(total.charAt(0)=='0'){
                                updateToggles(false, 3);
                            }
                            else if (total.length() != 0 && total.charAt(0) == '1') {
                                if(total.indexOf("?")!=-1){
                                        String partFrom = total.substring(2,(total.lastIndexOf("?")));
                                        String partTo = total.substring(total.lastIndexOf("?")+1,total.length());
                                        updateSystemRuleFields(partFrom, 3,1);
                                        updateSystemRuleFields(partTo, 3,2);
                                }
                            }
                            else {
                                updateToggles(false, 3);
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }


            } catch (MalformedURLException urlFalse) {
                urlFalse.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "Success";
        }
    }

    private void updateTextViews(final CharSequence input, final TextView textView) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (input.charAt(0)=='0'){
                    setPowerButtonText(false);
                }
                else{
                    setPowerButtonText(true);
                }
                textView.setText(input);
            }
        });
    }

    private void updateToggles(final boolean state, final int which){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Switch switchRule;
                switch (which){
                    case 1:
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable1);
                        switchRule.setChecked(state);
                        break;
                    case 2:
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable2);
                        switchRule.setChecked(state);
                        break;
                    case 3:
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable3);
                        switchRule.setChecked(state);
                        break;
                }
            }
        });

    }
    private void updateSystemRuleFields(final CharSequence input, final int number, final int which) {
        runOnUiThread(new Runnable() {
            public void run() {
                EditText editText;
                Switch switchRule;
                switch (number) {
                    case 1:
                        if (which == 1) {
                            editText = (EditText) findViewById(R.id.fromDegreeValue1);
                        } else {
                            editText = (EditText) findViewById(R.id.PowerValue1);
                        }
                        editText.setText(input);
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable1);
                        if (!switchRule.isChecked()) {
                            switchRule.toggle();
                        }
                        break;
                    case 2:
                        if (which == 1) {
                            editText = (EditText) findViewById(R.id.fromDegreeValue2);
                        } else {
                            editText = (EditText) findViewById(R.id.PowerValue2);
                        }
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable2);
                        if (!switchRule.isChecked()) {
                            switchRule.toggle();
                        }
                        editText.setText(input);
                        break;
                    case 3:
                        if (which == 1) {
                            editText = (EditText) findViewById(R.id.fromDegreeValue3);
                        } else {
                            editText = (EditText) findViewById(R.id.PowerValue3);
                        }
                        switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable3);
                        if (!switchRule.isChecked()) {
                            switchRule.toggle();
                        }
                        editText.setText(input);
                        break;
                }
            }
        });
    }

    private void setButtonListenerPowerField(){
        final TextView powerField = (EditText) findViewById(R.id.currentPowerLevel);
        powerField.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            if (!powerField.getText().toString().isEmpty()) {
                                new send().execute("power", powerField.getText().toString());
                            } else {
                                new send().execute("power", "0");
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }


    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "";
                Log.i("GalileoHome",params[0]);
                if(params[0].equals("power")){
                    urlParameters = "param1=pub&param2=smartilator/power&param3="+params[1];
                }else if(params[1].equals("0")){
                    urlParameters = "param1=pub&param2=smartilator/"+params[0]+"&param3=0?";
                }
                else {
                    urlParameters = "param1=pub&param2=smartilator/"+params[0]+"&param3="+params[1]+params[2]+"?"+params[3];
                }
                Log.i("GalileoHome", urlParameters);
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");

                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();
                }
                updatePending = false;
            }

        }
    }


    private void setPowerLevelClickListener(){
        EditText editText = (EditText) findViewById(R.id.currentPowerLevel);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_UP == event.getAction()) {
                    updatePending = true;
                }
                return false; // return is important...
            }
        });
    }

    private void setSwitchClickListener1(){
        final Switch switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable1);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!firstStart1){
                    updatePending = true;
                    EditText from = (EditText) findViewById(R.id.fromDegreeValue1);
                    EditText to = (EditText) findViewById(R.id.PowerValue1);
                    if(!from.getText().toString().isEmpty() && !to.getText().toString().isEmpty()){
                        if (isChecked) {
                            new send().execute("system_rule1","1?", from.getText().toString(), to.getText().toString());
                        } else {
                            new send().execute("system_rule1", "0?",from.getText().toString(), to.getText().toString());
                            //updateToggles(false, 1);
                        }
                    } else {
                        new send().execute("system_rule1", "0?","0","0");
                    }

                }
                firstStart1 = false;
                //updatePending = false;
            }
        });
    }
    private void setSwitchClickListener2(){
        final Switch switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable2);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!firstStart2){
                    updatePending = true;
                    EditText from = (EditText) findViewById(R.id.fromDegreeValue2);
                    EditText to = (EditText) findViewById(R.id.PowerValue2);
                    if(!from.getText().toString().isEmpty() && !to.getText().toString().isEmpty()){
                        if (isChecked) {
                            new send().execute("system_rule2","1?", from.getText().toString(), to.getText().toString());
                        } else {
                            new send().execute("system_rule2", "0?",from.getText().toString(), to.getText().toString());
                            //updateToggles(false, 1);
                        }
                    } else {
                        new send().execute("system_rule2", "0?","0","0");
                    }
                }
                firstStart2 = false;

                //updatePending = false;
            }
        });
    }


    private void setSwitchClickListener3(){
        final Switch switchRule = (Switch) findViewById(R.id.smartilatorSwitchEnable3);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!firstStart3){
                    updatePending = true;
                    EditText from = (EditText) findViewById(R.id.fromDegreeValue3);
                    EditText to = (EditText) findViewById(R.id.PowerValue3);
                    if(!from.getText().toString().isEmpty() && !to.getText().toString().isEmpty()){
                        if (isChecked) {
                            new send().execute("system_rule3","1?", from.getText().toString(), to.getText().toString());
                        } else {
                            new send().execute("system_rule3", "0?",from.getText().toString(), to.getText().toString());
                            //updateToggles(false, 1);
                        }
                    } else {
                        new send().execute("system_rule3", "0?","0","0");
                    }
                }
                firstStart3 = false;
                //updatePending = false;
            }
        });
    }

    private void setPowerButtonListener(){
        final Button info = (Button) findViewById(R.id.button_power_switch_smartilator);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(info.getText().toString().equals(getString(R.string.button_text_off))){
                    new send().execute("power", "0");
                }
                else if(info.getText().toString().equals(getString(R.string.button_text_on))){
                    new send().execute("power", "50");
                }
                //setPowerButtonText(false);
            }
        });
    }

    private void setPowerButtonText(boolean power){
        Button powerButton = (Button)findViewById(R.id.button_power_switch_smartilator);
        if(power){
            powerButton.setText(R.string.button_text_off);
        }
        else{
            powerButton.setText(R.string.button_text_on);
        }
    }
}