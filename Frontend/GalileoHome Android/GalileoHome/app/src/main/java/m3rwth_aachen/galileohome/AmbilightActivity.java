/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package m3rwth_aachen.galileohome;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


public class AmbilightActivity extends ActionBarActivity  {
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";
    private EditText fromTime;
    private EditText toTime;
    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    private boolean updatePending;
    private boolean firstStart2;
    private boolean firstStart3;
    private SharedPreferences prefs;
    private String ipAdress;
    private boolean stopEverything = false;
    private boolean firstHit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambilight);

        mDrawerList = (ListView)findViewById(R.id.navList);mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);


        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(R.string.title_Ambilight);

        prefs = this.getSharedPreferences("com.m3rwth_aachen", Context.MODE_PRIVATE);
        ipAdress = prefs.getString("ipAdress", "127.0.0.1");


        setButtonListenerHexField();
        setFromTimeListener();
        setToTimeListener();

        EditText edit = (EditText) findViewById(R.id.lumiosityValue);
        edit.setFilters(new InputFilter[]{new InputFilterMinMax("0","100")});

        ImageView imageView = (ImageView) findViewById(R.id.ambilight_icon);
        imageView.setColorFilter(Color.argb(255, 85, 85, 85));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2484ad")));

        if(Build.VERSION.SDK_INT > 20){
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#1B6382"));
        }

        setColorPickerButtonListener();
        setClickListener();

        updatePending = false;
        firstStart2 = true;
        firstStart3 = true;

        setSwitchClickListener1();
        setSwitchClickListener2();
        setSwitchClickListener3();


        Runnable updateRunner = new Runnable() {
            public void run() {
                if(!updatePending && !stopEverything){
                    new update().execute("color");
                    new update().execute("system_rule_door");
                    new update().execute("system_rule_brightness");
                    new update().execute("system_rule_time");
                }
            }
        };
        exec.scheduleAtFixedRate(updateRunner, 500, 3000, TimeUnit.MILLISECONDS);




    }
     public void onDestroy(){
         exec.shutdown();
         super.onDestroy();

     }

    public void onBackPressed(){
        if(firstHit){
            super.onBackPressed();
        }
        else {
            firstHit = true;
            Toast.makeText(this,R.string.text_stopApp,Toast.LENGTH_LONG).show();
        }
    }


    private void setButtonListenerHexField(){
        TextView hexString = (EditText) findViewById(R.id.colorString);
        hexString.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            changeColorSample();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void addDrawerItems() {
        Resources res = getResources();
        String[] controllables = res.getStringArray(R.array.things);
        mAdapter = new ArrayAdapter<String>(this, R.layout.custom_list_item, controllables);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        intent = new Intent(AmbilightActivity.this, SmartDoorOpener.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 1:
                        intent = new Intent(AmbilightActivity.this, SmartilatorActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 2:
                        intent = new Intent(AmbilightActivity.this, KaffeemaschineActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        intent = new Intent(AmbilightActivity.this, SensorsActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 6:
                        intent = new Intent(AmbilightActivity.this, Presets.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case 7:
                        intent = new Intent(AmbilightActivity.this, RulesActivity.class);
                        stopEverything = true;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
            }

        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.title_naviagtion_drawer);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.title_Ambilight);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(AmbilightActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        // Activate the navigation drawer toggle
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    private void colorChanged(int color) {          //Updates the ColorExaple Field and writes the
                                                    //Color in Hex
        ImageView colorView = (ImageView)findViewById(R.id.colorView);
        colorView.setBackgroundColor(color);
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        TextView colorString = (TextView) findViewById(R.id.colorString);
        colorString.setText(hexColor);
        String rgbColor = hexColor.substring(1);
        new send().execute("color",rgbColor);

    }

    private void changeColorSample (){      //Reads from the HexColorField and converts the Hex to
                                            //a Color and Updates the ColorExampleField
        TextView colorString = (TextView) findViewById(R.id.colorString);
        String hexColor = colorString.getText().toString();
        if(colorString.length()!=0 &&  hexColor.matches(HEX_PATTERN)){  //Checks if HexString is Hex
            ImageView colorView = (ImageView)findViewById(R.id.colorView);
            colorView.setBackgroundColor(Color.parseColor(hexColor));
            String rgbColor = hexColor.substring(1);
            new send().execute("color",rgbColor);
        }
    }



    private void setColorPickerButtonListener(){
        Button colorSelect = (Button) findViewById(R.id.colorSelectButton);
        colorSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view){
                HSVColorPickerDialog cpd = new HSVColorPickerDialog( AmbilightActivity.this, 0xFF4488CC,
                        new HSVColorPickerDialog.OnColorSelectedListener() {
                            public void colorSelected(Integer color) {
                                colorChanged(color);
                            }
                        });
                cpd.setTitle(R.string.text_colorPicker );
                cpd.show();

            }
        });
    }

    private void setFromTimeListener(){
        fromTime = (EditText)findViewById(R.id.fromValue);
        fromTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AmbilightActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if(selectedMinute<10){
                            fromTime.setText(selectedHour + ":" + "0" + selectedMinute);
                        }
                        else {
                            fromTime.setText(selectedHour + ":" + selectedMinute);
                        }

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(R.string.text_TimePicker);
                mTimePicker.show();

            }
        });
    }

    private void setToTimeListener(){
        toTime = (EditText)findViewById(R.id.toValue);
        toTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AmbilightActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (selectedMinute < 10) {
                            toTime.setText(selectedHour + ":" + "0" + selectedMinute);
                        } else {
                            toTime.setText(selectedHour + ":" + selectedMinute);
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(R.string.text_TimePicker);
                mTimePicker.show();

            }
        });
    }

    private class update extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL serverIp = new URL("http://"+ipAdress+"/m3server/ambilight/" + params[0] + ".html");
                HttpURLConnection serverConnection = (HttpURLConnection) serverIp.openConnection();
                serverConnection.setDoOutput(true);
                serverConnection.setRequestMethod("POST");
                serverConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                serverConnection.setConnectTimeout(1000);
                serverConnection.setReadTimeout(1000);
                InputStream in = new BufferedInputStream(serverConnection.getInputStream());
                final TextView textView;
                switch (params[0]) {
                    case "color":
                        textView = (TextView) findViewById(R.id.colorString);
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if (total.length() != 0) {
                                total.insert(0, "#");
                                updateTextViews(total, textView);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule_brightness":
                        textView = (TextView) findViewById(R.id.lumiosityValue);
                        final Switch aSwitch = (Switch) findViewById(R.id.byLumiositySwitch);
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if (total.length() != 0 && total.charAt(0) == '1') {
                                String parameter = total.substring(2);
                                updateTextViews(parameter, textView);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        aSwitch.setChecked(true);
                                    }
                                });
                            }
                            else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        aSwitch.setChecked(false);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule_door":
                        final Switch bSwitch = (Switch) findViewById(R.id.whenDoorOpensSwitch);
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if (total.length() != 0 && total.charAt(0) == '1') {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        bSwitch.setChecked(true);
                                    }
                                });
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        bSwitch.setChecked(false);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "system_rule_time":
                        final TextView textViewFrom = (TextView) findViewById(R.id.fromValue);
                        final EditText editTextTo = (EditText) findViewById(R.id.toValue);
                        try {
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            final Switch cSwitch = (Switch) findViewById(R.id.fromWhenTowhen);
                            String line;
                            while ((line = r.readLine()) != null) {
                                total.append(line);
                            }
                            if (total.length() != 0 && total.charAt(0) == '1') {
                                final String from = total.substring(2,total.lastIndexOf("?"));
                                final String fromTime = from.substring(0,from.length()-2)+":"+from.substring(from.length()-2,from.length());
                                final String to = total.substring(total.lastIndexOf("?")+1,total.length());
                                final String toTime = to.substring(0,to.length()-2)+":"+to.substring(to.length()-2,to.length());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cSwitch.setChecked(true);
                                        textViewFrom.setText(fromTime);
                                        editTextTo.setText(toTime);
                                    }
                                });
                            }
                            else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cSwitch.setChecked(false);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }


            } catch (MalformedURLException urlFalse) {
                urlFalse.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "Success";
        }
    }

    private void updateTextViews(final CharSequence input, final TextView textView) {
        runOnUiThread(new Runnable() {
            public void run() {
                textView.setText(input);
                TextView colorString = (TextView) findViewById(R.id.colorString);
                String hexColor = colorString.getText().toString();
                ImageView colorView = (ImageView)findViewById(R.id.colorView);
                colorView.setBackgroundColor(Color.parseColor(hexColor));
            }
        });
    }

    private class send extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection connection = null;
            try {
                //Create connection
                url = new URL("http://"+ipAdress+":8080");
                connection = (HttpURLConnection)url.openConnection();
                connection.setRequestMethod("POST");
                connection.setConnectTimeout(1000);
                connection.setReadTimeout(1000);
                connection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded");
                String urlParameters = "param1=pub&param2=ambilight/"+params[0]+"&param3="+params[1];
                connection.setRequestProperty("Content-Length", "" +
                        Integer.toString(urlParameters.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches (false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                Log.i("GalileoHome","ich mache send");
                wr.writeBytes (urlParameters);
                wr.flush ();
                wr.close ();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuilder response = new StringBuilder();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                Log.i("GalileoHome",response.toString());
                return response.toString();

            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {

                if(connection != null) {
                    connection.disconnect();

                }
                updatePending = false;
            }

        }
    }

    private void setClickListener(){
        EditText editText = (EditText) findViewById(R.id.colorString);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_UP == event.getAction()) {
                    updatePending = true;
                }
                return false; // return is important...
            }
        });
    }

    private void setSwitchClickListener1(){
        final Switch switchRule = (Switch) findViewById(R.id.whenDoorOpensSwitch);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updatePending = true;
                if (isChecked) {
                    new send().execute("system_rule_door", "1");
                } else {
                    new send().execute("system_rule_door", "0");
                }

            }
        });
    }


    private void setSwitchClickListener2(){
        final Switch switchRule = (Switch) findViewById(R.id.byLumiositySwitch);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!firstStart2){
                    updatePending = true;
                    EditText from = (EditText) findViewById(R.id.lumiosityValue);
                    if (isChecked && !from.getText().toString().isEmpty()) {
                        new send().execute("system_rule_brightness","1?"+from.getText().toString());
                    } else if (!isChecked && !from.getText().toString().isEmpty()){
                        new send().execute("system_rule_brightness","0?"+from.getText().toString());
                    } else {
                        new send().execute("system_rule_brightness","0?"+"0");
                    }
                }
                firstStart2 = false;
            }
        });
    }

    private void setSwitchClickListener3(){
        final Switch switchRule = (Switch) findViewById(R.id.fromWhenTowhen);
        switchRule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!firstStart3){
                    updatePending = true;
                    EditText from = (EditText) findViewById(R.id.fromValue);
                    EditText to = (EditText) findViewById(R.id.toValue);
                    if(!from.getText().toString().isEmpty() && !to.getText().toString().isEmpty()){
                        StringBuilder fromTime = new StringBuilder();
                        fromTime.append(from.getText().toString());
                        fromTime.delete(fromTime.lastIndexOf(":"), fromTime.lastIndexOf(":") + 1);
                        StringBuilder toTime = new StringBuilder();
                        toTime.append(to.getText().toString());
                        toTime.delete(toTime.lastIndexOf(":"),toTime.lastIndexOf(":")+1);
                        if(toTime.length()==3){
                            toTime.insert(0,'0');
                        }
                        if(fromTime.length()==3){
                            fromTime.insert(0,'0');
                        }
                        if (isChecked) {
                            new send().execute("system_rule_time","1?"+fromTime+"?"+toTime);
                        } else {
                            new send().execute("system_rule_time","0?"+fromTime+"?"+toTime);
                        }
                    }
                    updatePending = false;
                }
                firstStart3 = false;
            }
        });
    }



}