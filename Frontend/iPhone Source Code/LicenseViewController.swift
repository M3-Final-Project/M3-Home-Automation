//
//  LicenseViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 30/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class LicenseViewController: UIViewController {

    @IBOutlet weak var internationalTextView: UITextView!
    @IBOutlet weak var germanLicenseTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        internationalTextView.hidden = true
        germanLicenseTextView.hidden = true
        let locale = NSLocale.preferredLanguages()[0] as! String
        if locale == "de" {
            germanLicenseTextView.hidden = false
        } else {
            internationalTextView.hidden = false
        }
    }
}
