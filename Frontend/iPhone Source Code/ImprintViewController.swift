//
//  ImprintViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 30/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class ImprintViewController: UIViewController {
    
    
    @IBOutlet weak var internationalTextView: UITextView!
    @IBOutlet weak var germanTextView: UITextView!
    
    let disclamerLink = "http://www.rwth-aachen.de/cms/main/root/Footer/Service/~cesv/Disclaimer/?lidx=1"
    var disclamerButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        internationalTextView.hidden = true
        germanTextView.hidden = true
        let locale = NSLocale.preferredLanguages()[0] as! String
        if locale == "de" {
            germanTextView.hidden = false
        } else {
            internationalTextView.hidden = false
        }
        
        let rect = UIScreen.mainScreen().bounds
        disclamerButton.setTitle("Disclamer", forState: .Normal)
        disclamerButton.frame = CGRectMake(rect.size.width/2 - 82, 450 , 164, 30)
        disclamerButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        disclamerButton.backgroundColor = Constants.highlightColor1
        disclamerButton.layer.cornerRadius = 15.0
        disclamerButton.addTarget(self, action: Selector("openDisclamer"), forControlEvents: .TouchUpInside)
        self.view.addSubview(disclamerButton)
    }
    
    
    
    dynamic private func openDisclamer() {
        let disclamerURL = NSURL(string: disclamerLink)
        if let url = disclamerURL {
            UIApplication.sharedApplication().openURL(url)
        }
    }
}
