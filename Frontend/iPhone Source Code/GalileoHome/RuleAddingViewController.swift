//
//  RuleAddingViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 17/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//


import UIKit


class RuleAddingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    var preset : Preset = Preset.currentPreset.copyPreset()
    var selectedActions : [Bool] = [false, false, false, false, false]
    var actions : [String] = ["MoodLight".localized,
        "Open door".localized,
        "Close door".localized,
        "Make a coffee".localized,
        "Smartilator".localized]
    var checkmarkCount = 4
    var datePicker = UIDatePicker(frame: CGRectZero)
    var picker = UIPickerView()
    var timeTextField : UITextField? = nil
    var ventilatorPercent = UIButton()
    var oldName = ""
    
    var selectedEvents : [Bool] = [false, false, false, false, false, false, false, false, false, false, false]
    var events : [String] = ["Light turns on".localized,
        "Door opens".localized,
        "Door closes".localized,
        "Make a coffee".localized,
        "Temperature exceeds".localized,
        "Temperature below".localized,
        "Volume exceeds".localized,
        "Volume below".localized,
        "Brightness exceeds".localized,
        "Brightness below".localized,
        "Time".localized]
    var editMode = false
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var tableViewEvents: UITableView!
    @IBOutlet weak var tableViewActions: UITableView!
    
    
    override func viewDidLoad() {
        //Set up background color and navigation bar
        self.view.backgroundColor = Constants.backgroundColor
        var cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancelPressed"))
        var doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("donePressed"))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        
        //Set up table views
        tableViewEvents.backgroundColor = Constants.backgroundColor
        tableViewEvents.separatorColor = Constants.backgroundColor
        tableViewActions.backgroundColor = Constants.backgroundColor
        tableViewActions.separatorColor = Constants.backgroundColor
        
        //Default title
        preset.name = "Rule".localized + " " + "\(PresetStore.ruleStore.idCount)"
        
        //Set up text field
        nameTextField.returnKeyType = UIReturnKeyType.Done
        nameTextField.adjustsFontSizeToFitWidth = true
        
        //Check if we are editing or editing
        if Constants.ruleToEdit != nil {
            preset = Constants.ruleToEdit!
            //Constants.ruleToEdit = nil
            nameTextField.text = preset.name
            editMode = true
            selectedActions = [preset.lightTurnOnAction, preset.doorOpenAction, preset.doorCloseAction, preset.shouldMakeCoffee, preset.ventilatorPowerActionB]
            
            oldName = preset.name
            
            var dateStr = ""
            if (preset.time != nil){
                var dateFormat = NSDateFormatter()
                dateFormat.dateFormat = "hh:mm"
                dateStr = dateFormat.stringFromDate(preset.time!)
            }
            selectedEvents = [preset.lightTurnedOnEvent, preset.doorOpenEvent, preset.doorCloseEvent, preset.coffeeMade,count(preset.temperatureExceeds)>0,count(preset.temperatureBelow)>0,count(preset.volumeExceeds)>0,count(preset.volumeBelow)>0,count(preset.brightnessExceeds)>0,count(preset.brightnessBelow)>0,count(dateStr)>0]
            self.title = "Edit rule".localized
        }
    }
    
    func cancelPressed() {
        preset.name = oldName
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func donePressed() {
        var checkActions = false
        var checkEvents = false
        
        for action in selectedActions{
            if action == true {
                checkActions = true
            }
        }
        for event in selectedEvents{
            if event == true {
                checkEvents = true
            }
        }
        if checkEvents == false {
            var alert = UIAlertController(title: "Oops...".localized, message: "You have no events selected.".localized, preferredStyle: .Alert)
            var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        if checkActions == false {
            var alert = UIAlertController(title: "Oops...".localized, message: "You have no actions selected.".localized, preferredStyle: .Alert)
            var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        preset.lightTurnOnAction = selectedActions[0]
        preset.doorOpenAction = selectedActions[1]
        preset.doorCloseAction = selectedActions[2]
        preset.shouldMakeCoffee = selectedActions[3]
        preset.ventilatorPowerActionB = selectedActions[4]
        
        preset.lightTurnedOnEvent = selectedEvents[0]
        preset.doorOpenEvent = selectedEvents[1]
        preset.doorCloseEvent = selectedEvents[2]
        preset.coffeeMade = selectedEvents[3]
        
        if (Constants.ruleToAdd == nil) {
            Constants.ruleToAdd = []
        }

        if editMode {
            var newPreset:Preset = preset.copyAll()
            preset.sendRule("delete")
            //Constants.ruleToDelete?.append(preset)
            newPreset.sendRule("add")
            //Constants.ruleToAdd!.append(newPreset)
            if let i = PresetStore.ruleStore.indexOfItem(preset) {
                PresetStore.ruleStore.replaceItem(i, newItem: newPreset)
                Constants.ruleToEdit = newPreset
            }

        } else {
            preset.sendRule("add")
            Constants.ruleToAdd!.append(self.preset)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func addDoneButtonOnKeyboard(view: UIView?) {
        ///Return a 1x1 image with translucent highlightColor1
        func pixelImage() -> UIImage {
            //One pixel rect
            let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            
            CGContextSetFillColorWithColor(context, Constants.highlightColor1.colorWithAlphaComponent(0.8).CGColor)
            CGContextFillRect(context, rect)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image
        }
        
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.Black
        doneToolbar.setBackgroundImage(pixelImage(),
                                        forToolbarPosition: UIBarPosition.Bottom,
                                        barMetrics: UIBarMetrics.Default)
        //let tintColor = Constants.highlightColor1.colorWithAlphaComponent(0.5)
        doneToolbar.barTintColor = Constants.highlightColor1
        doneToolbar.translucent = true
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: view, action: "resignFirstResponder")
        done.tintColor = UIColor.whiteColor()
        var items = [AnyObject]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if let accessorizedView = view as? UITextView {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        } else if let accessorizedView = view as? UITextField {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        }
    }
    
    func colorToHEX(color : UIColor) -> String {
        //Get RGB values from color
        var getRed = CGFloat()
        var getGreen = CGFloat()
        var getBlue = CGFloat()
        var getAlpha = CGFloat()
        color.getRed(&getRed, green: &getGreen, blue: &getBlue, alpha: &getAlpha)
        
        //Convert RGB values to HEX
        var redHexString = String(NSString(format:"%2X", Int(getRed*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var blueHexString = String(NSString(format:"%2X", Int(getGreen*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var greenHexString = String(NSString(format:"%2X", Int(getBlue*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        //If only 1 char long -> Prefix with 0 to get 2 chars for each color component
        if count(redHexString) == 1{
            redHexString = "0" + redHexString
        }
        if count(blueHexString) == 1{
            blueHexString = "0" + blueHexString
        }
        if count(greenHexString) == 1{
            greenHexString = "0" + greenHexString
        }
        
        //Merge to one single string
        let colorHEX : String = redHexString + blueHexString + greenHexString
        
        return colorHEX
    }
    
    // MARK: - Table View Delegate / Data Source
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = nil
        if indexPath.row >= checkmarkCount && tableView == tableViewEvents {
            //no reuse for cells with textfield
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        }
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
            cell!.tintColor = Constants.highlightColor1
        }
        
        if tableView == tableViewEvents {
            if indexPath.row < checkmarkCount {
                if selectedEvents[indexPath.row] {
                    cell?.accessoryType = .Checkmark
                } else {
                    cell?.accessoryType = .None
                }
            }
            
            
            if indexPath.row >= checkmarkCount {
                //Add text field
                var textField = UITextField(frame: CGRectMake(100, 10, UIScreen.mainScreen().bounds.width-145, 30))
                textField.adjustsFontSizeToFitWidth = true
                textField.autocorrectionType = .No
                textField.textAlignment = .Right
                textField.delegate = self
                textField.tag = indexPath.row
                textField.keyboardType = UIKeyboardType.NumberPad
                textField.returnKeyType = UIReturnKeyType.Done
                addDoneButtonOnKeyboard(textField)
                
                switch indexPath.row {
                case checkmarkCount:
                    textField.placeholder = "°C"
                    textField.text = preset.temperatureExceeds
                case checkmarkCount+1:
                    textField.placeholder = "°C"
                    textField.text = preset.temperatureBelow
                case checkmarkCount+2:
                    textField.placeholder = "dB"
                    textField.text = preset.volumeExceeds
                case checkmarkCount+3:
                    textField.placeholder = "dB"
                    textField.text = preset.volumeBelow
                case checkmarkCount+4:
                    textField.placeholder = "Lux"
                    textField.text = preset.brightnessExceeds
                case checkmarkCount+5:
                    textField.placeholder = "Lux"
                    textField.text = preset.brightnessBelow
                case checkmarkCount+6:
                    textField.placeholder = "12:00"
                    if (preset.time != nil){
                        var dateFormat = NSDateFormatter()
                        dateFormat.dateFormat = "hh:mm"
                        textField.text = dateFormat.stringFromDate(preset.time!)
                    }
                default: break
                }
                
                let values : [AnyObject?] = [preset.temperatureExceeds, preset.temperatureBelow, preset.volumeExceeds, preset.volumeBelow, preset.brightnessExceeds, preset.brightnessBelow, preset.time]
                
                if let val: AnyObject = values[indexPath.row - checkmarkCount] {
                    textField.text = "\(val)"
                    if indexPath.row == checkmarkCount+6 {
                        var dateFormat = NSDateFormatter()
                        dateFormat.dateFormat = "hh:mm"
                        textField.text = dateFormat.stringFromDate(val as! NSDate)
                        
                    }
                }
                
                cell?.addSubview(textField)
            }
            
            cell!.textLabel?.text = events[indexPath.row]
        } else if tableView == tableViewActions {
            if selectedActions[indexPath.row] {
                cell?.accessoryType = .Checkmark
            } else {
                cell?.accessoryType = .None
            }
            
            cell!.textLabel?.text = actions[indexPath.row]
            
            if actions[indexPath.row] == "MoodLight" {
                let x : CGFloat = UIScreen.mainScreen().bounds.width-100
                let y : CGFloat = 10
                let width : CGFloat = cell!.frame.height-20
                let height : CGFloat = cell!.frame.height-20
                var colorField = UITextField(frame: CGRectMake(x, y, width, height))
                var color: UIColor = UIColor()
                var lightHue:CGFloat = 0.0
                var lightSaturation:CGFloat = 0.0
                var lightBrightness:CGFloat = 0.0
                
                if editMode && (count(preset.lightActionHex)>0) {
                    var rgbVal : UInt32 = 0
                    //Scan for hex string
                    let scanner: AnyObject = NSScanner.localizedScannerWithString(preset.lightActionHex)
                    
                    //If found, parse
                    if scanner.scanHexInt(&rgbVal) {
                        color = UIColor(red: CGFloat((rgbVal & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbVal & 0x00FF00) >> 8)/255.0, blue: CGFloat((rgbVal & 0x0000FF))/255.0, alpha: 1.0)
                        var alpha: CGFloat = 1.0
                        color.getHue(&lightHue, saturation: &lightSaturation, brightness: &lightBrightness, alpha: &alpha)
                    }
                }else {
                    lightHue = Preset.currentPreset.lightHue
                    lightSaturation = Preset.currentPreset.lightSaturation
                    lightBrightness = Preset.currentPreset.lightBrightness
                    color = UIColor(hue: lightHue, saturation: lightSaturation, brightness: lightBrightness, alpha: 1.0)
                }
                
                preset.lightActionHex = colorToHEX(color)
                colorField.backgroundColor = color
                colorField.accessibilityLabel = "Color field".localized
                colorField.userInteractionEnabled = false
                colorField.layer.cornerRadius = 5.0
                
                cell?.addSubview(colorField)
            } else if actions[indexPath.row] == "Smartilator" {
                let x : CGFloat = UIScreen.mainScreen().bounds.width-130
                let y : CGFloat = 10
                let width : CGFloat = cell!.frame.height+20
                let height : CGFloat = cell!.frame.height-20
                var ventilatorPercent = UIButton(frame: CGRectMake(x, y, width, height))
                if editMode == false {
                    preset.ventilatorPowerAction = Int(preset.ventilatorPower*100)
                }
                ventilatorPercent.setTitle("\(Int(preset.ventilatorPowerAction)) %", forState: .Normal)
                ventilatorPercent.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
                ventilatorPercent.titleLabel?.textAlignment = .Right
                ventilatorPercent.addTarget(self, action: Selector("changePercentage"), forControlEvents: .TouchUpInside)
                
                cell?.addSubview(ventilatorPercent)
            }
        }
        
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewEvents {
            return events.count
        } else {
            return actions.count
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Switch selection state
        if tableView == tableViewEvents {
            if indexPath.row < checkmarkCount {
                selectedEvents[indexPath.row] = !selectedEvents[indexPath.row]
            }
        } else {
            selectedActions[indexPath.row] = !selectedActions[indexPath.row]
        }
        
        //Deselect view again
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == tableViewEvents {
            return "If these events occur".localized
        } else {
            return "Do the following actions".localized
        }
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel.font = UIFont(name: "HelveticaNeueInterface-Regular", size: 13.0)
        header.tintColor = Constants.highlightColor1
        header.contentView.backgroundColor = UIColor.clearColor()
        header.textLabel.textColor = UIColor.whiteColor()
    }
    
    // MARK: - TextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.tag == checkmarkCount+6 {
            changeTimeView(textField)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.text == "" && textField == nameTextField {
            //Default title
            preset.name = "Preset".localized + " " + "\(PresetStore.ruleStore.idCount)"
        } else if textField == nameTextField {
            preset.name = textField.text
        } else {
            switch textField.tag {
            case checkmarkCount:
                selectedEvents[checkmarkCount] = count(textField.text)>0
                preset.temperatureExceeds = textField.text
            case checkmarkCount+1:
                selectedEvents[checkmarkCount+1] = count(textField.text)>0
                preset.temperatureBelow = textField.text
            case checkmarkCount+2:
                selectedEvents[checkmarkCount+2] = count(textField.text)>0
                preset.volumeExceeds = textField.text
            case checkmarkCount+3:
                selectedEvents[checkmarkCount+3] = count(textField.text)>0
                preset.volumeBelow = textField.text
            case checkmarkCount+4:
                selectedEvents[checkmarkCount+4] = count(textField.text)>0
                preset.brightnessExceeds = textField.text
            case checkmarkCount+5:
                selectedEvents[checkmarkCount+5] = count(textField.text)>0
                preset.brightnessBelow = textField.text
            case checkmarkCount+6:
                selectedEvents[checkmarkCount+6] = count(textField.text)>0
                var dateFormat = NSDateFormatter()
                dateFormat.dateFormat = "hh:mm"
                preset.time = dateFormat.dateFromString(textField.text)
            default: break
            }
        }
    }
    
    
    func sendRule(mode: String) {
        preset.sendRule(mode)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("sentRule"), name: "ruleSent", object: nil)
    }
    
    func sentRule() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ruleSent", object: nil)
    }
    
    // MARK: - Picker Views
    
    func changeTimeView(sender: UITextField) {
        // Actionsheet with no titel and space for content
        var title = ""
        var message = "\n\n\n\n\n\n\n\n\n\n"
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.modalInPopover = true
        
        var width = (alert.view.frame.width<alert.view.frame.height) ? alert.view.frame.width-50 : alert.view.frame.height-50
        var pickerFrame: CGRect = CGRectMake(0, 52, 0, 50)
        
        // Datepicker in time mode
        datePicker = UIDatePicker(frame: pickerFrame)
        datePicker.frame = pickerFrame
        datePicker.datePickerMode = UIDatePickerMode.Time
        datePicker.tag = 0
        
        alert.view.addSubview(datePicker)
        
        // Frameview with ok and cancel button
        let toolFrame = CGRectMake(17, 5, width, 45)
        let toolView = UIView(frame: toolFrame)
        
        // Cancel Button
        let buttonCancel = UIButton()
        buttonCancel.setTitle("Cancel".localized, forState: UIControlState.Normal)
        buttonCancel.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        toolView.addSubview(buttonCancel)
        buttonCancel.addTarget(self, action: "cancelSelection:", forControlEvents: UIControlEvents.TouchDown)
        
        // Ok Button
        var buttonOk: UIButton = UIButton()
        buttonOk.setTitle("Pick time".localized, forState: UIControlState.Normal)
        buttonOk.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        buttonOk.titleLabel?.font = UIFont.boldSystemFontOfSize(buttonOk.titleLabel!.font.pointSize)
        toolView.addSubview(buttonOk)
        buttonOk.addTarget(self, action: "selectTime", forControlEvents: UIControlEvents.TouchDown)
        
        // set button constraints
        buttonCancel.setTranslatesAutoresizingMaskIntoConstraints(false)
        buttonOk.setTranslatesAutoresizingMaskIntoConstraints(false)
        let viewsDictionary = ["buttonCancel": buttonCancel, "buttonOk": buttonOk] as NSDictionary
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[buttonCancel]-(>=8)-[buttonOk]-|", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[buttonCancel]", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraint(NSLayoutConstraint(item: buttonOk, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: buttonCancel, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        
        alert.view.addSubview(toolView)
        self.presentViewController(alert, animated: true, completion: nil)
    }

    
    func cancelSelection(sender: UIButton){
        //Dismiss date picker
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func selectTime() {
        //Save selected time
        preset.time = datePicker.date
        tableViewEvents.reloadData()
        
        //Dismiss picker view
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        let percent = row
        return "\(percent) %"
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 101
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func changePercentage() {
        //Actionsheet presenting picker
        var title = ""
        var message = "\n\n\n\n\n\n\n\n\n\n"
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.modalInPopover = true
        
        var width = (alert.view.frame.width<alert.view.frame.height) ? alert.view.frame.width-50 : alert.view.frame.height-50
        var pickerFrame: CGRect = CGRectMake(0, 52, 0, 50)
        
        //Percentage in UIPickerView
        picker = UIPickerView(frame: pickerFrame)
        picker.delegate = self
        picker.dataSource = self
        alert.view.addSubview(picker)
        picker.selectRow(Int(preset.ventilatorPowerAction), inComponent: 0, animated: false)
        
        // Frameview with ok and cancel button
        let toolFrame = CGRectMake(17, 5, width, 45)
        let toolView = UIView(frame: toolFrame)
        
        // Cancel Button
        let buttonCancel = UIButton()
        buttonCancel.setTitle("Cancel".localized, forState: UIControlState.Normal)
        buttonCancel.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        toolView.addSubview(buttonCancel)
        buttonCancel.addTarget(self, action: "cancelSelection:", forControlEvents: UIControlEvents.TouchDown)
        
        // Ok Button
        var buttonOk: UIButton = UIButton()
        buttonOk.setTitle("Pick".localized, forState: UIControlState.Normal)
        buttonOk.setTitleColor(Constants.highlightColor1, forState: .Normal)
        buttonOk.titleLabel?.font = UIFont.boldSystemFontOfSize(buttonOk.titleLabel!.font.pointSize)
        toolView.addSubview(buttonOk)
        buttonOk.addTarget(self, action: "selectPercentage", forControlEvents: UIControlEvents.TouchDown)
        
        // set button constraints
        buttonCancel.setTranslatesAutoresizingMaskIntoConstraints(false)
        buttonOk.setTranslatesAutoresizingMaskIntoConstraints(false)
        let viewsDictionary = ["buttonCancel": buttonCancel, "buttonOk": buttonOk] as NSDictionary
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[buttonCancel]-(>=8)-[buttonOk]-|", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[buttonCancel]", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraint(NSLayoutConstraint(item: buttonOk, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: buttonCancel, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        
        alert.view.addSubview(toolView)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func selectPercentage() {
        //Save selected percentage
        preset.ventilatorPowerActionB = true
        preset.ventilatorPowerAction = Int(picker.selectedRowInComponent(0))
        preset.ventilatorPower = CGFloat(preset.ventilatorPowerAction) / 100.0
        tableViewActions.reloadData()
        
        //Dismiss picker view
        self.dismissViewControllerAnimated(true, completion: nil);
    }
}
