//
//  PresetAddingViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 15/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit


class PresetAddingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate  {
    var preset : Preset = Preset.currentPreset.copyPreset()
    var selected : [Bool] = [false, false,false, false, false]
    var names : [String] = ["MoodLight".localized, "Open door".localized, "Close door".localized, "Make a coffee".localized, "Smartilator".localized]
    var editMode = false
    var picker = UIPickerView()
    var ventilatorPercent = UIButton()
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        //Set up background color and navigation bar
        self.view.backgroundColor = Constants.backgroundColor
        var cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancelPressed"))
        var doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("donePressed"))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        
        //Set up table view
        tableView.scrollEnabled = false
        tableView.backgroundColor = Constants.backgroundColor
        tableView.separatorColor = Constants.backgroundColor
        
        //Default title
        preset.name = "Preset".localized + " " + "\(PresetStore.mainStore.idCount)"
        
        //Check if we are editing or editing
        if Constants.presetToEdit != nil {
            preset = Constants.presetToEdit!
            Constants.presetToEdit = nil
            nameTextField.text = preset.name
            editMode = true
            selected = [preset.lightActivation, preset.doorOpened, preset.doorClosed, preset.shouldMakeCoffee, preset.ventilatorActivation]
            self.title = "Edit preset".localized
            return
        }
        //Default color
        preset.lightHue = Preset.currentPreset.lightHue
        preset.lightSaturation = Preset.currentPreset.lightSaturation
        preset.lightBrightness = Preset.currentPreset.lightBrightness
        
        //Set default values for new presets
        preset.ventilatorPowerAction = Int(preset.ventilatorPower)
    }
    
    func cancelPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func donePressed() {
        preset.lightActivation = selected[0]
        preset.doorOpened = selected[1]
        preset.doorClosed = selected[2]
        preset.shouldMakeCoffee = selected[3]
        preset.ventilatorActivation = selected[4]
        
        if editMode {
            Constants.presetEdited = true
            PresetStore.mainStore.replaceItem(Constants.presetEditIndex!, newItem: preset)
        } else {
            Constants.presetToAdd = self.preset
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Table View Delegate / Data Source
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if cell == nil {
            //cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell = UITableViewCell()
            cell!.tintColor = Constants.highlightColor1
        }
        if selected[indexPath.row] {
            cell?.accessoryType = .Checkmark
        } else {
            cell?.accessoryType = .None
        }
        
        cell!.textLabel?.text = names[indexPath.row]
        
        if names[indexPath.row] == "MoodLight" {
            let x : CGFloat = UIScreen.mainScreen().bounds.width-100
            let y : CGFloat = 10
            let width : CGFloat = cell!.frame.height-20
            let height : CGFloat = cell!.frame.height-20
            var colorField = UITextField(frame: CGRectMake(x, y, width, height))
            
            let hue = preset.lightHue
            let saturation = preset.lightSaturation
            let brightness = preset.lightBrightness
            let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
            colorField.backgroundColor = color
            colorField.accessibilityLabel = "Color field".localized
            colorField.userInteractionEnabled = false
            colorField.layer.cornerRadius = 5.0
            
            cell?.addSubview(colorField)
        } else if names[indexPath.row] == "Smartilator" {
            let x : CGFloat = UIScreen.mainScreen().bounds.width-140
            let y : CGFloat = 0
            let width : CGFloat = cell!.frame.height+20
            let height : CGFloat = cell!.frame.height
            var ventilatorPercent = UIButton(frame: CGRectMake(x, y, width, height))
            ventilatorPercent.setTitle("\(preset.ventilatorPowerAction) %", forState: .Normal)
            ventilatorPercent.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
            ventilatorPercent.titleLabel?.textAlignment = .Right
            ventilatorPercent.addTarget(self, action: Selector("changePercentage"), forControlEvents: .TouchUpInside)
            
            cell?.addSubview(ventilatorPercent)
        }
        
        return cell!
    }
    
    func changePercentage() {
        //Actionsheet presenting picker
        var title = ""
        var message = "\n\n\n\n\n\n\n\n\n\n"
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.modalInPopover = true
        
        var width = (alert.view.frame.width<alert.view.frame.height) ? alert.view.frame.width-50 : alert.view.frame.height-50
        var pickerFrame: CGRect = CGRectMake(0, 52, 0, 50)
        
        //Percentage in UIPickerView
        picker = UIPickerView(frame: pickerFrame)
        picker.delegate = self
        picker.dataSource = self
        alert.view.addSubview(picker)
        picker.selectRow(Int(preset.ventilatorPowerAction), inComponent: 0, animated: false)
        
        // Frameview with ok and cancel button
        let toolFrame = CGRectMake(17, 5, width, 45)
        let toolView = UIView(frame: toolFrame)
        
        // Cancel Button
        let buttonCancel = UIButton()
        buttonCancel.setTitle("Cancel".localized, forState: UIControlState.Normal)
        buttonCancel.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        toolView.addSubview(buttonCancel)
        buttonCancel.addTarget(self, action: "cancelSelection:", forControlEvents: UIControlEvents.TouchDown)
        
        // Ok Button
        var buttonOk: UIButton = UIButton()
        buttonOk.setTitle("Pick".localized, forState: UIControlState.Normal)
        buttonOk.setTitleColor(Constants.highlightColor1, forState: .Normal)
        buttonOk.titleLabel?.font = UIFont.boldSystemFontOfSize(buttonOk.titleLabel!.font.pointSize)
        toolView.addSubview(buttonOk)
        buttonOk.addTarget(self, action: "selectPercentage", forControlEvents: UIControlEvents.TouchDown)
        
        // set button constraints
        buttonCancel.setTranslatesAutoresizingMaskIntoConstraints(false)
        buttonOk.setTranslatesAutoresizingMaskIntoConstraints(false)
        let viewsDictionary = ["buttonCancel": buttonCancel, "buttonOk": buttonOk] as NSDictionary
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[buttonCancel]-(>=8)-[buttonOk]-|", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[buttonCancel]", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraint(NSLayoutConstraint(item: buttonOk, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: buttonCancel, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        
        alert.view.addSubview(toolView)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Switch selection state
        selected[indexPath.row] = !selected[indexPath.row]
        
        //Deselect view again
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        var header = view as! UITableViewHeaderFooterView
        header.textLabel.text = "Select all rows you want to save".localized
        header.textLabel.font = UIFont.systemFontOfSize(13)
        header.textLabel.textColor = UIColor.whiteColor()
        header.contentView.backgroundColor = Constants.highlightColor1
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //No code here
        return UITableViewHeaderFooterView()
    }
    
    // MARK: - TextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.text == "" {
            //Default title
            preset.name = "Preset".localized + " " + "\(PresetStore.mainStore.idCount)"
        } else {
            preset.name = textField.text
        }
    }
    
    // MARK: - Picker View
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        let percent = row
        return "\(percent) %"
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 101
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func cancelSelection(sender: UIButton){
        //Dismiss date picker
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func selectPercentage() {
        //Save selected percentage
        preset.ventilatorPowerAction = picker.selectedRowInComponent(0)
        tableView.reloadData()
        
        //Dismiss picker view
        self.dismissViewControllerAnimated(true, completion: nil);
    }
}
