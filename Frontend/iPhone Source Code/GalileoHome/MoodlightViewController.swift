//
//  MoodlightViewController.swift
//  GalileoHome
//
//  Created by Alexander Neumann on 10.06.15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class MoodlightViewController: UIViewController, UITextFieldDelegate,UIActionSheetDelegate {
    @IBOutlet weak var colorSlider: UISlider!
    @IBOutlet weak var colorTextfield: UITextField!
    @IBOutlet weak var colorLabel: UITextField!
    @IBOutlet weak var brightnessTextfield: UITextField!
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var saturationTextfield: UITextField!
    @IBOutlet weak var saturationSlider: UISlider!
    @IBOutlet weak var saturationImage: UIImageView!
    @IBOutlet weak var brightnessImage: UIImageView!
    
    @IBOutlet weak var lumiosityTextfield: UITextField!
    @IBOutlet weak var timeBeginTextfield: UITextField!
    @IBOutlet weak var timeEndTextfield: UITextField!
    
    @IBOutlet weak var rule1: UISwitch!
    @IBOutlet weak var rule2: UISwitch!
    @IBOutlet weak var rule3: UISwitch!
    
    @IBOutlet weak var systemRulesView: UIView!
    var brake : Int = 0
    
    var hue : CGFloat = 0.0
    var brightness: CGFloat = 0.0
    var saturation: CGFloat = 0.0
    var keyboardHeight: CGFloat = 0.0
    
    var currentTextfield = UITextField()
    var editting = false
    
    var sendRequestPending : Bool = false {
        didSet {
            if sendRequestPending == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.stopUpdating()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    self.startUpdating()
                })
            }
        }
    }
    
    var picker = UIDatePicker()
    
    var updateTimer = NSTimer()
    
    // MARK: - ViewController methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //No system rules on 4S
        if Constants.runningOn4S {
            systemRulesView.hidden = true
            systemRulesView.userInteractionEnabled = false
        }
        
        self.automaticallyAdjustsScrollViewInsets = false;
        
        self.view.backgroundColor = Constants.backgroundColor
        
        
        let clearImage = UIImage(named: "Transparent.png")
        var clearImageLeft = clearImage?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 17, 0, 0))
        var clearImageRight = clearImage?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 0, 0, 17))
        
        // Set slider bg transparent
        colorSlider.setMinimumTrackImage(clearImageLeft, forState: UIControlState.Normal)
        colorSlider.setMaximumTrackImage(clearImageRight, forState: UIControlState.Normal)
        brightnessSlider.setMinimumTrackImage(clearImageLeft, forState: UIControlState.Normal)
        brightnessSlider.setMaximumTrackImage(clearImageRight, forState: UIControlState.Normal)
        saturationSlider.setMinimumTrackImage(clearImageRight, forState: UIControlState.Normal)
        saturationSlider.setMaximumTrackImage(clearImageRight, forState: UIControlState.Normal)
        
        
        
        for subview in self.view.subviews{
            if subview.tag == 1 {
                self.view.bringSubviewToFront(subview as! UIView)
            }
        }
        brightnessSlider.tintColor = Constants.highlightColor1
        saturationSlider.tintColor = Constants.highlightColor1
        
        colorSlider.addTarget(self, action: Selector("sliderValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        brightnessSlider.addTarget(self, action: Selector("sliderValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        saturationSlider.addTarget(self, action: Selector("sliderValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        rule1.addTarget(self, action: "switchChanged:", forControlEvents: UIControlEvents.ValueChanged)
        rule2.addTarget(self, action: "switchChanged:", forControlEvents: UIControlEvents.ValueChanged)
        rule3.addTarget(self, action: "switchChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        colorTextfield.delegate=self
        brightnessTextfield.delegate=self
        saturationTextfield.delegate=self
        timeBeginTextfield.delegate=self
        timeEndTextfield.delegate=self
        lumiosityTextfield.delegate=self
        
        addDoneButtonOnKeyboard(colorTextfield)
        addDoneButtonOnKeyboard(brightnessTextfield)
        addDoneButtonOnKeyboard(saturationTextfield)
        addDoneButtonOnKeyboard(lumiosityTextfield)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        
        updateValues()
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        startUpdating()
    }
    
    override func viewDidDisappear(animated: Bool) {
        stopUpdating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Color converting helper methods
    
    //Convert a UIColor to hex string representing the same color
    func colorToHEX(color : UIColor) -> String {
        //Get RGB values from color
        var getRed = CGFloat()
        var getGreen = CGFloat()
        var getBlue = CGFloat()
        var getAlpha = CGFloat()
        color.getRed(&getRed, green: &getGreen, blue: &getBlue, alpha: &getAlpha)
        
        //Convert RGB values to HEX
        var redHexString = String(NSString(format:"%2X", Int(getRed*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var blueHexString = String(NSString(format:"%2X", Int(getGreen*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var greenHexString = String(NSString(format:"%2X", Int(getBlue*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        //If only 1 char long -> Prefix with 0 to get 2 chars for each color component
        if count(redHexString) == 1{
            redHexString = "0" + redHexString
        }
        if count(blueHexString) == 1{
            blueHexString = "0" + blueHexString
        }
        if count(greenHexString) == 1{
            greenHexString = "0" + greenHexString
        }
        
        //Merge to one single string
        let colorHEX : String = redHexString + blueHexString + greenHexString
        
        return colorHEX
    }
    
    ///Converts string of format #RRGGBB to UIColor
    func hexToColor(hex rgb : String) -> UIColor {
        //Get substrings containing HEX values
        let redHex = rgb.substringWithRange(Range<String.Index>(start: advance(rgb.startIndex,1), end: advance(rgb.startIndex, 3)))
        let greenHex = rgb.substringWithRange(Range<String.Index>(start: advance(rgb.startIndex, 3), end: advance(rgb.startIndex, 5)))
        let blueHex = rgb.substringFromIndex(advance(rgb.endIndex, -2))
        
        //Convert to float values
        var getRed = CGFloat(UInt8(strtoul(redHex, nil, 16)))/CGFloat(255)
        var getGreen = CGFloat(UInt8(strtoul(greenHex, nil, 16)))/CGFloat(255)
        var getBlue = CGFloat(UInt8(strtoul(blueHex, nil, 16)))/CGFloat(255)
        
        return UIColor(red: getRed, green: getGreen, blue: getBlue, alpha: 1.0)
    }
    
    func createGradientSliderImage(fromColor : UIColor, toColor:UIColor) -> UIImage {
        //Get RGB values from fromColor
        var fromRed = CGFloat()
        var fromGreen = CGFloat()
        var fromBlue = CGFloat()
        var fromAlpha = CGFloat()
        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)
        
        //Get RGB values from toColor
        var toRed = CGFloat()
        var toGreen = CGFloat()
        var toBlue = CGFloat()
        var toAlpha = CGFloat()
        toColor.getRed(&toRed, green: &toGreen, blue: &toBlue, alpha: &toAlpha)
        
        //Make context
        let size = CGSizeMake(1000.0, 10.0)
        //Start image
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        //Put together in one array
        var components : [CGFloat] = [fromRed, fromGreen, fromBlue, fromAlpha, toRed, toGreen, toBlue, toAlpha]
        var locations : [CGFloat] = [0.0, 1.0]
        
        
        //Make gradient
        let gradient = CGGradientCreateWithColorComponents(colorSpace, &components, &locations, 2)
        CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(size.width, 0), 0);
        
        //Create image from gradient
        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        //End image
        UIGraphicsEndImageContext()
        
        return img
    }
    
    // MARK: - Validity checking
    
    /// Checks if the String is a valid RGB color.
    ///
    /// :param: rgb A String which contains a color in RGB.
    /// :returns: A Boolean.
    func rgbRegEx(rgb:String)->Bool{
        let hexRegex = NSRegularExpression(pattern: "^#([A-Fa-f0-9]{6})$", options: nil, error: nil)!
        if let match = hexRegex.firstMatchInString(rgb, options: nil,
            range: NSRange(location: 0, length: count(rgb.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    
    /// Checks if the String is a valid percentage.
    ///
    /// :param: percent A String which contains a percentage.
    /// :returns: A Boolean.
    func percentRegex(percent:String)->Bool{
        let percentageRegex = NSRegularExpression(pattern: "^([0-9]|[1-9][0-9]|100)%$", options: nil, error: nil)!
        if let match = percentageRegex.firstMatchInString(percent, options: nil,
            range: NSRange(location: 0, length: count(percent.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    
    func percentRegex2(percent:String)->Bool{
        let percentageRegex = NSRegularExpression(pattern: "^([0-9]|[1-9][0-9]|100)$", options: nil, error: nil)!
        if let match = percentageRegex.firstMatchInString(percent, options: nil,
            range: NSRange(location: 0, length: count(percent.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    
    // MARK: - UI Updating

    func updateUI() {
        //Update color label
        let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        
        hue = CGFloat(colorSlider.value)
        saturation = CGFloat(saturationSlider.value)
        brightness = CGFloat(brightnessSlider.value)
        // gradient colors
        let colorS1 = UIColor(hue: hue, saturation: 0.0, brightness: brightness, alpha: 1.0)
        let colorS2 = UIColor(hue: hue, saturation: 1.0, brightness: brightness, alpha: 1.0)
        let colorB1 = UIColor(hue: hue, saturation: saturation, brightness: 0.0, alpha: 1.0)
        let colorB2 = UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0)
        colorLabel.backgroundColor = color
        
        //update slider images
        saturationImage.image = createGradientSliderImage(colorS1, toColor: colorS2)
        brightnessImage.image = createGradientSliderImage(colorB1, toColor: colorB2)
        
        //Update textfields
        colorTextfield.text = "#" + colorToHEX(color)
        brightnessTextfield.text = String(Int(brightness*100))+"%"
        if(brightness != 0.0) {
            saturationTextfield.text = String(Int(saturation*100))+"%"
        }
    }
    
    ///Updates current color, expects string with format #RRGGBB where RR, GG and BB are red, green and blue value as HEX
    func updateColor(rgb:String, force: Bool=false){
        //User has always highest priority in changing colors, no code should interrupt
        //Exception: force enabled - this also sends color back to server
        if sendRequestPending && !force {return}
        
        //get color
        let color = hexToColor(hex: rgb)
        
        //Update internal values
        var alpha : CGFloat = 0.0
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        let tempHue = hue
        let tempSat = saturation
        let tempBright = brightness
        //Update slider values
        //fix for jumping left (hue, satuartion)
        if ((brightness != 0.0) && (saturation != 0.0)) {
            if(brightness != 0.0) {
                UIView.animateWithDuration(Constants.sliderSpeed, animations: {
                    self.saturationSlider.setValue(Float(self.saturation), animated: true)
                })
            }
            // fix for jumping edges
            if ((colorSlider.value != 0.0 || hue != 1.0) && (colorSlider.value != 1.0 || hue != 0.0)){
                UIView.animateWithDuration(Constants.sliderSpeed, animations: {
                    self.colorSlider.setValue(Float(self.hue), animated: true)
                })
            }
        }
        
        UIView.animateWithDuration(Constants.sliderSpeed, animations: {
            self.brightnessSlider.setValue(Float(self.brightness), animated: true)
        })
        //Update UI
        updateUI()
        if force {
            hue = tempHue
            brightness = tempBright
            saturation = tempSat
            sendColor()
        }
    }
    
    // MARK: - Delegate methods / Target-Action Methods
    func addDoneButtonOnKeyboard(view: UIView?) {
        ///Return a 1x1 image with translucent highlightColor1
        func pixelImage() -> UIImage {
            //One pixel rect
            let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            
            CGContextSetFillColorWithColor(context, Constants.highlightColor1.colorWithAlphaComponent(0.8).CGColor)
            CGContextFillRect(context, rect)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image
        }
        
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.Black
        doneToolbar.setBackgroundImage(pixelImage(),
            forToolbarPosition: UIBarPosition.Bottom,
            barMetrics: UIBarMetrics.Default)
        //let tintColor = Constants.highlightColor1.colorWithAlphaComponent(0.5)
        doneToolbar.barTintColor = Constants.highlightColor1
        doneToolbar.translucent = true
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: view, action: "resignFirstResponder")
        done.tintColor = UIColor.whiteColor()
        var items = [AnyObject]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if let accessorizedView = view as? UITextView {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        } else if let accessorizedView = view as? UITextField {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        }
    }
    
    func switchChanged(sender: UISwitch) {
        switch sender {
            case rule1: sendMoodlightSystemRule1()
            case rule2: sendMoodlightSystemRule2()
            case rule3: sendMoodlightSystemRule3()
            default: break
        }
    }
    
    // door open rule
    func sendMoodlightSystemRule1() {
        Preset.currentPreset.moodlightSRule1 = rule1.on
        Preset.currentPreset.sendLightDoorOpens()
    }
    
    // brightness
    func sendMoodlightSystemRule2() {
        Preset.currentPreset.moodlightSRule2Brightness = lumiosityTextfield.text
        Preset.currentPreset.moodlightSRule2 = rule2.on
        Preset.currentPreset.sendLightLuminosity()
    }
    
    // time
    func sendMoodlightSystemRule3() {
        Preset.currentPreset.moodlightSRule3T1 = timeBeginTextfield.text.stringByReplacingOccurrencesOfString(":", withString: "")
        Preset.currentPreset.moodlightSRule3T2 = timeEndTextfield.text.stringByReplacingOccurrencesOfString(":", withString: "")
        Preset.currentPreset.moodlightSRule3 = rule3.on
        Preset.currentPreset.sendLightTime()
    }
    
    func sliderValueChanged(sender : UISlider) {
        //Lock code for changing UI
        sendRequestPending = true
        
        hue = CGFloat(colorSlider.value)
        brightness = CGFloat(brightnessSlider.value)
        saturation = CGFloat(saturationSlider.value)
        
        if(sender == saturationSlider) {
            saturationTextfield.text = String(Int(saturation*100))+"%"
        }
        
        sendColor()
        updateUI()
    }
    
    func updateBrightness(bright:String){
        brightness = CGFloat(bright.toInt()!)/100.0
        UIView.animateWithDuration(Constants.sliderSpeed, animations: {
            self.brightnessSlider.setValue(Float(self.brightness), animated: true)
        })
        sendColor()
        updateUI()
    }
    
    func updateSaturation(sat:String){
        saturation = CGFloat(sat.toInt()!)/100.0
        UIView.animateWithDuration(Constants.sliderSpeed, animations: {
            self.saturationSlider.setValue(Float(self.saturation), animated: true)
        })
        sendColor()
        updateUI()
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let k = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size.height {
                keyboardHeight = k - tabBarController!.tabBar.frame.height - 55
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if(currentTextfield.tag==4 && editting==false){
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.view.frame.origin.y -= keyboardHeight
            UIView.commitAnimations()
            editting=true
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //Lock code for changing UI
        sendRequestPending = true
        
        currentTextfield = textField
        return true
    }

    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if(textField.tag==1){
            if(rgbRegEx(textField.text)){
                colorTextfield.resignFirstResponder()
                updateColor(textField.text, force: true)
                return true
            }
        } else if(textField.tag==2){
            if(percentRegex(textField.text)){
                textField.resignFirstResponder()
                updateBrightness(textField.text.substringToIndex(advance(textField.text.endIndex,-1)))
                return true
            } else if(percentRegex2(textField.text)){
                textField.resignFirstResponder()
                updateBrightness(textField.text)
                return true
            }
        } else if(textField.tag==3){
            if(percentRegex(textField.text)){
                textField.resignFirstResponder()
                updateSaturation(textField.text.substringToIndex(advance(textField.text.endIndex,-1)))
                return true
            } else if(percentRegex2(textField.text)){
                textField.resignFirstResponder()
                updateSaturation(textField.text)
                return true
            }
        } else if(textField.tag==4){
            if(percentRegex(textField.text)){
                textField.resignFirstResponder()
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.2)
            
                self.view.frame.origin.y += keyboardHeight
                //self.view.frame.size.height -= keyboardHeight
                UIView.commitAnimations()
                editting = false
                sendMoodlightSystemRule2()
                return true
            } else if(percentRegex2(textField.text)){
                textField.resignFirstResponder()
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.2)
            
                self.view.frame.origin.y += keyboardHeight
                //self.view.frame.size.height -= keyboardHeight
                UIView.commitAnimations()
                editting = false
                textField.text = textField.text + "%"
                return true
            }
        } else {
            if(percentRegex(textField.text)){
                textField.resignFirstResponder()
                return true
            }
        }
        
        return false
    }
    
    @IBAction func changeTime(sender: UITextField) {
        currentTextfield = sender
        changeTimeView(sender)
    }
    
    func changeTimeView(sender: UITextField) {
        sendRequestPending = true
        // Actionsheet with no titel and space for content
        var title = ""
        var message = "\n\n\n\n\n\n\n\n\n\n"
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alert.modalInPopover = true
        
        var width = (alert.view.frame.width<alert.view.frame.height) ? alert.view.frame.width-50 : alert.view.frame.height-50
        var pickerFrame: CGRect = CGRectMake(0, 52, 0, 50)
        
        // Datepicker in time mode
        picker = UIDatePicker(frame: pickerFrame)
        picker.frame = pickerFrame
        picker.datePickerMode = UIDatePickerMode.Time
        picker.tag = 0
        
        alert.view.addSubview(picker)
        
        // Frameview with ok and cancel button
        let toolFrame = CGRectMake(17, 5, width, 45)
        let toolView = UIView(frame: toolFrame)
        
        // Cancel Button
        let buttonCancel = UIButton()
        buttonCancel.setTitle("Cancel".localized, forState: UIControlState.Normal)
        buttonCancel.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        toolView.addSubview(buttonCancel)
        buttonCancel.addTarget(self, action: "cancelSelection:", forControlEvents: UIControlEvents.TouchDown)
        
        // Ok Button
        var buttonOk: UIButton = UIButton()
        buttonOk.setTitle("Pick time".localized, forState: UIControlState.Normal)
        buttonOk.setTitleColor(Constants.highlightColor1, forState: UIControlState.Normal)
        buttonOk.titleLabel?.font = UIFont.boldSystemFontOfSize(buttonOk.titleLabel!.font.pointSize)
        toolView.addSubview(buttonOk)
        buttonOk.addTarget(self, action: "selectTime:", forControlEvents: UIControlEvents.TouchDown)
        
        // set button constraints
        buttonCancel.setTranslatesAutoresizingMaskIntoConstraints(false)
        buttonOk.setTranslatesAutoresizingMaskIntoConstraints(false)
        let viewsDictionary = ["buttonCancel": buttonCancel, "buttonOk": buttonOk] as NSDictionary
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[buttonCancel]-(>=8)-[buttonOk]-|", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[buttonCancel]", options: nil, metrics: nil, views: viewsDictionary as [NSObject : AnyObject]))
        toolView.addConstraint(NSLayoutConstraint(item: buttonOk, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: buttonCancel, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        
        alert.view.addSubview(toolView)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func cancelSelection(sender: UIButton){
        //Dismiss date picker
        self.dismissViewControllerAnimated(true, completion: nil);
        sendRequestPending = false
    }
    
    func selectTime(sender: UIButton) {
        //Save selected time
        let time = picker.date
        var dateFormat = NSDateFormatter()
        dateFormat.dateFormat = "hh:mm"
        currentTextfield.text = dateFormat.stringFromDate(time)
        sendMoodlightSystemRule3()
        sendRequestPending = false
        
        //Dismiss picker view
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    // MARK: - Network methods
    
    func updateValues() {
        Preset.currentPreset.receiveLightColor()
        
        //Update rules 1 time / 2 Seconds
        if brake == 0 {
            Preset.currentPreset.receiveMoodlightRules()
        }
        brake++
        brake %= Int(1/Constants.updateInterval) * 2
        
        //Received rules
        rule1.setOn(Preset.currentPreset.moodlightSRule1, animated: true)
        rule2.setOn(Preset.currentPreset.moodlightSRule2, animated: true)
        rule3.setOn(Preset.currentPreset.moodlightSRule3, animated: true)
        
        lumiosityTextfield.text = Preset.currentPreset.moodlightSRule2Brightness.stringByReplacingOccurrencesOfString("%", withString: "") + "%"
        let hour = Preset.currentPreset.moodlightSRule3T1.substringToIndex(advance(Preset.currentPreset.moodlightSRule3T1.startIndex, 2))
        let min = Preset.currentPreset.moodlightSRule3T1.substringFromIndex(advance(Preset.currentPreset.moodlightSRule3T1.startIndex, 2))
        let hour2 = Preset.currentPreset.moodlightSRule3T2.substringToIndex(advance(Preset.currentPreset.moodlightSRule3T1.startIndex, 2))
        let min2 = Preset.currentPreset.moodlightSRule3T2.substringFromIndex(advance(Preset.currentPreset.moodlightSRule3T1.startIndex, 2))
        timeBeginTextfield.text = hour + ":" + min
        timeEndTextfield.text = hour2 + ":" + min2
        
        //Received color
        let color = UIColor(hue: Preset.currentPreset.lightHue, saturation: Preset.currentPreset.lightSaturation, brightness: Preset.currentPreset.lightBrightness, alpha: 1.0)
        
        //Convert color to hex string
        let colorHEX : String = colorToHEX(color)
        
        //Update UI
        updateColor("#"+colorHEX)
    }

    func startUpdating() {
        //updateValues()
        updateTimer.invalidate()
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.updateInterval, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    func stopUpdating() {
        updateTimer.invalidate()
    }
    
    func sendColor() {
        //Update preset
        Preset.currentPreset.lightHue = hue
        Preset.currentPreset.lightBrightness = brightness
        Preset.currentPreset.lightSaturation = saturation
        
        Preset.currentPreset.sendLight()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("sentColor"), name: "lightSent", object: nil)
    }
    
    dynamic private func sentColor() {
        //Stop waiting for an answer
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "lightSent", object: nil)
        
        //Start updating again
        sendRequestPending = false
    }
}