//
//  Constants.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit
import MapKit

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "", comment: "")
    }
}

extension Int {
    func toBool () -> Bool? {
        switch self {
        case 0: return false
        case 1: return true
        default: return nil
        }
    }
}


class Constants {
    ///Logout request
    static var shouldLogout = false
    
    //MARK: - Constants
    //Light grey
    static let backgroundColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1)
    
    //Blue (like the nav bar)
    static let highlightColor1 = UIColor(red: 36.0/255.0, green: 132.0/255.0, blue: 173.0/255.0, alpha: 1.0)
    
    //Brighter Blue
    static let highlightColor2 = UIColor(red: 13.0/255.0, green: 109/255.0, blue: 188.0/255.0, alpha: 1.0)
    static let disableColor = UIColor(white: 0.6, alpha: 0.78)
    static let textColor = UIColor.darkTextColor()
    static let infoViewKeyboardOffset = CGFloat(40.0)
    
    static let updateInterval = 0.1
    static let sliderSpeed = 0.3
    
    static var runningOn4S = false
    
    //MARK: - Globals
    static var presetToAdd : Preset? = nil
    static var presetToEdit : Preset? = nil
    static var ruleToAdd : [Preset]? = []
    static var ruleToDelete : [Preset]? = []
    static var ruleToEdit : Preset? = nil
    static var presetEditIndex : Int? = nil
    static var ruleEditIndex : Int? = nil
    static var presetEdited : Bool = false
    static var connection : Bool = false
    
    static var tabbarController : UITabBarController? = nil
    static var homeController : HomeViewController? = nil
    static var presentingAlert = false
    
    static let hardwareVersion = "1.0"
    static let appVersion = (NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String) + " (" + String(NSString(format:"%2X", (NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey as String) as! String).toInt()!)) + ")"
    
    //Network paths
    static let activeDevices = "active_devices"
    static let receivePresets = "rules"
    
    static let lightColorPath = "ambilight/color"
    static let ambR1Path = "ambilight/system_rule_door"
    static let ambR2Path = "ambilight/system_rule_brightness"
    static let ambR3Path = "ambilight/system_rule_time"
    static let doorStatePath = "sdo/state"
    static let ventilatorPowerPath = "smartilator/power"
    static let ventRPath = "smartilator/system_rule"
    static let coffeePath = "coffee/make_coffee"
    static let twitterMessagePath = "coffee/message"
    static let twitterConsumerKeyPath = "coffee/consumer_key"
    static let twitterConsumerSecretPath = "coffee/consumer_secret"
    static let twitterAccessKeyPath = "coffee/access_token_key"
    static let twitterAccessSecretPath = "coffee/access_token_secret"
    static let temperaturePath = "sensor/temperature"
    static let brightnessPath = "sensor/brightness"
    static let volumePath = "sensor/volume"
    static let beaconPath = "ibeacon"
    static let passwordResultPath = "passwordResult"
    
    //MARK: - Stored values
    static var homeLocation : CLLocationCoordinate2D {
        get {
            let storedValLong : CLLocationDegrees?  = NSUserDefaults.standardUserDefaults().objectForKey("homeLongitude") as? CLLocationDegrees
            let storedValLat : CLLocationDegrees? = NSUserDefaults.standardUserDefaults().objectForKey("homeLatitude") as? CLLocationDegrees
            if storedValLong == nil || storedValLat == nil {
                return CLLocationCoordinate2D(latitude: 0, longitude: 0)
            }
            return CLLocationCoordinate2D(latitude: storedValLat!, longitude: storedValLong!)
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal.longitude, forKey: "homeLongitude")
            NSUserDefaults.standardUserDefaults().setObject(newVal.latitude, forKey: "homeLatitude")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    static var enableGeofence : Bool {
        get {
            let storedVal : Bool?  = NSUserDefaults.standardUserDefaults().objectForKey("enableGeofence") as? Bool
            if storedVal == nil {
                return false
            }
            return storedVal!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "enableGeofence")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    static var twitterEnabled : Bool {
        get {
            let storedVal : Bool?  = NSUserDefaults.standardUserDefaults().objectForKey("enableTwitter") as? Bool
            if storedVal == nil {
        return false
        }
        return storedVal!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "enableTwitter")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    static var inBeaconRange : Bool {
        get {
            let storedVal : Bool?  = NSUserDefaults.standardUserDefaults().objectForKey("inBeaconRange") as? Bool
            if storedVal == nil {
                return false
            }
            return storedVal!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "inBeaconRange")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    class func checkNetwork() {
        let networkDevices = NetworkController.testConnection(Constants.activeDevices, timeout: 4.0)
        let network = (networkDevices == nil)
        
        if !network {
            //Constants.shouldLogout = true
            NetworkController.passwordStr = ""
            Constants.tabbarController?.selectedIndex = 0
            tabbarController?.selectedViewController
            
            let alertView = UIAlertController(title: "No network".localized, message: "You will be logged out, because the server is not reachable".localized, preferredStyle: .Alert)
            var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: dismissedAlert)
            alertView.addAction(cancelAction)
            if tabbarController != nil {
                if tabbarController!.isViewLoaded() && (tabbarController!.view.window != nil) && !Constants.presentingAlert {
                    Constants.presentingAlert = true
                    Constants.tabbarController?.presentViewController(alertView, animated: true, completion: nil)
                }
            }
        }
    }
    
    class func dismissedAlert(alert: UIAlertAction!) {
        if Constants.homeController != nil {
            Constants.homeController!.performSegueWithIdentifier("LoginSegue", sender: self)
            Constants.presentingAlert = false
        }
    }
}
