//
//  Preset.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 14/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

extension String {
    init(htmlEncodedString: String) {
        let encodedData = htmlEncodedString.dataUsingEncoding(NSUTF8StringEncoding)!
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        let attributedString = NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil, error: nil)!
        
        // Dann halt so..
        var enString = attributedString.string.stringByReplacingOccurrencesOfString("Ã¤", withString: "ä")
        enString = enString.stringByReplacingOccurrencesOfString("Ã„", withString: "Ä")
        enString = enString.stringByReplacingOccurrencesOfString("Ã¶", withString: "ö")
        enString = enString.stringByReplacingOccurrencesOfString("Ã–", withString: "Ö")
        enString = enString.stringByReplacingOccurrencesOfString("Ã¼", withString: "ü")
        enString = enString.stringByReplacingOccurrencesOfString("Ãœ", withString: "Ü")
        self.init(enString)
    }
    
    
}

class Preset: NSObject, NSCoding {
    // MARK: - Preset properties
    
    //Preset values
    var name = "Preset".localized
    var id : Int
    internal static var idCount = 0
    
    //Current preset
    static var currentPreset = Preset()
    static var deviceList = ["ambilight","sdo","coffee","smartilator","sensor"]
    static var activeDeviceList = [true,true,true,true,true]

    // MARK: - Current values
    
    //Light Controls
    var lightActivation = true
    var lightHue : CGFloat = 0.0
    var lightSaturation : CGFloat = 0.0
    var lightBrightness : CGFloat = 0.0
    
    var moodlightSRule1 = false
    var moodlightSRule2 = false
    var moodlightSRule3 = false
    var moodlightSRule2Brightness = "0"
    var moodlightSRule3T1 = "0000"
    var moodlightSRule3T2 = "0000"
    
    //Ventilator
    var ventilatorPower : CGFloat = 0.0
    var ventilatorPowerLastValue : CGFloat = 0.0
    var ventilatorActivation = true
    
    var ventRule1 = false
    var ventRule2 = false
    var ventRule3 = false
    
    var ventRule1From = ""
    var ventRule1Set = ""
    var ventRule2From = ""
    var ventRule2Set = ""
    var ventRule3From = ""
    var ventRule3Set = ""
    
    //Smart Door
    var doorOpened = false
    var doorClosed = false
    
    
    //Sensors
    var temperature : Int? = nil
    var brightness : Int? = nil
    var volume : Int? = nil
    
    // MARK: - Actions
    
    // Moodlight
    var lightTurnOnAction = false
    var lightActionHex = ""
    
    // SDO
    var doorOpenAction = false
    var doorCloseAction = false
    
    //Coffee
    var shouldMakeCoffee = false
    
    //Ventilator
    var ventilatorPowerActionB = false
    var ventilatorPowerAction:Int = 0
    
    // MARK: - Event values
    
    //Light Controls
    var lightStartTime = NSDate()
    var lightEndTime = NSDate()
    var lightTurnOnWhenDoorOpens = false
    var lightTurnOnLuminosityLevel : CGFloat = 1.0
    var lightTurnedOnEvent = false
    
    //Smartilator
    var ventilatorActivationLevel : Int = 0
    var ventilatorPowerLevel : CGFloat = 0.0
    
    //Smart door
    var doorOpenEvent = false
    var doorCloseEvent = false
    
    //Coffee
    var coffeeMade = false
    var coffeeState = false
    
    //Sensors
    var temperatureExceeds = ""
    var temperatureBelow = ""
    var volumeExceeds = ""
    var volumeBelow = ""
    var brightnessExceeds = ""
    var brightnessBelow = ""
    
    //Time value
    var time : NSDate? = nil
    
    var marked = false
    var ruleAdded = false
    var ruleDeleted = false
    var ruleEdited = false
    
    // MARK: - Creation Methods
    
    ///Return preset with unique ID
    override init() {
        id = Preset.idCount
        Preset.idCount++
        super.init()
    }
    
    ///Creates copy of preset without copying events and id
    func copyPreset() -> Preset {
        var newPreset = Preset()
        newPreset.lightSaturation = self.lightSaturation
        newPreset.lightHue = self.lightHue
        newPreset.lightBrightness = self.lightBrightness
        newPreset.name = self.name
        newPreset.doorOpened = self.doorOpened
        newPreset.shouldMakeCoffee = self.shouldMakeCoffee
        newPreset.ventilatorPower = self.ventilatorPower
        return newPreset
    }
    
    /// Creates a copy of the preset with all attributes
    func copyAll() -> Preset {
        var newPreset = Preset()
        newPreset.name = self.name
        //newPreset.id = Preset.idCount
        //Preset.idCount++
        // MARK: Current values
        
        //Light Controls
        newPreset.lightActivation = self.lightActivation
        newPreset.lightHue = self.lightHue
        newPreset.lightSaturation = self.lightSaturation
        newPreset.lightBrightness = self.lightBrightness
        
        newPreset.moodlightSRule1 = self.moodlightSRule1
        newPreset.moodlightSRule2 = self.moodlightSRule2
        newPreset.moodlightSRule3 = self.moodlightSRule3
        newPreset.moodlightSRule2Brightness = self.moodlightSRule2Brightness
        newPreset.moodlightSRule3T1 = self.moodlightSRule3T1
        newPreset.moodlightSRule3T2 = self.moodlightSRule3T2
        
        //Ventilator
        newPreset.ventilatorPower = self.ventilatorPower
        newPreset.ventilatorActivation = self.ventilatorActivation
        
        newPreset.ventRule1 = self.ventRule1
        newPreset.ventRule2 = self.ventRule2
        newPreset.ventRule3 = self.ventRule3
        
        newPreset.ventRule1From = self.ventRule1From
        newPreset.ventRule1Set = self.ventRule1Set
        newPreset.ventRule2From = self.ventRule2From
        newPreset.ventRule2Set = self.ventRule2Set
        newPreset.ventRule3From = self.ventRule3From
        newPreset.ventRule3Set = self.ventRule3From
        
        //Smart Door
        newPreset.doorOpened = self.doorOpened
        newPreset.doorClosed = self.doorClosed
        
        
        //Sensors
        newPreset.temperature = self.temperature
        newPreset.brightness = self.brightness
        newPreset.volume  = self.volume
        
        // MARK: Actions
        
        // Moodlight
        newPreset.lightTurnOnAction = self.lightTurnOnAction
        newPreset.lightActionHex = self.lightActionHex
        
        // SDO
        newPreset.doorOpenAction = self.doorOpenAction
        newPreset.doorCloseAction = self.doorCloseAction
        
        //Coffee
        newPreset.shouldMakeCoffee = self.shouldMakeCoffee
        
        //Ventilator
        newPreset.ventilatorPowerActionB = self.ventilatorPowerActionB
        newPreset.ventilatorPowerAction = self.ventilatorPowerAction
        
        // MARK: Event values
        
        //Light Controls
        newPreset.lightStartTime = self.lightStartTime
        newPreset.lightEndTime = self.lightEndTime
        newPreset.lightTurnOnWhenDoorOpens = self.lightTurnOnWhenDoorOpens
        newPreset.lightTurnOnLuminosityLevel = self.lightTurnOnLuminosityLevel
        newPreset.lightTurnedOnEvent = self.lightTurnedOnEvent
        
        //Smartilator
        newPreset.ventilatorActivationLevel = self.ventilatorActivationLevel
        newPreset.ventilatorPowerLevel = self.ventilatorPowerLevel
        
        //Smart door
        newPreset.doorOpenEvent = self.doorOpenEvent
        newPreset.doorCloseEvent = self.doorCloseEvent
        
        //Coffee
        newPreset.coffeeMade = self.coffeeMade
        newPreset.coffeeState = self.coffeeState
        
        //Sensors
        newPreset.temperatureExceeds = self.temperatureExceeds
        newPreset.temperatureBelow = self.temperatureBelow
        newPreset.volumeExceeds = self.volumeExceeds
        newPreset.volumeBelow = self.volumeBelow
        newPreset.brightnessExceeds = self.brightnessExceeds
        newPreset.brightnessBelow = self.brightnessBelow
        
        //Time value
        newPreset.time = self.time
        
        newPreset.marked = self.marked
        newPreset.ruleAdded = self.ruleAdded
        newPreset.ruleDeleted = self.ruleDeleted
        newPreset.ruleEdited = self.ruleEdited
        return newPreset
    }
    
    
    // MARK: - NSCoding Methods
    func encodeWithCoder(aCoder: NSCoder) {
        //TODO: encode remaining stuff
        //example
        
        aCoder.encodeObject(id, forKey: "presetId")
        aCoder.encodeObject(name, forKey: "presetName")
        aCoder.encodeObject(lightHue, forKey: "lightHue")
        aCoder.encodeObject(lightSaturation, forKey: "lightSaturation")
        aCoder.encodeObject(lightBrightness, forKey: "lightBrightness")
        aCoder.encodeObject(lightActivation, forKey: "lightActivation")
        aCoder.encodeObject(doorOpened, forKey: "doorOpened")
        aCoder.encodeObject(doorClosed, forKey: "doorClosed")
        aCoder.encodeObject(shouldMakeCoffee, forKey: "shouldMakeCoffee")
        aCoder.encodeObject(ventilatorActivation, forKey: "ventilatorActivation")
        aCoder.encodeObject(ventilatorPowerAction, forKey: "ventilatorPowerAction")
        
        aCoder.encodeObject(ventilatorPowerActionB, forKey: "ventilatorPowerActionB")
        aCoder.encodeObject(lightTurnOnAction, forKey: "lightTurnOnAction")
        aCoder.encodeObject(lightTurnedOnEvent, forKey: "lightTurnedOnEvent")
        aCoder.encodeObject(doorOpenAction, forKey: "doorOpenAction")
        aCoder.encodeObject(doorCloseAction, forKey: "doorCloseAction")
        aCoder.encodeObject(doorOpenEvent, forKey: "doorOpenEvent")
        aCoder.encodeObject(doorCloseEvent, forKey: "doorCloseEvent")
        aCoder.encodeObject(coffeeMade, forKey: "coffeeMade")
        aCoder.encodeObject(coffeeState, forKey: "coffeeState")
        aCoder.encodeObject(temperatureExceeds, forKey: "temperatureExceeds")
        aCoder.encodeObject(temperatureBelow, forKey: "temperatureBelow")
        aCoder.encodeObject(volumeExceeds, forKey: "volumeExceeds")
        aCoder.encodeObject(volumeBelow, forKey: "volumeBelow")
        aCoder.encodeObject(brightnessExceeds, forKey: "brightnessExceeds")
        aCoder.encodeObject(brightnessBelow, forKey: "brightnessBelow")
        aCoder.encodeObject(time, forKey: "time")
        
        
    }
    
    required init(coder aDecoder: NSCoder) {
        id = Preset.idCount
        Preset.idCount++
        super.init()
        
        if let storedVal = aDecoder.decodeObjectForKey("presetId") as? Int{
            id = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("temperatureExceeds") as? String {
            temperatureExceeds = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("temperatureBelow") as? String {
            temperatureBelow = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("volumeExceeds") as? String {
            volumeExceeds = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("volumeBelow") as? String {
            volumeBelow = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("brightnessExceeds") as? String {
            brightnessExceeds = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("brightnessBelow") as? String {
            brightnessBelow = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("time") as? NSDate {
            time = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("coffeeMade") as? Bool {
            coffeeMade = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("coffeeState") as? Bool {
            coffeeState = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorOpenAction") as? Bool {
            doorOpenAction = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorCloseAction") as? Bool {
            doorCloseAction = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorOpenEvent") as? Bool {
            doorOpenEvent = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorCloseEvent") as? Bool {
            doorCloseEvent = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightTurnOnAction") as? Bool {
            lightTurnOnAction = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightTurnedOnEvent") as? Bool {
            lightTurnedOnEvent = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("ventilatorPowerActionB") as? Bool {
            ventilatorPowerActionB = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("presetName") as? String {
            name = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightHue") as? CGFloat {
            lightHue = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightSaturation") as? CGFloat {
            lightSaturation = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightBrightness") as? CGFloat {
            lightBrightness = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("lightActivation") as? Bool {
            lightActivation = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorOpened") as? Bool {
            doorOpened = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("doorClosed") as? Bool {
            doorClosed = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("shouldMakeCoffee") as? Bool {
            shouldMakeCoffee = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("ventilatorActivation") as? Bool {
            ventilatorActivation = storedVal
        }
        if let storedVal = aDecoder.decodeObjectForKey("ventilatorPowerAction") as? Int {
            ventilatorPowerAction = storedVal
        }
        
        
    }
    
    // MARK: - Network Methods
    
    func updatePreset() {
        receiveDoorState()
        receiveLightColor()
        receiveSensorBrightness()
        receiveSensorTemperature()
        receiveSensorVolume()
        receiveVentilatorPower()
    }
    
    /// Request all current devices
    func requestActiveDevices() {
        NetworkController.receiveURL(Constants.activeDevices)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedActiveDevices:"), name: "receivedURL/"+Constants.activeDevices, object: nil)
    }
    
    dynamic private func receivedActiveDevices(notification: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.activeDevices, object: nil)
        
        //Parse answer
        if notification.object != nil {
            var dataStr = NetworkController.parseServerDataToString2(notification.object as! NSData)
            var devices = split(dataStr) {$0 == "\n"}
            
            // Set all devices off
            let count = Preset.deviceList.count
            Preset.activeDeviceList = [Bool](count: Preset.deviceList.count, repeatedValue: false)
            
            // Activate running devices
            for dev in devices {
                let idx = find(Preset.deviceList,dev)
                if (idx != nil){
                    Preset.activeDeviceList[idx!] = true
                }
            }
            NSLog("Devices: \(Preset.activeDeviceList)")
        }
    }
    
    func receivePresets() {
        NetworkController.receiveURL(Constants.receivePresets)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedPresets:"), name: "receivedURL/"+Constants.receivePresets, object: nil)
    }
    
    dynamic private func receivedPresets(notification: NSNotification) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.receivePresets, object: nil)
        
        //Parse answer
        if notification.object != nil && Constants.connection {
            for rule in PresetStore.ruleStore.allObjects(){
                rule.marked = false
            }
            
            var dataStr = NetworkController.parseServerDataToString2(notification.object as! NSData)
            // Split rules
            var rules = split(dataStr) {$0 == "\n"}
            for rule in rules {
                // new Preset for the rule
                var addPreset = Preset()
                var ruleParts = split(rule) {$0 == "?"}
                //set id and name
                var charactersToRemove = NSCharacterSet.alphanumericCharacterSet().invertedSet
                addPreset.id = ("".join(ruleParts[0].componentsSeparatedByCharactersInSet(charactersToRemove))).toInt()!
                addPreset.name = ruleParts[0]
                
                var nameParts = split(ruleParts[1]) {$0 == "="}
                if (nameParts[0] == "name"){
                    let str = nameParts[1].cStringUsingEncoding(NSISOLatin1StringEncoding)
                    addPreset.name = String(CString: str!, encoding: NSUTF8StringEncoding)!
                }
                
                var foundID = false
                for cRule in PresetStore.ruleStore.allObjects() {
                    if cRule.id == addPreset.id {
                        cRule.marked=true
                        foundID = true
                    }
                }
                // Events
                //
                // lightTurnedOnEvent light=1
                // doorOpenEvent door=1
                // doorCloseEvent door=0
                // coffeeMade coffee=1
                // temperatureBelow temperature<val
                // temperatureExceeds temperature>val
                // volumeBelow volume<val
                // volumeExceeds volume>val
                // brightnessBelow brightness<val
                // brightnessExceeds brightness>val
                // time time=val
                
                if foundID == false {
                    // split events
                    var components = split(ruleParts[2]) {$0 == "!"}
                    for component in components {
                        
                        //NSLog("Rule \(addPreset.id) - \(component)")
                        var events = split(component) {$0 == "="}
                        switch events[0]{
                        case "light":
                            let val = events[1].toInt()!.toBool()!
                            if val {
                                addPreset.lightTurnedOnEvent = true
                            }else{
                                addPreset.lightTurnedOnEvent = false // TODO
                            }
                        case "door":
                            let val = events[1].toInt()!.toBool()!
                            NSLog("Door : \(events[1]) \(val)")
                            if val {
                                addPreset.doorOpenEvent = val
                            }else{
                                addPreset.doorCloseEvent = !val // TODO
                            }
                        case "coffee": addPreset.coffeeMade = events[1].toInt()!.toBool()!
                        case "time" :
                            var dateFormat = NSDateFormatter()
                            dateFormat.dateFormat = "hhmm"
                            addPreset.time = dateFormat.dateFromString(events[1])
                        default : break
                        }
                        
                        // below events
                        var events2 = split(component) {$0 == "<"}
                        switch events2[0]{
                        case "temperature":
                            addPreset.temperatureBelow = events2[1]
                        case "volume":
                            addPreset.volumeBelow = events2[1]
                        case "brightness":
                            addPreset.brightnessBelow = events2[1]
                        default : break
                        }
                        
                        //exceeds events
                        var events3 = split(component) {$0 == ">"}
                        switch events3[0]{
                        case "temperature":
                            addPreset.temperatureExceeds = events3[1]
                        case "volume":
                            addPreset.volumeExceeds = events3[1]
                        case "brightness":
                            addPreset.brightnessExceeds = events3[1]
                        default : break
                        }
                    }
                    
                    // Actions
                    //
                    // lightTurnedOnAction light=1
                    // doorOpenAction door=1
                    // doorCloseAction door=0
                    // shouldMakeCoffee coffee=1
                    // ventilatorActivation smartilator=val
                    
                    //split actions
                    var components2 = split(ruleParts[3]) {$0 == "!"}
                    for component in components2 {
                        var actions = split(component) {$0 == "="}
                        switch actions[0]{
                        case "light":
                            let val = count(actions[1])>0
                            if val{
                                addPreset.lightTurnOnAction = true
                                addPreset.lightActionHex = actions[1]
                            }else{
                                // turn off TODO
                            }
                        case "door":
                            let val = actions[1].toInt()!.toBool()!
                            if val{
                                addPreset.doorOpenAction = true
                            }else{
                                addPreset.doorCloseAction = true
                            }

                        case "smartilator":
                            addPreset.ventilatorPowerAction = actions[1].toInt()!
                            addPreset.ventilatorPowerActionB = true
                        case "coffee": addPreset.shouldMakeCoffee = actions[1].toInt()!.toBool()!
                        default : break
                        }
                    }
                    
                    //add to tableview later
                    Preset.currentPreset.ruleAdded = true
                    Constants.ruleToAdd?.append(addPreset)
                }
            }
            
            var deleteRules: [Preset] = []
            for rule in PresetStore.ruleStore.allObjects() {
                if rule.marked==false{
                    deleteRules.append(rule)
                    Preset.currentPreset.ruleDeleted = true
                }
            }
            for rule in deleteRules{
                PresetStore.ruleStore.removeItem(rule)
            }
            //NSLog("Devices: \(rules)")
        }
    }

    
    
    // MARK: Light
    
    ///Receive light color
    func receiveLightColor() {
        let url = Constants.lightColorPath
        NetworkController.receiveURL(url)
        
        //Listen for answers
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedLightColor:"), name: "receivedURL/"+Constants.lightColorPath, object: nil)
    }
    
    ///Received light color
    dynamic private func receivedLightColor(notification: NSNotification) {
        //Stop listening for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.lightColorPath, object: nil)
        
        //Parse answer
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var rgbVal : UInt32 = 0
            //Scan for hex string
            let scanner: AnyObject = NSScanner.localizedScannerWithString(dataStr)
            
            //If found, parse
            if scanner.scanHexInt(&rgbVal) {
                let color = UIColor(red: CGFloat((rgbVal & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbVal & 0x00FF00) >> 8)/255.0, blue: CGFloat((rgbVal & 0x0000FF))/255.0, alpha: 1.0)
                var alpha : CGFloat = 1.0
                color.getHue(&lightHue, saturation: &lightSaturation, brightness: &lightBrightness, alpha: &alpha)
                //NSLog("Color received H: \(lightSaturation), S: \(lightSaturation), B: \(lightBrightness)")
            }
        }
    }
    
    func receiveMoodlightRules() {
        let url = Constants.ambR1Path
        NetworkController.receiveURL(url)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedMoodlightR1:"), name: "receivedURL/"+Constants.ambR1Path, object: nil)
        
        
        let url2 = Constants.ambR2Path
        NetworkController.receiveURL(url2)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedMoodlightR2:"), name: "receivedURL/"+Constants.ambR2Path, object: nil)
        
        let url3 = Constants.ambR3Path
        NetworkController.receiveURL(url3)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedMoodlightR3:"), name: "receivedURL/"+Constants.ambR3Path, object: nil)
    }
    
    func receivedMoodlightR1(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ambR1Path, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            moodlightSRule1 = dataStr.toInt()!.toBool()!
            
            NSLog("Moodlight Rule1 received \(moodlightSRule1)")
        }
    }
    
    func receivedMoodlightR2(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ambR2Path, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var components = split(dataStr) {$0 == "?"}
            moodlightSRule2 = components[0].toInt()!.toBool()!
            moodlightSRule2Brightness = components[1]
            NSLog("Moodlight Rule1 received \(moodlightSRule2)  \(moodlightSRule2Brightness)")
        }
    }
    
    func receivedMoodlightR3(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ambR3Path, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var components = split(dataStr) {$0 == "?"}
            moodlightSRule3 = components[0].toInt()!.toBool()!
            moodlightSRule3T1 = components[1]
            moodlightSRule3T2 = components[2]
            //NSLog("Moodlight Rule3 received \(moodlightSRule3) \(moodlightSRule3T1) - \(moodlightSRule3T2)")
        }
    }
    
    /// Sends the current light values to server
    func sendLight() {
        //Get color
        let color = UIColor(hue: lightHue, saturation: lightSaturation, brightness: lightBrightness, alpha: 1.0)
        
        //Convert color to hex string
        var getRed = CGFloat()
        var getGreen = CGFloat()
        var getBlue = CGFloat()
        var getAlpha = CGFloat()
        color.getRed(&getRed, green: &getGreen, blue: &getBlue, alpha: &getAlpha)
        var redHexString = String(NSString(format:"%2X", Int(getRed*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var blueHexString = String(NSString(format:"%2X", Int(getGreen*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        var greenHexString = String(NSString(format:"%2X", Int(getBlue*CGFloat(255.9999)))).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if count(redHexString) == 1{
            redHexString = "0" + redHexString
        }
        if count(blueHexString) == 1{
            blueHexString = "0" + blueHexString
        }
        if count(greenHexString) == 1{
            greenHexString = "0" + greenHexString
        }
        let colorHEX : String = redHexString + blueHexString + greenHexString
        
        //Send hex string to server
        NetworkController.sendParameters(["param2" : Constants.lightColorPath, "param3" : colorHEX], completionNotification: "lightSent")
    }
    
    // MARK: Sensors
    
    ///Request current temperature in celsius
    func receiveSensorTemperature() {
        let url = Constants.temperaturePath
        NetworkController.receiveURL(url)
        
        //Listen for answer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedSensorTemperature:"), name: "receivedURL/"+Constants.temperaturePath, object: nil)
    }

    dynamic private func receivedSensorTemperature(notification: NSNotification) {
        //Stop listening for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.temperaturePath, object: nil)
        
        //Parse answer
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            temperature = dataStr.toInt()!
            NSLog("Temperature received \(temperature)")
        }
    }
    
    ///Request current brightness in lux
    func receiveSensorBrightness() {
        let url = Constants.brightnessPath
        NetworkController.receiveURL(url)
        
        //Listen for answers
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedSensorBrightness:"), name: "receivedURL/"+Constants.brightnessPath, object: nil)
    }
    
    dynamic private func receivedSensorBrightness(notification: NSNotification) {
        //Stop listening for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.brightnessPath, object: nil)
        
        //Parse answer
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            brightness = dataStr.toInt()!
            NSLog("Brightness received \(temperature)")
        }
    }
    
    ///Request current volume in dB
    func receiveSensorVolume() {
        let url = Constants.volumePath
        NetworkController.receiveURL(url)
        
        //Listen for answer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedSensorVolume:"), name: "receivedURL/"+Constants.volumePath, object: nil)
    }
    
    dynamic private func receivedSensorVolume(notification: NSNotification) {
        //Stop listening for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.volumePath, object: nil)
        
        //Parse answer
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            volume = dataStr.toInt()!
            NSLog("Volume received \(temperature)")
        }
    }
    
    // MARK: Door
    
    ///Receive door state
    func receiveDoorState() {
        let url = Constants.doorStatePath
        NetworkController.receiveURL(url)
        
        //Set up observers for network
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedDoorState:"), name: "receivedURL/"+Constants.doorStatePath, object: nil)
    }
    
    dynamic private func receivedDoorState(notification: NSNotification) {
        //Set up observers for network
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.doorStatePath, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            
            if let doorState = dataStr.toInt()?.toBool(){
                //Changes? Inform user
                if doorState != doorOpened {
                    if doorState {
                        SmartDoorViewController.showDoorOpenNotification()
                    } else {
                        SmartDoorViewController.showDoorClosedNotification()
                    }
                }
                
                self.doorClosed = !doorState
                self.doorOpened = doorState
                
                NSLog("Door state received \(doorState)")
            } else {
                NSLog("Nil")
            }
        }
    }
    
    ///Send request to open door to server
    func sendOpenDoor() {
        NetworkController.sendParameters(["param2" : Constants.doorStatePath, "param3" : "1"], completionNotification: "openDoorSent")
    }
    
    ///Send request to close door to server
    func sendCloseDoor() {
        NetworkController.sendParameters(["param2" : Constants.doorStatePath, "param3" : "0"], completionNotification: "openDoorSent")
    }
    
    // MARK: Ventilator
    
    func receiveVentilatorPower() {
        let url = Constants.ventilatorPowerPath
        NetworkController.receiveURL(url)
        
        //Listen for answer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedVentilatorPower:"), name: "receivedURL/"+Constants.ventilatorPowerPath, object: nil)
    }
    
    func receivedVentilatorPower(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ventilatorPowerPath, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            let receivedPower = CGFloat(dataStr.toInt()!) / 100.0
            
            if ventilatorActivation {
                ventilatorPower = receivedPower
            } else {
                if receivedPower != 0 {
                    ventilatorPower = receivedPower
                    NSNotificationCenter.defaultCenter().postNotificationName("ventilatorTurnOn", object: nil)
                }
            }
            
            NSLog("Ventilator power received \(receivedPower)")
        }
    }
    
    func receiveVentilatorRules() {
        let url = Constants.ventRPath
        NetworkController.receiveURL(url + "1")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedVentilatorRule1:"), name: "receivedURL/"+url + "1", object: nil)
    
        NetworkController.receiveURL(url + "2")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedVentilatorRule2:"), name: "receivedURL/"+url + "2", object: nil)

        NetworkController.receiveURL(url + "3")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedVentilatorRule3:"), name: "receivedURL/"+url + "3", object: nil)
    }
    
    func receivedVentilatorRule1(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ventRPath + "1", object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var components = split(dataStr) {$0 == "?"}
            ventRule1 = components[0].toInt()!.toBool()!
            ventRule1From = components[1]
            ventRule1Set = components[2]
        }
    }
    
    func receivedVentilatorRule2(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ventRPath + "2", object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var components = split(dataStr) {$0 == "?"}
            ventRule2 = components[0].toInt()!.toBool()!
            ventRule2From = components[1]
            ventRule2Set = components[2]
        }
    }
    
    func receivedVentilatorRule3(notification: NSNotification) {
        //Stop waiting for answers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.ventRPath + "3", object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            var components = split(dataStr) {$0 == "?"}
            ventRule3 = components[0].toInt()!.toBool()!
            ventRule3From = components[1]
            ventRule3Set = components[2]
        }
    }
    
    ///Update ventilator power value on server
    func sendVentilatorPowerValue() {
        
        //Convert to percentage and send to server
        let power = Int(ventilatorPower*100)
        NetworkController.sendParameters(["param2" : Constants.ventilatorPowerPath, "param3" : "\(power)"], completionNotification: "ventilatorPowerSent")
    }
    
    func turnOffVentilator() {
        //Silently set power
        ventilatorActivation = false
        NetworkController.sendParameters(["param2" : Constants.ventilatorPowerPath, "param3" : "0"], completionNotification: "ventilatorPowerSent")
    }
    
    //Convienience function for sending current power after ventilator was turned off
    func turnOnVentilator() {
        ventilatorActivation = true
        sendVentilatorPowerValue()
    }
    
    // MARK: Coffee
    
    ///Ask server to cook coffee
    func sendCoffeeRequest() {
        coffeeState = true
        NetworkController.sendParameters(["param2" : Constants.coffeePath, "param3" : "1"], completionNotification: "")
    }
    
    func receiveCoffeeState() {
        let url = Constants.coffeePath
        NetworkController.receiveURL(url)
        
        //Set up observers for network
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("receivedCoffeeState:"), name: "receivedURL/"+Constants.coffeePath, object: nil)
    }
    
    dynamic private func receivedCoffeeState(notification: NSNotification) {
        //Set up observers for network
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "receivedURL/"+Constants.coffeePath, object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            
            if let coffeeState = dataStr.toInt()?.toBool() {
                self.coffeeState = coffeeState
                
                NSLog("Coffee state received \(coffeeState)")
            }
        }
    }
    
    // MARK: Events
    
    /// Send "When Door Opens" switch value to server
    ///
    func sendLightDoorOpens() {
        var param3 = ""
        if Preset.currentPreset.moodlightSRule1==true {
            param3 = "1"
        }else {
            param3 = "0"
        }
        NetworkController.sendParameters(["param2" : Constants.ambR1Path, "param3" : param3], completionNotification: "")
    }
    
    /// Send "By Luminosity" switch value to server
    ///
    func sendLightLuminosity() {
        var param3 = ""
        if Preset.currentPreset.moodlightSRule2==true {
            param3 = "1"
        }else {
            param3 = "0"
        }
        param3 += "?" + Preset.currentPreset.moodlightSRule2Brightness.stringByReplacingOccurrencesOfString("%", withString: "")
        NetworkController.sendParameters(["param2" : Constants.ambR2Path, "param3" : param3], completionNotification: "")

    }
    
    /// Send time switch value to server
    ///
    func sendLightTime() {
        var param3 = ""
        if Preset.currentPreset.moodlightSRule3==true {
            param3 = "1"
        }else {
            param3 = "0"
        }
        param3 += "?" + Preset.currentPreset.moodlightSRule3T1.stringByReplacingOccurrencesOfString(":", withString: "")
        param3 += "?" + Preset.currentPreset.moodlightSRule3T2.stringByReplacingOccurrencesOfString(":", withString: "")
        NetworkController.sendParameters(["param2" : Constants.ambR3Path, "param3" : param3], completionNotification: "")
    }
    
    
    ///Update ventilator temperature event
    func sendVentilatorEvent(index : Int) {
        //assert(index < 0, "ERROR: sendVentilatorEvent: Must not provide negative value for index")
        //assert(index > 2, "ERROR: sendVentilatorEvent: Must not provide value larger than 2 for index")
        var param3 = ""
        switch index{
            case 0:
                if Preset.currentPreset.ventRule1==true {
                    param3 = "1"
                }else {
                    param3 = "0"
                }
                param3 += "?" + Preset.currentPreset.ventRule1From
                param3 += "?" + Preset.currentPreset.ventRule1Set.stringByReplacingOccurrencesOfString("%", withString: "")
            case 1:
                if Preset.currentPreset.ventRule2==true {
                    param3 = "1"
                }else {
                    param3 = "0"
                }
                param3 += "?" + Preset.currentPreset.ventRule2From
                param3 += "?" + Preset.currentPreset.ventRule2Set.stringByReplacingOccurrencesOfString("%", withString: "")
            case 2:
                if Preset.currentPreset.ventRule3==true {
                    param3 = "1"
                }else {
                    param3 = "0"
                }
                param3 += "?" + Preset.currentPreset.ventRule3From
                param3 += "?" + Preset.currentPreset.ventRule3Set.stringByReplacingOccurrencesOfString("%", withString: "")
            default: break
        }
        NetworkController.sendParameters(["param2" : Constants.ventRPath+String((index+1)), "param3" : param3], completionNotification: "")
    }
    
    
    
    /// Send Rule
    ///
    ///:param: mode Mode can be add/edit/delete
    func sendRule(mode: String) {
        var idString = String(self.id)
        var eventString = ""
        if mode=="delete" {
            NSLog("\(mode) Rule : \(idString)" )
            NetworkController.sendRule(["param2" : mode, "param3" : idString], completionNotification: "")
        }else{
            // add or edit
            // events which will cause the actions
            
            let nameString = "name=" + self.name //.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
            if(self.lightTurnedOnEvent) {
                eventString += "!light=1"
            }
            if(self.doorOpenEvent) {
                eventString += "!door=1"
            }
            if(self.doorCloseEvent) {
                eventString += "!door=0"
            }
            if(self.coffeeMade) {
                eventString += "!coffee=1"
            }
            if self.temperatureBelow != "" {
                eventString += "!temperature<" + self.temperatureBelow
            }
            if self.temperatureExceeds != "" {
                eventString += "!temperature>" + self.temperatureExceeds
            }
            if self.volumeBelow != "" {
                eventString += "!volume<" + self.volumeBelow
            }
            if self.volumeExceeds != "" {
                eventString += "!volume>" + self.volumeExceeds
            }
            if self.brightnessBelow != "" {
                eventString += "!brightness<" + self.brightnessBelow
            }
            if self.brightnessExceeds != "" {
                eventString += "!brightness>" + self.brightnessExceeds
            }
            if(self.time != nil) {
                var dateFormat = NSDateFormatter()
                dateFormat.dateFormat = "hhmm"
                eventString += "!time=" + dateFormat.stringFromDate(self.time!)
            }
            
            var actionString = ""
            if(self.lightTurnOnAction) {
                actionString += "!light=\(self.lightActionHex)"
            }
            if(self.doorOpenAction) {
                actionString += "!door=1"
            }
            if(self.doorCloseAction) {
                actionString += "!door=0"
            }
            if(self.shouldMakeCoffee) {
                actionString += "!coffee=1"
            }
            if(self.ventilatorPowerActionB) {
                actionString += "!smartilator=\(self.ventilatorPowerAction)"
            }
            
            // Erste "!" löschen
            actionString = actionString.substringFromIndex(advance(actionString.startIndex,1))
            eventString = eventString.substringFromIndex(advance(actionString.startIndex,1))
            eventString = idString + "?" + nameString + "?" + eventString + "?"+actionString
            NSLog("\(mode) Rule : \(eventString)" )
            NetworkController.sendRule(["param2" : mode, "param3" : eventString], completionNotification: "")
        }
    }
    
    /*
    dynamic private func receivedAddRule(notification: NSNotification) {
        //Set up observers for network
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ruleAdded", object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            NSLog("Rule delted:  \(dataStr)")
        }
    }
    
    dynamic private func receivedDeleteRule(notification: NSNotification) {
        //Set up observers for network
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ruleDeleted", object: nil)
        
        if notification.object != nil {
            let dataStr = NetworkController.parseServerDataToString(notification.object as! NSData)
            NSLog("Rule added:  \(dataStr)")
        }
    }
    */
}
