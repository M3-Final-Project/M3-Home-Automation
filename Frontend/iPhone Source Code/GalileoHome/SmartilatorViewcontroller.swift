//
//  SmartilatorViewcontroller.swift
//  GalileoHome
//
//  Created by Alexander Neumann on 10.06.15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class SmartilatorViewController: UIViewController , UITextFieldDelegate{
    @IBOutlet weak var currentPower: UITextField!
    @IBOutlet weak var currentpowerSlider: UISlider!
    @IBOutlet weak var rule1Celcius: UITextField!
    @IBOutlet weak var rule1Percent: UITextField!
    @IBOutlet weak var rule2Celcius: UITextField!
    @IBOutlet weak var rule2Percent: UITextField!
    @IBOutlet weak var rule3Celcius: UITextField!
    @IBOutlet weak var rule3Percent: UITextField!
    @IBOutlet weak var activationButton: UIButton!
    
    @IBOutlet weak var rule1Switch: UISwitch!
    @IBOutlet weak var rule2Switch: UISwitch!
    @IBOutlet weak var rule3Switch: UISwitch!
    @IBOutlet weak var systemRulesView: UIView!
    
    var brake = 0
    
    var smartilatorImage = UIImageView(image: UIImage(named: "FanIconBig")!)
    var currentTextfield = UITextField()
    var disableView = UIView()
    var keyboardHeight: CGFloat = 200.0
    var tediting = false
    var activated = true
    var rotationDegree = 0.0
    var rotationTimer = NSTimer()
    var updateTimer = NSTimer()
    
    var sendRequestPending : Bool = false {
        didSet {
            if sendRequestPending == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.stopUpdating()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    self.startUpdating()
                })
            }
        }
    }
    
    override func viewDidLoad() {
        //No system rules on 4S
        if Constants.runningOn4S {
            systemRulesView.hidden = true
            systemRulesView.userInteractionEnabled = false
        }
        
        
        self.view.backgroundColor = Constants.backgroundColor
        
        //Set up smartilator icon and (de)activation button
        smartilatorImage.frame = CGRectMake(16, 83, 128, 128)
        smartilatorImage.tintColor = UIColor.darkGrayColor()
        self.view.addSubview(smartilatorImage)
        smartilatorImage.accessibilityLabel = "Smartilator Icon"
        activationButton.layer.cornerRadius = 20.0
        activationButton.setTitle("Turn off".localized, forState: .Normal)
        
        currentPower.delegate = self
        rule1Celcius.delegate = self
        rule1Percent.delegate = self
        rule2Celcius.delegate = self
        rule2Percent.delegate = self
        rule3Celcius.delegate = self
        rule3Percent.delegate = self
        
        self.addDoneButtonOnKeyboard(currentPower)
        self.addDoneButtonOnKeyboard(rule1Celcius)
        self.addDoneButtonOnKeyboard(rule1Percent)
        self.addDoneButtonOnKeyboard(rule2Celcius)
        self.addDoneButtonOnKeyboard(rule2Percent)
        self.addDoneButtonOnKeyboard(rule3Celcius)
        self.addDoneButtonOnKeyboard(rule3Percent)
        
        //Turn switches off initially
        rule1Switch.setOn(false, animated: false)
        rule2Switch.setOn(false, animated: false)
        rule3Switch.setOn(false, animated: false)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        
        //Set up view to lay over deactivated elements
        disableView = UIView(frame: CGRect(x: 0, y: 230, width: self.view.frame.width, height: 269))
        disableView.backgroundColor = UIColor.blackColor()
        disableView.alpha = 0.5
        disableView.hidden = true
        disableView.userInteractionEnabled = true
        self.view.addSubview(disableView)
        
        currentpowerSlider.addTarget(self, action: Selector("percentageChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        //Initially set slider
        currentpowerSlider.setValue(Float(Preset.currentPreset.ventilatorPower), animated: true)
        percentageChanged(currentpowerSlider)
        currentpowerSlider.tintColor = Constants.highlightColor1
        rotationTimer = NSTimer.scheduledTimerWithTimeInterval(0.0166/(4.0), target: self, selector: Selector("rotate"), userInfo: nil, repeats: true)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
        startUpdating()
    }
    
    override func viewDidDisappear(animated: Bool) {
        stopUpdating()
    }
    
    func rotate() {
        if activated {
            var transform = CGAffineTransformMakeTranslation(0, 0)
            transform = CGAffineTransformRotate(transform, CGFloat(-self.rotationDegree*M_PI/60.0))
            self.smartilatorImage.transform = transform
            self.rotationDegree+=1.0*Double(Preset.currentPreset.ventilatorPower)
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        sendRequestPending = true
        if((textField.tag<=1&&tediting==false)||(currentTextfield.tag>1&&tediting==true&&textField.tag<=1)){
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.view.frame.origin.y -= keyboardHeight
            self.view.frame.size.height += 200
            UIView.commitAnimations()
        }else if ((textField.tag>1&&tediting==true&&currentTextfield.tag<=1)){
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.view.frame.origin.y += keyboardHeight
            self.view.frame.size.height -= 200
            UIView.commitAnimations()
        }
        for i in 0..<2 {
            Preset.currentPreset.sendVentilatorEvent(i)
        }
        currentTextfield=textField
        tediting = true
        return true
    }
    
    func hideView(sender:UIView?,view hideView: UIView, time: NSTimeInterval) {
        sender?.userInteractionEnabled = false
        let alpha = hideView.alpha
        UIView.animateWithDuration(time, delay: 0.0, options: .CurveEaseOut, animations: {
            hideView.alpha = 0.0
            }, completion: {_ in
                hideView.hidden = true
                hideView.alpha = alpha
                sender?.userInteractionEnabled = true
        })
    }
    
    func showView(sender:UIView?,view showView : UIView, time: NSTimeInterval) {
        sender?.userInteractionEnabled = false
        let alpha = showView.alpha
        showView.alpha = 0.0
        showView.hidden = false
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(time)
        
        showView.alpha = alpha
        sender?.userInteractionEnabled = true
        
        UIView.commitAnimations()
    }
    
    @IBAction func setSmartilator() {
        if !sendRequestPending {
        activated = !activated
        if(activated) {
            dispatch_async(dispatch_get_main_queue(),{
                self.hideView(self.activationButton,view: self.disableView, time: 0.5)
                self.activationButton.setTitle("Turn off".localized, forState: .Normal)
            })
            Preset.currentPreset.turnOnVentilator()
            sendRequestPending = true
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.sendRequestPending = false
            }
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "ventilatorTurnOn", object: nil)
            
        } else {
            dispatch_async(dispatch_get_main_queue(),{
                self.showView(self.activationButton,view: self.disableView, time: 0.3)
                self.activationButton.setTitle("Turn on".localized, forState: .Normal)
            })
            Preset.currentPreset.turnOffVentilator()
            sendRequestPending = true
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.sendRequestPending = false
            }
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("setSmartilator"), name: "ventilatorTurnOn", object: nil)
        }
        }
    }
    
    func addDoneButtonOnKeyboard(view: UIView?)
    {
        
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        doneToolbar.barStyle = UIBarStyle.BlackTranslucent
        var flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        var done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: view, action: "resignFirstResponder")
        var items = [AnyObject]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if let accessorizedView = view as? UITextView {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        } else if let accessorizedView = view as? UITextField {
            accessorizedView.inputAccessoryView = doneToolbar
            accessorizedView.inputAccessoryView = doneToolbar
        }
        
    }
    
    
    func percentageChanged(sender: UISlider){
        sendPower()
        updateUI(force:true)
    }
    
    func updateUI(force:Bool=false) {
        if !sendRequestPending || force {
            //Received power
            var power = Preset.currentPreset.ventilatorPower
            if Preset.currentPreset.ventilatorActivation == false {
                power = 0
            }
            
            //Update percentage textfield
            currentPower.text = String((Int(power*100))) + "%"

            //Update slider
            UIView.animateWithDuration(Constants.sliderSpeed, animations: {
                self.currentpowerSlider.setValue(Float(power), animated: true)
            })
        }
    }
    
    func numberRegex(percent:String)->Bool{
        let percentageRegex = NSRegularExpression(pattern: "^([0-9]|[1-9][0-9]|100)$", options: nil, error: nil)!
        if let match = percentageRegex.firstMatchInString(percent, options: nil,
            range: NSRange(location: 0, length: count(percent.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    func numberRegex2(percent:String)->Bool{
        let percentageRegex = NSRegularExpression(pattern: "^([0-9]|[1-9][0-9]|100)%$", options: nil, error: nil)!
        if let match = percentageRegex.firstMatchInString(percent, options: nil,
            range: NSRange(location: 0, length: count(percent.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if(textField.tag>=1){
            if(numberRegex(textField.text)){
                sendrules()
                
                if(textField.tag==2){
                    let percentage = Float(textField.text.toInt()!)
                    currentpowerSlider!.value = Float(percentage/100)
                }
                textField.text = textField.text + "%"
                return true
            }else if(numberRegex2(textField.text)){
                sendrules()
                if(textField.tag==2){
                    let percentage = Float(textField.text.substringToIndex(advance(textField.text.endIndex,-1)).toInt()!)
                    currentpowerSlider!.value = Float(percentage/100)
                }
                return true
            }
        }else{
            if(numberRegex(textField.text)){
                sendrules()
                return true
            }
        }
        return false
    }
    
    func keyboardWillHide(sender: NSNotification) {
        if(currentTextfield.tag<=1){
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.view.frame.origin.y += keyboardHeight
            self.view.frame.size.height -= 200
            UIView.commitAnimations()
            tediting = false
        }
    }
    
    // MARK: - Network methods
    func updateValues() {
        Preset.currentPreset.receiveVentilatorPower()
        
        //Update rules 1 time / 2 seconds
        if brake == 0 {
            Preset.currentPreset.receiveVentilatorRules()
        }
        brake++
        brake %= Int(1/Constants.updateInterval) * 2
        
        rule1Switch.setOn(Preset.currentPreset.ventRule1, animated: true)
        rule2Switch.setOn(Preset.currentPreset.ventRule2, animated: true)
        rule3Switch.setOn(Preset.currentPreset.ventRule3, animated: true)
        
        rule1Celcius.text = Preset.currentPreset.ventRule1From
        rule1Percent.text = Preset.currentPreset.ventRule1Set.stringByReplacingOccurrencesOfString("%", withString: "") + "%"
        
        rule2Celcius.text = Preset.currentPreset.ventRule2From
        rule2Percent.text = Preset.currentPreset.ventRule2Set.stringByReplacingOccurrencesOfString("%", withString: "") + "%"
        
        rule3Celcius.text = Preset.currentPreset.ventRule3From
        rule3Percent.text = Preset.currentPreset.ventRule3Set.stringByReplacingOccurrencesOfString("%", withString: "") + "%"
        //Update UI
        updateUI()
    }
    
    private func startUpdating() {
        updateTimer.invalidate()
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.updateInterval, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    private func stopUpdating() {
        updateTimer.invalidate()
    }
    
    private func sendPower() {
        //Update preset
        Preset.currentPreset.ventilatorPower = CGFloat(currentpowerSlider.value)
        
        //Inform server
        sendRequestPending = true
        Preset.currentPreset.sendVentilatorPowerValue()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("sentPower"), name: "ventilatorPowerSent", object: nil)
    }
    
    dynamic private func sentPower() {
        //Stop waiting for an answer
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ventilatorPowerSent", object: nil)
        
        //Start updating again after short waiting
        sendRequestPending = false
        
    }
    
    func sendrules() {
        Preset.currentPreset.ventRule1From = rule1Celcius.text
        Preset.currentPreset.ventRule2From = rule2Celcius.text
        Preset.currentPreset.ventRule3From = rule3Celcius.text
        Preset.currentPreset.ventRule1Set = rule1Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
        Preset.currentPreset.ventRule2Set = rule2Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
        Preset.currentPreset.ventRule3Set = rule3Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
        Preset.currentPreset.sendVentilatorEvent(0)
        Preset.currentPreset.sendVentilatorEvent(1)
        Preset.currentPreset.sendVentilatorEvent(2)
    }
    
    @IBAction func ruleSwitchChanged(sender: UISwitch) {
        switch sender{
            case rule1Switch:
                Preset.currentPreset.ventRule1 = sender.on
                Preset.currentPreset.ventRule1From = rule1Celcius.text
                Preset.currentPreset.ventRule1Set = rule1Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
                Preset.currentPreset.sendVentilatorEvent(0)
            case rule2Switch:
                Preset.currentPreset.ventRule2 = sender.on
                Preset.currentPreset.ventRule2From = rule2Celcius.text
                Preset.currentPreset.ventRule2Set = rule2Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
                Preset.currentPreset.sendVentilatorEvent(1)
            case rule3Switch:
                Preset.currentPreset.ventRule3 = sender.on
                Preset.currentPreset.ventRule3From = rule3Celcius.text
                Preset.currentPreset.ventRule3Set = rule3Percent.text.stringByReplacingOccurrencesOfString("%", withString: "")
                Preset.currentPreset.sendVentilatorEvent(2)
            default: break
        }
        
    }

}