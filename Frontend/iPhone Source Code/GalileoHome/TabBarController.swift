//
//  TabBarController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    var networkCheckTimer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.tabbarController = self
        
        // Show status bar
        UIApplication.sharedApplication().statusBarHidden = false
        
        // Set up tabbar
        tabBar.barTintColor = UIColor.whiteColor()
        self.tabBar.tintColor = Constants.highlightColor1
        
        //Listen for changes in Settings.app
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onDefaultsChange", name: NSUserDefaultsDidChangeNotification, object: nil)
        
        NetworkRequestController.requestBeaconID()
        
        networkCheckTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "checkNetwork", userInfo: nil, repeats: true)
    }
    
    func checkNetwork() {
        Constants.checkNetwork()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Settings
    func onDefaultsChange() {
        let standardDefaults = NSUserDefaults.standardUserDefaults()
        
        //Receive from settings and send to server
        if let storedVal : String = standardDefaults.stringForKey("twitter_consumer_key") {
            NetworkController.sendParameters(["param2" : Constants.twitterConsumerKeyPath, "param3" : storedVal])
        }
        
        if let storedVal : String = standardDefaults.stringForKey("twitter_consumer_secret") {
            NetworkController.sendParameters(["param2" : Constants.twitterConsumerSecretPath, "param3" : storedVal])
        }
        
        if let storedVal : String = standardDefaults.stringForKey("twitter_access_token_key") {
            NetworkController.sendParameters(["param2" : Constants.twitterAccessKeyPath, "param3" : storedVal])
        }
        
        if let storedVal : String = standardDefaults.stringForKey("twitter_access_token_secret") {
            NetworkController.sendParameters(["param2" : Constants.twitterAccessSecretPath, "param3" : storedVal])
        }
        
        if let storedVal : String = standardDefaults.stringForKey("beacon_ID") {
            NetworkController.sendParameters(["param2" : Constants.beaconPath, "param3" : storedVal])
        }
    }
}
