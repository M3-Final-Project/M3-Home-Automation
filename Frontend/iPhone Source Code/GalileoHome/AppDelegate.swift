//
//  AppDelegate.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //Check if the user is running the app on 4S
        let height = UIScreen.mainScreen().bounds.size.height
        if (height == 480) {
            Constants.runningOn4S = true
        }
        
        //Fetch Preset in background as often as possible
        UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(
            UIApplicationBackgroundFetchIntervalMinimum)
        
        // White status bar
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        //Register for local notifications
        if(UIApplication.instancesRespondToSelector(Selector("registerUserNotificationSettings:"))) {
            UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Alert | .Sound | .Badge, categories: nil))
        }
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        //Load App from storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let startVC : TabBarController = storyboard.instantiateInitialViewController() as! TabBarController
        self.window?.rootViewController = startVC
        self.window?.makeKeyAndVisible()
        
        //Decide wether to show login screen or not
        let loginVC : LoginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
        if NetworkController.ipAddrStr == "" || NetworkController.passwordStr == "" {
            startVC.presentViewController(loginVC, animated: false, completion: nil)
        }
        
        return true
    }
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        //Fetch preset
        Preset.currentPreset.updatePreset()
        
        //Assume we always fetch successfully
        completionHandler(.NewData)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        PresetStore.saveStores()
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        PresetStore.saveStores()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        PresetStore.loadStores()
    }
    

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        PresetStore.loadStores()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        PresetStore.saveStores()
    }


}

