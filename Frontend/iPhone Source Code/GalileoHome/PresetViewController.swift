//
//  SecondViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class PresetViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var editMode = false
    var editButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var hintLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //Set background color
        self.view.backgroundColor = Constants.backgroundColor
        tableView.backgroundColor = Constants.backgroundColor
        
        //Prevent iOS from creating inset
        self.automaticallyAdjustsScrollViewInsets = false;
        
        //Color table view separator
        tableView.separatorColor = Constants.backgroundColor
        
        //Setup navigation bar
        editButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: Selector("editPressed"))
        doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("donePressed"))
        self.navigationItem.leftBarButtonItem = editButton
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("addPressed"))
        
        //Explain the tab to the user
        let rect = UIScreen.mainScreen().bounds
        hintLabel = UILabel(frame: CGRectMake(20, rect.origin.y+100, rect.size.width - 40, 200))
        hintLabel.textColor = UIColor.darkGrayColor()
        hintLabel.font = UIFont.boldSystemFontOfSize(15.0)
        hintLabel.textAlignment = .Center
        hintLabel.numberOfLines = 0
        hintLabel.lineBreakMode = .ByWordWrapping
        hintLabel.text = "Presets store current settings of your smart home. \n\nCreate a new preset by tapping the plus sign in the top right corner.".localized
        hintLabel.hidden = true
        self.view.addSubview(hintLabel)
    }
    
    override func viewDidAppear(animated: Bool) {
        //If not canceled
        if let preset = Constants.presetToAdd {
            //Hide hint
            UIView.hideView(view: hintLabel, time: 0.5)
            
            //Add to database
            PresetStore.mainStore.addPreset(preset)
            
            //Insert in table view
            let count = PresetStore.mainStore.allObjects().count
            let path = NSIndexPath(forRow: count-1, inSection: 0)
            tableView.insertRowsAtIndexPaths([path], withRowAnimation: .Top)
        }
        
        if Constants.presetEdited == true {
            tableView.reloadData()
            Constants.presetEdited = false
        }
        
        //Delete preset after adding to database
        Constants.presetToAdd = nil
        
        if PresetStore.mainStore.count() == 0 && hintLabel.hidden {
            UIView.showView(view: hintLabel, time: 0.5)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Button action methods
    func editPressed() {
        editMode=true
        setEditing(editMode, animated: true)
        self.navigationItem.leftBarButtonItem = doneButton
        tableView.setEditing(editMode, animated: true)
    }
    
    func donePressed(){
        editMode=false
        setEditing(editMode, animated: true)
        self.navigationItem.leftBarButtonItem = editButton
        tableView.setEditing(editMode, animated: true)
        
    }
    
    func addPressed() {
        //Present view
        Constants.presetToAdd = nil
        self.performSegueWithIdentifier("modalAdding", sender: self)
        
        //Handling of new presets after dismissing VC -> viewDidAppear
    }

    //MARK : Table View Delegate / Data Source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PresetStore.mainStore.allObjects().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.tintColor = Constants.highlightColor1
            cell!.accessoryType = .DetailButton
        }
        cell!.textLabel?.text = PresetStore.mainStore.allObjects()[indexPath.row].name
        return cell!
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        PresetStore.mainStore.move(sourceIndexPath.row, toIndex: destinationIndexPath.row)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //Delete from database
            let deletePreset = PresetStore.mainStore.allObjects()[indexPath.row]
            PresetStore.mainStore.removeItem(deletePreset)
            
            //Delete with animation
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            //Show hint if neccessary
            if PresetStore.mainStore.count() == 0 && hintLabel.hidden {
                UIView.showView(view: hintLabel, time: 0.5)
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        PresetStore.mainStore.pushPresetAtIndexToServer(indexPath.row)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        Constants.presetToEdit = PresetStore.mainStore.allObjects()[indexPath.row]
        Constants.presetEditIndex = indexPath.row
        
        performSegueWithIdentifier("modalAdding", sender: self)
        
        
    }
}