//
//  RuleViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 17/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class RuleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var editMode = false
    var editButton = UIBarButtonItem()
    var doneButton = UIBarButtonItem()
    var hintLabel = UILabel()
    var updateTimer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set background color
        self.view.backgroundColor = Constants.backgroundColor
        tableView.backgroundColor = Constants.backgroundColor
        
        //Prevent iOS from creating inset
        self.automaticallyAdjustsScrollViewInsets = false;
        
        //Color table view separator
        tableView.separatorColor = Constants.backgroundColor
        
        //Setup navigation bar
        editButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: Selector("editPressed"))
        doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("donePressed"))
        self.navigationItem.leftBarButtonItem = editButton
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("addPressed"))
        
        //Little logo above table view
        let logo = UIImageView(image: UIImage(named: "TableViewHint"))
        logo.frame = CGRectMake((UIScreen.mainScreen().bounds.width/2)-25, -120, 50, 50)
        logo.tintColor = UIColor.darkGrayColor()
        tableView.addSubview(logo)
        
        //Little hint above table view
        let label = UILabel(frame: CGRectMake(0, -70, UIScreen.mainScreen().bounds.width, 20))
        label.textAlignment = .Center
        label.textColor = UIColor.darkGrayColor()
        label.font = UIFont.systemFontOfSize(11.0)
        label.text = "GalileoHome updates automatically.".localized
        tableView.addSubview(label)
        
        //Explain the tab to the user
        let rect = UIScreen.mainScreen().bounds
        hintLabel = UILabel(frame: CGRectMake(20, rect.origin.y+100, rect.size.width - 40, 200))
        hintLabel.textColor = UIColor.darkGrayColor()
        hintLabel.font = UIFont.boldSystemFontOfSize(15.0)
        hintLabel.textAlignment = .Center
        hintLabel.numberOfLines = 0
        hintLabel.lineBreakMode = .ByWordWrapping
        hintLabel.text = "Rules can adjust your devices for you when specific events occur. \n\nCreate a new rule by tapping the plus sign in the top right corner. \n\nRules are synced across all your GalileoHome apps.".localized
        hintLabel.hidden = true
        self.view.addSubview(hintLabel)
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        if Constants.ruleToEdit != nil {
            if let i = PresetStore.ruleStore.indexOfItem(Constants.ruleToEdit!) {
                tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: i, inSection: 0)], withRowAnimation: .Fade)
            }
            Constants.ruleToEdit = nil
        }
        
        startUpdating()
    }
    
    override func viewDidDisappear(animated: Bool) {
        stopUpdating()
    }
    
    /// reload rules
    func updateTableView() {
        Preset.currentPreset.receivePresets()
        
        //If not canceled
        if (Constants.ruleToDelete != nil){
            for ruletoDelete in Constants.ruleToDelete! {
                let idx = find(PresetStore.ruleStore.items, ruletoDelete)
                let path = NSIndexPath(forRow: idx!, inSection: 0)
                PresetStore.ruleStore.removeItem(ruletoDelete)
            }
        }
        Constants.ruleToDelete = []
        
        if Preset.currentPreset.ruleDeleted == true{
            //PresetStore.ruleStore.items.sort({ $0.id < $1.id})
            tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Fade)
            Preset.currentPreset.ruleDeleted = false
        }

        
        if (Constants.ruleToAdd != nil) {
            for ruletoAdd in Constants.ruleToAdd! {
                //Add to database
                PresetStore.ruleStore.addPreset(ruletoAdd)
                
                //Insert in table view
                let count = PresetStore.ruleStore.allObjects().count
                let path = NSIndexPath(forRow: count-1, inSection: 0)
                //tableView.insertRowsAtIndexPaths([path], withRowAnimation: .Top)
                tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Fade)
            }
        }
        
        //Delete preset after adding to database
        Constants.ruleToAdd = []
        
        if Preset.currentPreset.ruleAdded == true{
            //PresetStore.ruleStore.items.sort({ $0.id < $1.id})
            Preset.currentPreset.ruleAdded = false
        }
        
        //Show hint if neccessary
        if PresetStore.ruleStore.count() == 0 && hintLabel.hidden {
            UIView.showView(view: hintLabel, time: 0.5)
        } else if PresetStore.ruleStore.count() != 0 && !hintLabel.hidden {
            UIView.hideView(view: hintLabel, time: 0.5)
        }
    }
    
    
    //MARK: - Button action methods
    func editPressed() {
        editMode=true
        setEditing(editMode, animated: true)
        self.navigationItem.leftBarButtonItem = doneButton
        tableView.setEditing(editMode, animated: true)
    }
    
    func donePressed(){
        editMode=false
        setEditing(editMode, animated: true)
        self.navigationItem.leftBarButtonItem = editButton
        tableView.setEditing(editMode, animated: true)
        
    }
    
    func addPressed() {
        //Present view
        Constants.ruleToAdd = nil
        self.performSegueWithIdentifier("modalAdding", sender: self)
        
        //Handling of new presets after dismissing VC -> viewDidAppear
    }
    
    //MARK: - Table View Delegate / Data Source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PresetStore.ruleStore.allObjects().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.tintColor = Constants.highlightColor1
            cell!.accessoryType = .DetailButton
        }
        let idx = indexPath.row
        if idx >= PresetStore.ruleStore.allObjects().count {
            cell!.textLabel?.text = "Unknown".localized
        }else{
            cell!.textLabel?.text = PresetStore.ruleStore.allObjects()[indexPath.row].name
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        PresetStore.ruleStore.move(sourceIndexPath.row, toIndex: destinationIndexPath.row)
        
        
        
    }
    
    func tableView(tableView: UITableView, willBeginEditingRowAtIndexPath indexPath: NSIndexPath) {
        NSLog("")
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //Delete from database
            let deletePreset = PresetStore.ruleStore.allObjects()[indexPath.row]
            deletePreset.sendRule("delete")
            PresetStore.ruleStore.removeItem(deletePreset)
            
            //Delete with animation
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        PresetStore.ruleStore.pushPresetAtIndexToServer(indexPath.row)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        Constants.ruleToEdit = PresetStore.ruleStore.allObjects()[indexPath.row]
        Constants.ruleEditIndex = indexPath.row
        
        performSegueWithIdentifier("modalAdding", sender: self)
    }
    
    // MARK: - Network methods
    
    func updateValues() {
        updateTableView()
    }
    
    func startUpdating() {
        //updateValues()
        updateTimer.invalidate()
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    func stopUpdating() {
        updateTimer.invalidate()
    }

}