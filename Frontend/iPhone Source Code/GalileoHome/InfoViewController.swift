//
//  InfoViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 20/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var infoText: UILabel!
    @IBOutlet weak var visitWebsiteButton: UIButton!
    @IBOutlet weak var teamBarButton: UIBarButtonItem!
    @IBOutlet weak var teamView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Apply tint to logo
        let logoImage = UIImage(named: "GalileoHomeClear1024.png")!.imageWithRenderingMode(.AlwaysTemplate)
        logo.image = logoImage
        logo.tintColor = Constants.highlightColor1
        
        //Round rect of visit website button
        visitWebsiteButton.layer.cornerRadius = 20.0
        
        teamView.hidden = true
        
        //Set info text
        infoText.text = "This App was developed as part of Multimodal Media Madness hosted by Media Computing Group of RWTH Aachen University.\n\nFor more information visit\nhttp://hci.rwth-aachen.de/m3_ss15".localized
        
        //Less text on 4S
        if Constants.runningOn4S {
            infoText.text = "This App was developed as part of Multimodal Media Madness hosted by Media Computing Group of RWTH Aachen University.\n\nFor more information visit\nhttp://hci.rwth-aachen.de/m3_ss15".localized
            var range = infoText.text?.rangeOfString("\n\n")
            range?.startIndex = infoText.text!.startIndex
            infoText.text!.replaceRange(range!, with: "\n\n\n\n\n")
        }
    }
    
    @IBAction func visitWebsitePressed(sender: AnyObject) {
        //Open M3 webpage in Safari
        let url = NSURL(string: "http://hci.rwth-aachen.de/m3_ss15")
        UIApplication.sharedApplication().openURL(url!)
    }
    
    @IBAction func teamPressed(sender: AnyObject) {
        if teamBarButton.title == "Team" {
            teamBarButton.title = "Website"
            UIView.showView(view: teamView, time: 0.5)
        } else {
            teamBarButton.title = "Team"
            UIView.animateWithDuration(0.5, animations: {
                self.teamView.alpha = 0.0
                }, completion: {
                    (value: Bool) in
                    self.teamView.hidden = true
                    self.teamView.alpha = 1.0
            })
        }
    }
    
}
