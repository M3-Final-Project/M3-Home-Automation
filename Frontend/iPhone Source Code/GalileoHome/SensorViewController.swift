//
//  SensorViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 10/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class SensorViewController: UIViewController {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var brightnessLabel: UILabel!
    @IBOutlet weak var loudnessLabel: UILabel!
    
    @IBOutlet weak var tempValueLabel: UILabel!
    @IBOutlet weak var brightValueLabel: UILabel!
    @IBOutlet weak var soundValueLabel: UILabel!
    
    @IBOutlet weak var tempImage: UIImageView!
    @IBOutlet weak var lightImage: UIImageView!
    @IBOutlet weak var soundImage: UIImageView!
    override func viewDidLoad() {
        self.view.backgroundColor = Constants.backgroundColor
        temperatureLabel.text = "Temperature".localized
        brightnessLabel.text = "Brightness".localized
        loudnessLabel.text = "Loudness".localized
        
        tempImage.tintColor = UIColor.darkGrayColor()
        lightImage.tintColor = UIColor.darkGrayColor()
        soundImage.tintColor = UIColor.darkGrayColor()
        
        tempValueLabel.text = "Unknown".localized
        brightValueLabel.text = "Unknown".localized
        soundValueLabel.text = "Unknown".localized
        updateValues()
    }
    
    var updateTimer = NSTimer()
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.updateInterval, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(animated: Bool) {
        updateTimer.invalidate()
    }
    
    func updateValues() {
        Preset.currentPreset.receiveSensorTemperature()
        Preset.currentPreset.receiveSensorBrightness()
        Preset.currentPreset.receiveSensorVolume()
        
        if let temp = Preset.currentPreset.temperature {
            tempValueLabel.text = "\(temp)" + " °C"
        } else {
            tempValueLabel.text = "Unknown".localized
        }
        
        if let bright = Preset.currentPreset.brightness {
            brightValueLabel.text = "\(bright)" + " Lux"
        } else {
            brightValueLabel.text = "Unknown".localized
        }
        
        if let volume = Preset.currentPreset.volume {
            soundValueLabel.text = "\(volume)" + " dB"
        } else {
            soundValueLabel.text = "Unknown".localized
        }
    }
}
