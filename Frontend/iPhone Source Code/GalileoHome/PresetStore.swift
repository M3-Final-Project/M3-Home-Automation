//
//  PresetStore.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 14/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class PresetStore: NSObject {
    ///Array containing all presets
    internal var items = [Preset]()
    
    var idCount = 1
    
    ///Single instance of preset store
    static let mainStore = PresetStore()
    
    ///Single instance of rule store
    static let ruleStore = PresetStore()

    ///Sends preset to server
    func pushPresetAtIndexToServer(index: Int) {
        //get preset to push
        let preset = self.allObjects()[index]
        if(preset.lightActivation) {
            preset.sendLight()
        }
        if(preset.doorOpened) {
            preset.sendOpenDoor()
        }
        if(preset.doorClosed) {
            preset.sendCloseDoor()
        }
        if(preset.shouldMakeCoffee) {
            preset.sendCoffeeRequest()
        }
        if(preset.ventilatorActivation) {
            preset.sendVentilatorPowerValue()
        }
    }
    

    /// Deletes all presets in database
    func clearDatabase() {
        items = [Preset]()
    }
    
    ///Returns number of Presets in store
    func count() -> Int {
        return items.count
    }
    
    ///Returns array containing all presets
    func allObjects() -> [Preset] {
        return items
    }

    ///Adds item to database
    func addPreset(preset : Preset) {
        items.insert(preset, atIndex: items.count)
        idCount++
    }
    
    ///Removes item from database
    func removeItem(preset : Preset) {
        if let index = find(items, preset) {
            items.removeAtIndex(index)
        }
    }
    
    ///Replace item at position index with a new item
    func replaceItem(index : Int, newItem : Preset) {
        if index < 0 || index >= allObjects().count {
            return
        }
        
        items[index] = newItem
    }
    
    func indexOfItem(item : Preset) -> Int? {
        for i in 0...items.count-1 {
            if items[i].id == item.id {
                return i
            }
        }
        return nil
    }
    
    ///Move preset from fromIndex to toIndex
    func move(fromIndex : Int, toIndex : Int) {
        if fromIndex == toIndex {return}
        if fromIndex >= items.count || toIndex >= items.count {return}
        
        var preset = items[fromIndex]
        items.removeAtIndex(fromIndex)
        items.insert(preset, atIndex: toIndex)
    }
    
    ///Saves both standard stores (main and rule) in user defaults
    class func saveStores() {
        let mainData = NSKeyedArchiver.archivedDataWithRootObject(PresetStore.mainStore.allObjects())
        let ruleData = NSKeyedArchiver.archivedDataWithRootObject(PresetStore.ruleStore.allObjects())
        NSUserDefaults.standardUserDefaults().setObject(mainData, forKey: "mainStore")
        NSUserDefaults.standardUserDefaults().setObject(ruleData, forKey: "ruleStore")
    }
    
    ///Load both standard stores (main and rule) from user defaults
    class func loadStores() {
        //Load data from user defaults
        let mainData = NSUserDefaults.standardUserDefaults().objectForKey("mainStore") as? NSData
        
        if let mainData = mainData {
            //Get array from data
            let mainItems = NSKeyedUnarchiver.unarchiveObjectWithData(mainData) as? [Preset]
            
            //If array not nil -> replace store
            if let mainItems = mainItems {
                PresetStore.mainStore.items = mainItems
            }
            
        }
        
        
        //Same thing as above for PresetStore.ruleStore
        let ruleData = NSUserDefaults.standardUserDefaults().objectForKey("ruleStore") as? NSData
        
        if let ruleData = ruleData {
            let ruleItems = NSKeyedUnarchiver.unarchiveObjectWithData(ruleData) as? [Preset]
            
            if let ruleItems = ruleItems {
                PresetStore.ruleStore.items = ruleItems
            }
        }
        
    }
    
    }
