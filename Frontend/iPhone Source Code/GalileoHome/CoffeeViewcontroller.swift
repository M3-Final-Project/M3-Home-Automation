//
//  KaffeemaschineViewcontroller.swift
//  GalileoHome
//
//  Created by Alexander Neumann on 10.06.15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class CoffeeViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var coffeeButton: UIButton!
    @IBOutlet weak var twitterLabel: UILabel!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var twitterMessageLabel: UILabel!
    @IBOutlet weak var twitterMessageHintLabel: UILabel!
    @IBOutlet weak var twitterView: UIView!
    
    var localizedButtonTitle : String? = ""
    var coffeeImageButton: UIButton!
    var twitterSetUp = true //All values in settings set?
    var twitterConsumerKey : String = ""
    var twitterConsumerSecret : String = ""
    var twitterAccessTokenKey : String = ""
    var twitterAccessTokenSecret : String = ""
    var coffeeTimer = NSTimer()
    
    var keyboardHeight: CGFloat = 0.0
    
    
    @IBOutlet weak var twitterSwitch: UISwitch!
    override func viewDidLoad() {
        //No twitter on 4S
        if Constants.runningOn4S {
            twitterView.hidden = true
            twitterView.userInteractionEnabled = false
        }
        
        self.view.backgroundColor = Constants.backgroundColor
        coffeeButton.backgroundColor = Constants.highlightColor1
        coffeeButton.layer.cornerRadius = 20.0
        coffeeButton.addTarget(self, action: Selector("coffeeButtonPressed:"), forControlEvents: .TouchUpInside)
        localizedButtonTitle = coffeeButton.titleLabel?.text
        
        twitterMessageHintLabel.text = "Sent when coffee is made".localized
        twitterMessageLabel.text = "Twitter Message".localized
        
        let coffeeImage = UIImage(named: "Coffee-Clear1024.png")!.imageWithRenderingMode(.AlwaysTemplate)
        
        coffeeImageButton = UIButton(frame: CGRectMake(UIScreen.mainScreen().bounds.width/2 - 100, 80, 200, 200))
        coffeeImageButton.addTarget(self, action: Selector("coffeeButtonPressed:"), forControlEvents: .TouchUpInside)
        coffeeImageButton.setImage(coffeeImage, forState: .Normal)
        coffeeImageButton.tintColor = UIColor.darkGrayColor()
        coffeeImageButton.accessibilityLabel = "Make coffee".localized
        coffeeImageButton.accessibilityHint = "Press to cook a coffee".localized
        
        activityIndicator.hidden = true
        
        //Check if coffee machine is busy - update UI if necessary
        if Preset.currentPreset.coffeeState {
            //Check for results
            coffeeTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "checkCoffeeState", userInfo: nil, repeats: true)
            coffeeButton.userInteractionEnabled = false
            coffeeButton.setTitle("", forState: .Normal)
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
        }
        
        if !Constants.twitterEnabled {
            twitterSwitch.setOn(false, animated: false)
        }
        
        self.view.addSubview(coffeeImageButton)
        
        
        if let messageData = NetworkController.syncRequest(Constants.twitterMessagePath) {
            messageField.text = NetworkController.parseServerDataToString(messageData)
        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    }
    
    func coffeeButtonPressed(sender: UIButton){
        //Inform server
        Preset.currentPreset.sendCoffeeRequest()
        
        //Update button
        coffeeButton.userInteractionEnabled = false
        coffeeButton.setTitle("", forState: .Normal)
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        
        
        //Check for results
        coffeeTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "checkCoffeeState", userInfo: nil, repeats: true)
    }
    
    func checkCoffeeState() {
        Preset.currentPreset.receiveCoffeeState()
        
        //Coffee done - reset UI
        if !Preset.currentPreset.coffeeState {
            coffeeButton.setTitle(localizedButtonTitle, forState: .Normal)
            coffeeButton.userInteractionEnabled = true
            activityIndicator.hidden = true
            activityIndicator.stopAnimating()
            coffeeTimer.invalidate()
        }
    }
    
    func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let k = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue().size.height {
                keyboardHeight = k - tabBarController!.tabBar.frame.height - 35
                UIView.animateWithDuration(0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
        
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.view.frame.origin.y -= keyboardHeight
            UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        
        self.view.frame.origin.y += keyboardHeight
        //self.view.frame.size.height -= keyboardHeight
        UIView.commitAnimations()
        return true
    }
    
    func checkTwitter() {
        let standardDefaults = NSUserDefaults.standardUserDefaults()
        
        //Check consumer key
        if let storedVal : String? = standardDefaults.stringForKey("twitter_consumer_key") {
            if storedVal != nil && storedVal != "" {
                twitterConsumerKey = storedVal!
            } else {
                twitterSetUp = false
            }
        }
        
        //Check consumer secret
        if let storedVal : String? = standardDefaults.stringForKey("twitter_consumer_secret") {
            if storedVal != nil && storedVal != "" {
                twitterConsumerSecret = storedVal!
            } else {
                twitterSetUp = false
            }
        }
        
        //Check access token key
        if let storedVal : String? = standardDefaults.stringForKey("twitter_access_token_key") {
            if storedVal != nil && storedVal != "" {
                twitterAccessTokenKey = storedVal!
            } else {
                twitterSetUp = false
            }
        }
        
        //Check access token secret
        if let storedVal : String? = standardDefaults.stringForKey("twitter_access_token_secret") {
            if storedVal != nil && storedVal != "" {
                twitterAccessTokenSecret = storedVal!
            } else {
                twitterSetUp = false
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
        checkTwitter()
        
        if twitterSetUp == false {
            twitterSwitch.setOn(false, animated: true)
        }
    }
    
    
    @IBAction func twitterSwitchChanged(sender: UISwitch) {
        checkTwitter()
        
        if sender.on && !twitterSetUp {
            //Tell user to set keys in settings.app
            var alertController = UIAlertController (title: "Settings".localized, message: "To use twitter you first need to set your keys in settings".localized, preferredStyle: .Alert)
            
            var settingsAction = UIAlertAction(title: "Open Settings".localized, style: .Default) { (_) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            
            var cancelAction = UIAlertAction(title: "Cancel".localized, style: .Default, handler: nil)
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            
            self.presentViewController(alertController, animated: true, completion: {
                sender.setOn(false, animated: true)
            })
            
            
        } else if sender.on {
            Constants.twitterEnabled = true
        } else {
            Constants.twitterEnabled = false
        }
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        NetworkController.sendParameters(["param2" : Constants.twitterMessagePath, "param3" : textField.text], completionNotification: nil)
    }
}