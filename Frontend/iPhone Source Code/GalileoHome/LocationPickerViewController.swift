//
//  LocationPickerViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 10/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit
import MapKit

class LocationPickerViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var annotation = MKPointAnnotation()
    var locManager = CLLocationManager()
    
    override func viewDidLoad() {
        //Set up back and done button in navigation bar
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: Selector("cancelPressed"))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("donePressed"))
        
        //Tap gesture recognizer for picking location
        var tap = UITapGestureRecognizer(target: self, action: Selector("locationChosen:"))
        mapView.addGestureRecognizer(tap)
        
        //Detect drags
        var pan = UIPanGestureRecognizer(target: self, action: Selector("didDragMap"))
        mapView.addGestureRecognizer(pan)
        var swipe = UISwipeGestureRecognizer(target: self, action: Selector("didDragMap"))
        mapView.addGestureRecognizer(swipe)
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    func cancelPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func donePressed() {
        Constants.homeLocation = annotation.coordinate
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didDragMap() {
        mapView.setUserTrackingMode(MKUserTrackingMode.None, animated: true)
    }
    
    func locationChosen(tap: UITapGestureRecognizer) {
        //Remove old annotation
        mapView.removeAnnotation(annotation)
        
        //Add annotation at tapped position
        let point = tap.locationInView(mapView)
        let location = mapView.convertPoint(point, toCoordinateFromView: mapView)
        annotation.coordinate = location
        mapView.addAnnotation(annotation)
    }
    
    func mapViewDidFinishLoadingMap(mapView: MKMapView!) {
        //Add existing home location
        if Constants.homeLocation.latitude != 0 && Constants.homeLocation.longitude != 0 {
            annotation.coordinate = Constants.homeLocation
            mapView.addAnnotation(annotation)
        }
        
        //Ask for tracking permission
        let authorization = CLLocationManager.authorizationStatus()
        if authorization != .AuthorizedAlways {
            locManager.requestWhenInUseAuthorization()
        }
        
        //Follow user's position
        mapView.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true)
    }
}
