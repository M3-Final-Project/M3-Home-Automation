//
//  NetworkRequestController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 16/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import Foundation
import UIKit

///Requests values from server, that are not associated with the current preset
class NetworkRequestController: NSObject {
    
    ///Request iBeacon UUID
    ///
    /// :return: string value if available, nil otherwise
    class func requestBeaconID() -> String? {
        
        if let data =  NetworkController.syncRequest(Constants.beaconPath) {
            let id = NetworkController.parseServerDataToString(data)
            if id != "" {
                NSUserDefaults.standardUserDefaults().setObject(id, forKey: "beacon_ID")
                NSUserDefaults.standardUserDefaults().synchronize()
                return id
            }
        }
        
        //If user decided to not use iBeacon -> do not use it
        if let storedVal : Bool? = NSUserDefaults.standardUserDefaults().objectForKey("beacon_enabled") as? Bool {
            if storedVal == false {
                return nil
            }
        }
        
        if let storedBeaconID : String? = NSUserDefaults.standardUserDefaults().objectForKey("beacon_ID") as? String {
            return storedBeaconID
        }
        
        
        return nil
//        return "9F5E879C-5A83-4C8E-9DF0-CC6D4D5D3D19"
//        return "C2A94B61-726C-4954-3230-313478303031"
    }
    
    class func requestBeaconMajorID() -> UInt16 {
        return 23632
    }
    class func requestBeaconMinorID() -> UInt16 {
        return 24
    }
    
}
