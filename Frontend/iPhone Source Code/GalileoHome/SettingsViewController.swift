//
//  SettingsViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit

class SettingsViewController: UINavigationController, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView!
    var logoutButton: UIButton!
    var logoutActivity: UIActivityIndicatorView!
    
    var logoutView: UIView!
    

    ///Set up table view and logout button after loading from xib
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set up table view
        tableView = UITableView(frame: CGRectMake(0, 50, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height-180), style: .Grouped)
        tableView.dataSource = self
        tableView.bounces = false
        tableView.scrollEnabled = false
        tableView.delegate = self
        tableView.backgroundColor = UIColor.clearColor()
        self.view.addSubview(tableView)
        
        //Register reuse id for table view cells
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.backgroundColor = Constants.backgroundColor
        
        //Set up Logout button
        //Center logout view
        logoutView = UIView(frame: CGRectMake(54, 452, 164, 30))
        let tabbarStart = UIScreen.mainScreen().bounds.size.height - self.tabBarController!.tabBar.frame.height
        let tableEnd = tableView.frame.origin.y + tableView.frame.height
        logoutView.center.y = tableEnd + (tabbarStart - tableEnd)/2
        logoutView.center.x = UIScreen.mainScreen().bounds.size.width / 2
        logoutView.layer.cornerRadius = 15.0
        
        //Set background color, add button and activity indicator
        logoutView.backgroundColor = Constants.highlightColor1
        //logoutButton = UIButton(frame: CGRectMake(0, 0, 212, 29))
        logoutButton = UIButton(frame: CGRectMake(0, 0, logoutView.frame.size.width, logoutView.frame.size.height))
        logoutButton.setTitle("Logout".localized, forState: .Normal)
        logoutButton.titleLabel?.textColor = Constants.textColor
        logoutButton.titleLabel?.font = UIFont.boldSystemFontOfSize(17.0)
        logoutButton.addTarget(self, action: Selector("logoutPressed"), forControlEvents: .TouchUpInside)
        logoutActivity = UIActivityIndicatorView(frame: CGRectMake(96, 5, 20, 20))
        self.view.addSubview(logoutView)
        logoutView.addSubview(logoutButton)
        logoutView.addSubview(logoutActivity)
        
    }
    
    ///Get rid of unneccessary memory in case there is not much memory left
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    

    ///Gets called whenever user touches logout button
    @IBAction func logoutPressed() {
        Constants.shouldLogout = true
        NetworkController.passwordStr = ""
        self.tabBarController?.selectedIndex = 0
    }
    
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        cell.accessoryType = .DisclosureIndicator
        
        if indexPath.section == 0 {
            cell.textLabel?.text = "Help".localized
            //TODO: Hilfe im Storyboard hinzufügen - Link zu Doku
        } else if indexPath.section == 1 {
            switch indexPath.row {
                case 0: cell.textLabel?.text = "Imprint".localized
                case 1: cell.textLabel?.text = "Info".localized
                case 2: cell.textLabel?.text = "Licenses".localized
                default: return cell
            }
        } else if indexPath.section == 2 {
            cell = UITableViewCell(style: .Value2, reuseIdentifier: "versionCell")
            switch indexPath.row {
                case 0: cell.textLabel?.text = "App Version".localized
                        cell.textLabel?.textColor = Constants.highlightColor1
                        cell.detailTextLabel?.text = Constants.appVersion
                case 1: cell.textLabel?.text = "Hardware".localized
                        cell.textLabel?.textColor = Constants.highlightColor1
                        cell.detailTextLabel?.text = Constants.hardwareVersion
                default: return cell
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if Constants.runningOn4S {
            return nil
        }
        if section == 2 {return "Version".localized}
        return nil
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        var header = view as! UITableViewHeaderFooterView
        header.textLabel.textColor = Constants.textColor
        header.textLabel.textColor = UIColor.darkGrayColor()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 1
            
        case 1:
            return 3
            
        case 2:
            if Constants.runningOn4S {return 0}
            return 2
            
        default:
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            let data = NetworkController.syncRequest("instructables")
            let url  = NetworkController.parseServerDataToString(data)
            let helpURL = NSURL(string: url)
            if let url = helpURL {
                UIApplication.sharedApplication().openURL(url)
            }
            
        } else if indexPath.section == 1 {
            switch indexPath.row {
                case 0: self.performSegueWithIdentifier("pushImprint", sender: self)
                case 1: self.performSegueWithIdentifier("pushInfo", sender: self)
                case 2: self.performSegueWithIdentifier("pushLicense", sender: self)
                default: return
            }
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
