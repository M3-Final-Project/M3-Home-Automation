//
//  SmartDoorViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 10/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit
import MapKit

extension UIView {
    class func hideView(view hideView: UIView, time: NSTimeInterval) {
        let alpha = hideView.alpha
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(time)
        
        hideView.alpha = 0.0
        
        UIView.commitAnimations()
        
        hideView.hidden = true
        hideView.alpha = alpha
    }
    
    class func showView(view showView : UIView, time: NSTimeInterval) {
        let alpha = showView.alpha
        showView.alpha = 0.0
        showView.hidden = false
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(time)
        
        showView.alpha = alpha
        
        UIView.commitAnimations()
    }
}

class SmartDoorViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var autoSwitch: UISwitch!
    var doorButton: UIButton!
    @IBOutlet weak var doorLabel: UILabel!
    @IBOutlet weak var homeLocationButton: UIButton!
    @IBOutlet weak var beaconButton: UIButton!
    @IBOutlet weak var doorButton2: UIButton!
    
    @IBOutlet weak var automaticallyOpenView: UIView!
    var closedImage = UIImage()
    var openImage = UIImage()
    var open : Bool = true //Door button thinks door open?
    var openAutomatically : Bool = false
    var locManager = CLLocationManager()
    var beaconAvailable : Bool = false
    var beaconRegion = CLBeaconRegion()
    
    var updateTimer = NSTimer()
    var sendRequestPending = false
    let maximumLockImageOffset = 23.0
    var currentOffset : CGFloat = 0.0 //Offset of finger in pixels
    var imageOffset : CGFloat = 0.0 //Offset of image in pixels
    var lockImages : [UIImage] = [] //Contains all images for lock animation
    var draggingLock = false //Indicates lock has been dragged
    
    func createLockImage(offset : CGFloat) -> UIImage {
        let shackle = UIImage(named: "Shackle1024.png")
        let lock = UIImage(named: "Lock1024.png")
        
        let newSize = CGSizeMake(600, 600);
        UIGraphicsBeginImageContext( newSize );
        
        // Use existing opacity as is
        shackle?.drawInRect(CGRectMake(0, offset*3, newSize.width, newSize.height))
        lock?.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height), blendMode: kCGBlendModeNormal, alpha: 1.0)
        
        var newImage = UIGraphicsGetImageFromCurrentImageContext().imageWithRenderingMode(.AlwaysTemplate)
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    @IBAction func beaconSettingsPressed(sender: AnyObject) {
        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
        if let url = settingsUrl {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    
    override func viewDidLoad() {
        //Deactivate automatically open on 4S
        if Constants.runningOn4S {
            automaticallyOpenView.hidden = true
            automaticallyOpenView.userInteractionEnabled = false
        }
        
        //home location button set color
        self.view.backgroundColor = Constants.backgroundColor
        self.homeLocationButton.backgroundColor = Constants.highlightColor1
        self.homeLocationButton.hidden = true
        homeLocationButton.layer.cornerRadius = 15.0
        
        //Set up door button and load images
        openImage = UIImage(named: "LockOpen")!.imageWithRenderingMode(.AlwaysTemplate)
        closedImage = UIImage(named: "LockClosed")!.imageWithRenderingMode(.AlwaysTemplate)
        doorButton = UIButton(frame: CGRectMake(UIScreen.mainScreen().bounds.width/2 - 100, 80, 200, 200))
        doorButton.addTarget(self, action: Selector("doorButtonPressed"), forControlEvents: .TouchUpInside)
        doorButton.setImage(openImage, forState: .Normal)
        doorButton.tintColor = UIColor.darkGrayColor()
        doorButton.accessibilityLabel = "Open door".localized
        doorButton.accessibilityHint = "Press to open or close door".localized
        doorButton.addTarget(self, action: "wasDragged:withEvent:", forControlEvents: .TouchDragInside)
        doorButton2.layer.cornerRadius = 20.0
        doorButton2.titleLabel?.textColor = Constants.textColor
        doorButton2.backgroundColor = Constants.highlightColor1
        self.view.addSubview(doorButton)
        
        beaconButton.titleLabel!.numberOfLines = 2
        beaconButton.titleLabel!.lineBreakMode = .ByWordWrapping
        beaconButton.titleLabel!.textAlignment = .Center
        var attrStr = NSMutableAttributedString(string: "Use iBeacon\nNo setup required".localized)
        let range = attrStr.string.rangeOfString("\n", options: .allZeros, range: nil, locale: nil)
        var index: Int = distance(attrStr.string.startIndex, range!.startIndex)
        attrStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSMakeRange(0, index))
        attrStr.addAttribute(NSForegroundColorAttributeName, value: Constants.highlightColor1, range: NSMakeRange(index+1, attrStr.length-index-1))
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .Center
        attrStr.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attrStr.length))
        beaconButton.setAttributedTitle(attrStr, forState: .Normal)
        
        
        
        //Set up location manager
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        
        //Set switch if necessary
        if Constants.enableGeofence && ((Constants.homeLocation.longitude != 0 || Constants.homeLocation.latitude != 0 ) || receiveBeacon()) {
            autoSwitch.setOn(true, animated: false)
        }
        
        if !receiveBeacon() {
            homeLocationButton.superview?.bringSubviewToFront(homeLocationButton)
            homeLocationButton.hidden = false
        }
        
        updateValues()
    }
    
    override func viewDidAppear(animated: Bool) {
        if Constants.shouldLogout {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
        if lockImages == [] {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), {
                for i in 0...23 {
                    self.lockImages.append(self.createLockImage(CGFloat(i)))
                }
            })
        }
        
        
        if Constants.enableGeofence {
            //Update geofence
            stopGeofence()
            NSLog("\(locManager.monitoredRegions)")
            startGeofence()
            NSLog("\(locManager.monitoredRegions)")
        }
        
        //Show button for choosing home location if there is no beacon
        if !receiveBeacon() && homeLocationButton.hidden == true {
            homeLocationButton.superview?.bringSubviewToFront(homeLocationButton)
            UIView.showView(view: homeLocationButton, time: 0.5)
        } else if receiveBeacon() && homeLocationButton.hidden == false {
            UIView.hideView(view: homeLocationButton, time: 0.5)
        }
        
        startUpdating()
    }
    
    override func viewDidDisappear(animated: Bool) {
        stopUpdating()
    }
    
    func receiveBeacon() -> Bool {
        //Set up beacon region
        var uuidString = NetworkRequestController.requestBeaconID()
        var beaconMajorID:CLBeaconMajorValue = NetworkRequestController.requestBeaconMajorID()
        var beaconMinorID:CLBeaconMinorValue = NetworkRequestController.requestBeaconMinorID()
        
        let id = "GH TestBeacon"
        if uuidString != nil {
            let uuid = NSUUID(UUIDString: uuidString!)
            if(uuid != nil) {
                beaconRegion = CLBeaconRegion(proximityUUID: uuid, identifier: id)
                //beaconRegion = CLBeaconRegion(proximityUUID: uuid, major: beaconMajorID, minor: beaconMinorID, identifier:
                return true
            }
        }
        return false
    }
    
    
    
    @IBAction func doorButtonPressed(sender: AnyObject) {
        doorButtonPressed()
    }
    func doorButtonPressed() {
        //Respond to dragging
        if draggingLock {
            draggingLock = false
            if currentOffset <= 17 {
                openDoor()
            } else if currentOffset > 17 {
                closeDoor()
            }
            updateDoorButton()
            return
        }
        
        //Respond to tapping
        if open {
            closeDoor()
        } else {
            openDoor()
        }
        updateDoorButton()
    }
    
    func updateDoorButton() {
        if !open {
            doorButton.setImage(closedImage, forState: .Normal)
            currentOffset = 23.0
            doorLabel?.text = "Door closed".localized
            doorButton2.setTitle("Open door".localized, forState: .Normal)
        } else {
            doorButton.setImage(openImage, forState: .Normal)
            doorLabel?.text = "Door open".localized
            currentOffset = 0.0
            doorButton2.setTitle("Close door".localized, forState: .Normal)
        }
    }
    
    func startGeofence() {
        //Need to update location to get didRangeBeacon calls in background
        locManager.startUpdatingLocation()
        if receiveBeacon() {
            locManager.startMonitoringForRegion(beaconRegion)
            locManager.startRangingBeaconsInRegion(beaconRegion)
        } else {
            let homeRegion = CLCircularRegion(circularRegionWithCenter: Constants.homeLocation, radius: 100.0, identifier: "HomeGeofence")
            locManager.startMonitoringForRegion(homeRegion)
        }
        Constants.enableGeofence = true
        NSLog("Start geofence")
    }
    
    func stopGeofence() {
        //Stop updating location
        locManager.stopUpdatingLocation()
        
        //Stop beacon ranging or geofencing
        if receiveBeacon() {
            locManager.stopMonitoringForRegion(beaconRegion)
            locManager.stopRangingBeaconsInRegion(beaconRegion)
        } else {
            let homeRegion = CLCircularRegion(circularRegionWithCenter: Constants.homeLocation, radius: 100.0, identifier: "HomeGeofence")
            locManager.stopMonitoringForRegion(homeRegion)
        }
        
        Constants.enableGeofence = false
        NSLog("Stop geofence")
    }
    
    func wasDragged(button : UIButton, withEvent event: UIEvent) {
        //Prevent updates while dragging
        sendRequestPending = true
        
        //Inform others
        draggingLock = true

        let touch : UITouch = event.allTouches()?.first as! UITouch
        let location = touch.locationInView(self.view)
        let previousLoc = touch.previousLocationInView(self.view)
        let delta : CGFloat = location.y - previousLoc.y
        
        // 4 px drag = 1 px lock
        currentOffset += delta / 4.0
        
        //Constraints for lock
        if currentOffset > 23.0 {currentOffset = 23.0}
        if currentOffset < 0.0 {currentOffset = 0.0}

        //Only animate if enough images have been loaded
        if lockImages.count > Int(currentOffset) {
            doorButton.setImage(lockImages[Int(currentOffset)], forState: .Normal)
        }
    }
    
    @IBAction func switchChanged(sender: AnyObject) {
        openAutomatically = autoSwitch.on
        
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            //Request access to user's location
            locManager.requestAlwaysAuthorization()
            while CLLocationManager.authorizationStatus() == .NotDetermined {}
        }
        
        if autoSwitch.on {
            //Check wether we are allowed to track user
            let authorization = CLLocationManager.authorizationStatus()
            if authorization != .AuthorizedAlways {
                //Access granted?
                if authorization != .AuthorizedAlways {
                    autoSwitch.setOn(false, animated: true)
                    let alertView = UIAlertController(title: "No Authorization".localized, message: "Please allow location tracking in iOS settings to use this feature".localized, preferredStyle: .Alert)
                    var settingsAction = UIAlertAction(title: "Open Settings".localized, style: .Default) { (_) -> Void in
                        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            UIApplication.sharedApplication().openURL(url)
                        }
                    }
                    var cancelAction = UIAlertAction(title: "Cancel".localized, style: .Default, handler: nil)
                    alertView.addAction(settingsAction)
                    alertView.addAction(cancelAction)
                    
                    
                    self.presentViewController(alertView, animated: true, completion: nil)
                    return
                }
                
                //Check if home location is unknown
                if Constants.homeLocation.longitude == 0 && Constants.homeLocation.latitude == 0 {
                    var alert = UIAlertController(title: "Unkown home location".localized, message: "You need to set your home location, before activation the smart door".localized, preferredStyle: .Alert)
                    var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    return
                }
            } else {
                //Start monitoring location
                startGeofence()
            }
        } else {
            stopGeofence()
        }
    }
    
    ///Informs user with a notification about recently opened door
    class func showDoorOpenNotification() {
        //Show notification
        var doorNotification = UILocalNotification()
        doorNotification.alertBody = "The door of your smart home has been opened.".localized
        doorNotification.alertAction = "Show me".localized
        doorNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().presentLocalNotificationNow(doorNotification)
    }
    
    ///Informs user with a notification about recently closed door
    class func showDoorClosedNotification() {
        //Show notification
        var doorNotification = UILocalNotification()
        doorNotification.alertBody = "The door of your smart home has been locked.".localized
        doorNotification.alertAction = "Show me".localized
        doorNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().presentLocalNotificationNow(doorNotification)
    }
    
    
    func openDoor() {
        //Wait for completion - prevent UI changes by server
        sendRequestPending = true
        
        //Open door on server
        Preset.currentPreset.sendOpenDoor()

        //Inform user
        SmartDoorViewController.showDoorOpenNotification()
        
        //Inform door button
        open = true
    }
    
    func closeDoor() {
        //Wait for completion - prevent UI changes by server
        sendRequestPending = true
        
        //Close door on server
        Preset.currentPreset.sendCloseDoor()
        
        //Inform user
        SmartDoorViewController.showDoorClosedNotification()
        
        //Inform door button
        open = false
    }
    
    // MARK: Location Manager Delegate
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        let homeRegion = CLCircularRegion(circularRegionWithCenter: Constants.homeLocation, radius: 10.0, identifier: "HomeGeofence")
        
        //If the region is a geographical region -> Open door
        //Otherwise the didRangeBeacons method takes care of it
        if region == homeRegion {
            openDoor()
            updateDoorButton()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        closeDoor()
        updateDoorButton()
        Constants.inBeaconRange = false
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        //Filter out unknown - and therefore undetected - beacons
        let detectedBeacons = beacons.filter{ $0.proximity != .Unknown }
        
        NSLog("\(detectedBeacons)")
        
        var found = false
        
        //Beacon found?
        if detectedBeacons.count > 0 {
            //Only respond to relatively close beacons
            if detectedBeacons[0].proximity == .Immediate || detectedBeacons[0].proximity == .Near {
                //Only open door if it isn't already opened
                if Constants.inBeaconRange != true {
                    openDoor()
                    updateDoorButton()
                    Constants.inBeaconRange = true
                    found = true
                }
            }
            if detectedBeacons[0].proximity == .Far && Constants.inBeaconRange == true {
                closeDoor()
                updateDoorButton()
                Constants.inBeaconRange = false
            }
        }
    }
    
    // MARK: - Network methods
    func updateValues() {
        Preset.currentPreset.receiveDoorState()
        
        //Update UI
        if !sendRequestPending {
            open = Preset.currentPreset.doorOpened
            updateDoorButton()
        }
        
        //Assume it takes no longer than one update interval to send
        sendRequestPending = false
    }
    
    private func startUpdating() {
        updateTimer.invalidate()
        updateTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateValues"), userInfo: nil, repeats: true)
    }
    
    private func stopUpdating() {
        updateTimer.invalidate()
    }
}
