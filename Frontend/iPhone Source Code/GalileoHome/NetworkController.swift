//
//  NetworkController.swift
//  GalileoDownloader
//
//  Created by Jörg Christian Kirchhof on 02/05/15.
//  Copyright (c) 2015 Jörg Christian Kirchhof. All rights reserved.
//

import Foundation

/**
    This class is responsible for receiving information from and for passing information to the Galileo board.
*/
class NetworkController: NSObject {
    static var ipAddrStr : String {
        get {
            let storedVal : NSString? = NSUserDefaults.standardUserDefaults().objectForKey("ipAddrStr") as? NSString
            var string : String? = storedVal as String?
            if string == nil {string = ""}
            return string!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "ipAddrStr")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    static var ipURLStr : String {
        get {
            return "http://" + ipAddrStr + portNr + "/m3server/"
        }
    }
    static var portNr = ":80"
    static var passwordStr : String {
        get {
            let storedVal : NSString? = NSUserDefaults.standardUserDefaults().objectForKey("passwordStr") as? NSString
            var string : String? = storedVal as String?
            if string == nil {string = ""}
            return string!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "passwordStr")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    internal static var receivedData : NSMutableData? = nil
    

    static func parseServerDataToString(data: NSData!) -> String {
        let dataStrNS = NSString(data: data, encoding: NSASCIIStringEncoding)
        var dataStr = dataStrNS as! String
        dataStr = dataStr.stringByReplacingOccurrencesOfString("\n", withString: "", options: .allZeros, range: nil)
        return dataStr
    }
    
    static func parseServerDataToString2(data: NSData!) -> String {
        let dataStrNS = NSString(data: data, encoding: NSASCIIStringEncoding)
        var dataStr = dataStrNS as! String
        return dataStr
    }
    
    ///Sets up observers to wait for user input about Galileo (e.g. IP, password)
    class func listenToInfos() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("didReceiveNetworkInfo:"), name: "IPReceived", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("didReceiveNetworkInfo:"), name: "PasswordReceived", object: nil)
    }
    
    /**
        Updates info about the network (e.g. IP, password) whenever user enters info
        
        :param: notification Object containing the new info
    */
    class func didReceiveNetworkInfo(notification: NSNotification) {
        let receivedDict = notification.userInfo
        if notification.name == "IPReceived" {
            ipAddrStr = notification.object as! String
        } else if notification.name == "PasswordReceived" {
            passwordStr = notification.object as! String
        }
    }
    
    /**
        Sends a HTTP POST to Galileo. 
        
        :param: action Action to be performed by Galileo, e.g. 'modify'
        :param: params Dictionary of parameters, added after action
        :param: completionNotification String to post to default notification center when done sending
    */
    class func sendParameters(params : Dictionary<String, String>, completionNotification:String?=nil) {
        
        //Create string from parameters
        var paramStr = String()
        for (k, v) in params {
            paramStr += "&" + k + "=" + v
        }
        
        //Length of parameters
        let length = "\(count(paramStr))"
        
        //Signal that we push info to the server
        var message = "param1=pub"
        message +=  paramStr
        
        //Send messageS
        sendString(message, length: length, completionNotification: completionNotification)
    }
    
    class func sendPassword(params : Dictionary<String, String>, completionNotification:String?=nil) {
        
        //Create string from parameters
        var paramStr = String()
        for (k, v) in params {
            paramStr += "&" + k + "=" + v
        }
        
        //Length of parameters
        let length = "\(count(paramStr))"
        
        //Signal that we push info to the server
        var message = "param1=password"
        message +=  paramStr
        
        //Send message
        NSLog("\(message)")
        sendString(message, length: length, completionNotification: completionNotification)
    }

    
    
    /// Send a HTTP Post to Galileo
    class func sendRule(params : Dictionary<String, String>, completionNotification:String?=nil) {
        
        //Create string from parameters
        var paramStr = String()
        for (k, v) in params {
            paramStr += "&" + k + "=" + v
        }
        
        //Length of parameters
        let length = "\(count(paramStr))"
        
        //Signal that we push info to the server
        var message = "param1=rule"
        message +=  paramStr
        
        //Send message
        //NSLog("\(message)")
        sendString(message, length: length, completionNotification: completionNotification)
    }
    
    /**
        Sends a HTTP POST to Galileo containing a given message plus the automatically added password.
        
        For internal use only. Use sendParameters instead.
    
        :param: message String to be send to Galileo. Added after password.
        :param: length Length of parameters for HTTP Content-Length field
        :param: completionNotification String sent to default notification center when request is done (no matter if successful or not)
    */
    internal class func sendString(message: String, length : String, completionNotification:String?=nil) {
        
        //Create HTTP POST
        //let body = "pwd="+passwordStr + "&" + message
        let body = message
        
        NSLog("POST \(message)")
        
        let urlStr =  ipURLStr.stringByReplacingOccurrencesOfString("/m3server/", withString: "80", options: .allZeros, range: nil)
        let url = NSURL(string: urlStr)
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.addValue(length, forHTTPHeaderField: "Content-Length")
        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let queue : NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            if let note = completionNotification {
                NSNotificationCenter.defaultCenter().postNotificationName(note, object: nil)
            }
        })
    }
    
    /**
        Requests an info from Galileo, posts "receivedURL/"+infoToGet notification to defaultCenter when done
    
        :param: infoToGet Name of the info to get from Galileo
    */
    class func receiveURL(infoToGet : String) {
        
        //Create URL / Request
        let urlStr =  ipURLStr + infoToGet + ".html"
        let url = NSURL(string: urlStr)
        
        let request = NSURLRequest(URL: url!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 3.0)
        let queue : NSOperationQueue = NSOperationQueue()
        receivedData = NSMutableData()
        
        //Establish connection
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            //Log possible error
            if error != nil {
                Constants.connection = false
                NSLog("Connection request failed")
                NSNotificationCenter.defaultCenter().postNotificationName("connectionLost", object: nil)
                //Forward failed connection to caller
                NSNotificationCenter.defaultCenter().postNotificationName("receivedURL/"+infoToGet, object: nil)
            } else {
                Constants.connection = true
                //Forward requested info to caller
                NSNotificationCenter.defaultCenter().postNotificationName("receivedURL/"+infoToGet, object: data)
                NSNotificationCenter.defaultCenter().postNotificationName("connectionFound", object: nil)
            }
        })
    }
    
    ///Gets some info synchronous
    internal class func syncRequest(infoToGet : String, timeout:NSTimeInterval=0.2) -> NSData? {
        //Create URL / Request
        let urlStr =  ipURLStr + infoToGet + ".html"
        let url = NSURL(string: urlStr)
        
        //Request with very short timeout to prevent blocking threads
        let request = NSURLRequest(URL: url!, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: timeout)
        var response : NSURLResponse? = nil
        var error : NSError? = nil
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        
        if error != nil {
            //Print error
            NSLog(error!.localizedDescription)
            return nil
        }
        
        return data   
    }
    
    internal class func testConnection(infoToGet : String, timeout:NSTimeInterval=0.2) -> NSError? {
        //Create URL / Request
        let urlStr =  ipURLStr + infoToGet + ".html"
        let url = NSURL(string: urlStr)
        
        //Request with very short timeout to prevent blocking threads
        let request = NSURLRequest(URL: url!, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: timeout)
        var response : NSURLResponse? = nil
        var error : NSError? = nil
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: &error)
        
        if error != nil {
            //Print error
            NSLog(error!.localizedDescription)
        }
        
        return error
    }
}