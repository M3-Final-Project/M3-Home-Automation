//
//  HomeViewController.swift
//  
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//
//

import UIKit

class HomeViewController: UITableViewController, UITableViewDelegate {
    
    var demo = false
    var logoutTimer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.homeController = self
        
        logoutTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "checkLogout", userInfo: nil, repeats: true)
        
        Preset.currentPreset.requestActiveDevices()
        
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: tableView, selector: Selector("reloadData"), userInfo: nil, repeats: false)
        
        //Move table view slightly upwards so there is a smaller gap to the navigation bar
        self.tableView.contentInset = UIEdgeInsetsMake(-26, 0, 0, 0)
        
        //Set default background color
        self.tableView.backgroundColor = Constants.backgroundColor
        
        Preset.currentPreset.receivePresets()
        
        //Uncomment to make gesture available for ignoring available devices contraint
        /*
        let doubleTap = UITapGestureRecognizer(target: self, action: Selector("ignoreAvailability"))
        doubleTap.numberOfTapsRequired = 2
        tableView.addGestureRecognizer(doubleTap)
        */
    }
    
    func ignoreAvailability() {
        demo = !demo
        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {


        if NetworkController.ipAddrStr == "" || Constants.shouldLogout {
            if (self.isViewLoaded() && self.view.window != nil) {
                self.performSegueWithIdentifier("LoginSegue", sender: self)
            }
        }
        
        //Uncomment this line when something on the server went wrong
        //NetworkController.sendParameters(["param2" : "sensor/brightness", "param3" : "164"], completionNotification: "")
        
        // Initially receive preset values
        Preset.currentPreset.updatePreset()
        
        Preset.currentPreset.requestActiveDevices()
        
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: tableView, selector: Selector("reloadData"), userInfo: nil, repeats: false)
    }
    
    func checkLogout() {
        if NetworkController.ipAddrStr == "" || Constants.shouldLogout {
            if (self.isViewLoaded() && self.view.window != nil) {
                self.performSegueWithIdentifier("LoginSegue", sender: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UITableViewDelegate
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
//        NSLog("Device No. \(indexPath.section) \(Preset.activeDeviceList[indexPath.section])")
        
        if !Preset.activeDeviceList[indexPath.section] && !demo {
            let textX = cell.textLabel!.frame.origin.x
            let textY = cell.textLabel!.frame.origin.y
            let textHeight = cell.textLabel!.frame.size.height
            let textWidth = cell.textLabel!.frame.size.width
            cell.textLabel?.frame = CGRectMake(textX, textY, textWidth, textHeight-20.0)
            
            cell.backgroundColor = UIColor.lightGrayColor()
            let textLabelBottom = cell.textLabel!.frame.origin.y + cell.textLabel!.frame.size.height
            var label = UILabel(frame: CGRectMake(textX, textLabelBottom, textWidth, 20.0))
            label.tag = 123
            label.text = "Device unavailable".localized
            label.textColor = UIColor.redColor()
            label.font = UIFont.systemFontOfSize(10)
            //cell.addSubview(label)
            
            cell.separatorInset = UIEdgeInsets(top: -20.0, left: 0, bottom: 0, right: 0)
        } else {
            cell.backgroundColor = UIColor.whiteColor()
            
            //Remove "Device unavailable" label
            self.view.viewWithTag(123)?.removeFromSuperview()
            //cell.subviews.last?.removeFromSuperview()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if !Preset.activeDeviceList[indexPath.section] && !demo {
            return
        }
        
        switch indexPath.section {
            case 0: self.performSegueWithIdentifier("pushLight", sender: self)
            case 1: self.performSegueWithIdentifier("pushDoor", sender: self)
            case 2: self.performSegueWithIdentifier("pushCoffee", sender: self)
            case 3: self.performSegueWithIdentifier("pushVentilator", sender: self)
            case 4: self.performSegueWithIdentifier("pushSensors", sender: self)
            default: return
        }
    }
}
