//
//  LoginViewController.swift
//  GalileoHome
//
//  Created by Jörg Christian Kirchhof on 07/06/15.
//  Copyright (c) 2015 Media Computing Group of RWTH Aachen University. All rights reserved.
//

import UIKit


//
extension String  {
    var md5: String! {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        var hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.dealloc(digestLen)
        
        return String(format: hash as String)
    }
}

class LoginViewController: UIViewController, UITextFieldDelegate {
    var ipAddrStr : String {
        get {
            let storedVal : NSString? = NSUserDefaults.standardUserDefaults().objectForKey("ipAddrStr") as? NSString
            var string : String? = storedVal as String?
            if string == nil {string = ""}
            return string!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "ipAddrStr")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var passwordStr : String {
        get {
            let storedVal : NSString? = NSUserDefaults.standardUserDefaults().objectForKey("passwordStr") as? NSString
            var string : String? = storedVal as String?
            if string == nil {string = ""}
            return string!
        }
        set (newVal) {
            NSUserDefaults.standardUserDefaults().setObject(newVal, forKey: "passwordStr")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    @IBOutlet weak var ipTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nextActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nextView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Show status bar
        UIApplication.sharedApplication().statusBarHidden = false
        
        //Show button
        nextButton.hidden = false
        
        //Hide Activity Indicator
        nextActivityIndicator.hidden = true
        
        //Round edges for button
        nextView.layer.cornerRadius = 3.0
        
        //Setup text fields
        ipTextField.adjustsFontSizeToFitWidth = true
        ipTextField.autocorrectionType = .No
        ipTextField.tag = 1
        ipTextField.keyboardType = .NumbersAndPunctuation
        ipTextField.returnKeyType = .Next
        ipTextField.clearButtonMode = .WhileEditing
        
        passwordTextField.adjustsFontSizeToFitWidth = true
        passwordTextField.autocorrectionType = .No
        passwordTextField.tag = 2
        passwordTextField.keyboardType = .Default
        passwordTextField.returnKeyType = .Done
        passwordTextField.clearButtonMode = .WhileEditing
        
        //Gesture recognizer to dismiss keyboard when tapping outside
        let tapRec = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
        tapRec.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRec)
    }
    
    override func viewDidAppear(animated: Bool) {
        //Listener for keyboard - triggers sliding views
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide"), name: UIKeyboardWillHideNotification, object: nil)
        
        if ipAddrStr != "" {
            ipTextField.text = ipAddrStr
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(false)
    }

    func keyboardWillShow() {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        
        self.view.frame.origin.y -= Constants.infoViewKeyboardOffset
        self.view.frame.size.height += Constants.infoViewKeyboardOffset
        
        UIView.commitAnimations()
    }
    
    func keyboardWillHide() {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        
        self.view.frame.origin.y += Constants.infoViewKeyboardOffset
        self.view.frame.size.height -= Constants.infoViewKeyboardOffset
        
        UIView.commitAnimations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkIP(ip: String) -> Bool {
        // Regular expression for a valid IP adress
        let ipRegex = NSRegularExpression(pattern: "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$", options: nil, error: nil)!
        
        // Check the regular expression
        if let match = ipRegex.firstMatchInString(ip, options: nil,
            range: NSRange(location: 0, length: count(ip.utf16))) {
                //Success return true
                return true
        }
        return false
    }
    
    func establishConnection() -> Bool {
        var alert = UIAlertController()
        
        // Check network connection
        let networkDevices = NetworkController.testConnection(Constants.activeDevices, timeout: 5.0)
        let network = (networkDevices == nil)
        
        if (network){
            // TODO check password correctly
            NetworkController.sendPassword(["param2" : passwordTextField.text.md5])
            
            if let response = NetworkController.syncRequest(Constants.passwordResultPath) {
                if NetworkController.parseServerDataToString(response).toInt() == 1 {
                    return true
                } else {
                    // Wrong password
                    alert = UIAlertController(title: "Oops...".localized, message: "The password you entered is incorrect. Please try again.".localized, preferredStyle: .Alert)
                    var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        } else {
            // Could not connect... damn
            alert = UIAlertController(title: "Oops...".localized, message: "Could not connect to server. Please check your input or try again later".localized, preferredStyle: .Alert)
            var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        //Reset Next button
        nextActivityIndicator.stopAnimating()
        nextActivityIndicator.hidden = true
        nextButton.hidden = false
    
        return false
    }
    
    @IBAction func nextPressed(sender: AnyObject) {
        //Hide text and show activity indicator instead
        nextButton.hidden = true
        nextActivityIndicator.hidden = false
        nextActivityIndicator.startAnimating()
        let ipCheck = checkIP(ipTextField.text)
        if (ipCheck){
            //Try to connect
            if establishConnection() {
                //Success = dismiss login page
                nextActivityIndicator.stopAnimating()
                NetworkController.ipAddrStr = ipTextField.text
                Constants.shouldLogout = false
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        } else {
            nextActivityIndicator.stopAnimating()
            nextActivityIndicator.hidden = true
            nextButton.hidden = false
            var alert = UIAlertController(title: "Oops...".localized, message: "Please enter a valid IP address.".localized, preferredStyle: .Alert)
            var cancelAction = UIAlertAction(title: "OK".localized, style: .Default, handler: nil)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: UITextField Delegate
    func textFieldDidEndEditing(textField: UITextField) {
        //In case the IP was changed -> check it's validity
        if textField.tag == ipTextField.tag {
            let ip = textField.text
            if checkIP(ip) {
                ipAddrStr = ip
            }
        } else {
            passwordStr = textField.text
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    
}
