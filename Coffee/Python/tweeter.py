import sys
import twitter

if len(sys.argv) < 4:
  print "USAGE: consumer_key consumer_secret access_token_key access_token_secret message"
  exit(1)

consumer_k = sys.argv[1]
consumer_s = sys.argv[2]
access_k = sys.argv[3]
access_s = sys.argv[4]
message = sys.argv[5]

#Update total Coffee count
try:
    with open("coffeecount.txt") as f:
        count = int(f.read())
except IOError:
    count = 11124; #The start value for the first tweet.

count += 1;

with open('coffeecount.txt', 'w+') as f:
    f.write(str(count));


#Connect to twitter api. Edit with own keys!
api = twitter.Api(consumer_key=consumer_k, consumer_secret=consumer_s,
                      access_token_key=access_k, access_token_secret=access_s,
                      );

status = api.PostUpdate(message + " No. " + str(count));
print "%s just posted: %s" % (status.user.name, status.text)

