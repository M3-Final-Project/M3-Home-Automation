#!usr/bin/python2.6

import sys
import ip_adress
import struct
from phue import Bridge
from rgb_cie import Converter

input = sys.argv[2]
ipAddr = sys.argv[1]

converter = Converter()
x,y = converter.hexToCIE1931(input)

[r,g,b] = struct.unpack('BBB',input.decode('hex'))
bri = max(r,g,b)

b = ip_adress.createBridge(ipAddr)

lights = b.get_light_objects()

for light in lights:
    light.on = True
    light.transitiontime = 2 
    light.brightness = bri
    light.xy = [x,y]
