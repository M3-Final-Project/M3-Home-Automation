#!/usr/bin/python
import ip_adress
import random
import sys

ipAddr = sys.argv[1]

b = ip_adress.createBridge(ipAddr)

lights = b.get_light_objects()

for light in lights:
	light.on = False
