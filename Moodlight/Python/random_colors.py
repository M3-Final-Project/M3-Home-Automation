#!/usr/bin/python
import ip_adress
import random
import time
import sys

ipAddr = sys.argv[1]
b = ip_adress.createBridge(ipAddr)

lights = b.get_light_objects()

while True:
	time.sleep(5)
	for light in lights:
		light.saturation = 254
		light.brightness = 255
		light.transitiontime = 30
		light.xy = [random.random(),random.random()]
