#!/usr/bin/python
import ip_adress
import random
import sys

ipAddr = sys.argv[1]

b = ip_adress.createBridge(ipAddr)

lights = b.get_light_objects()

for light in lights:
	light.transitiontime = 75
	light.on = True
	light.hue = 53000
	light.saturation = 255
	light.brightness = 255
