extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>

  // Vom Galileo nicht unterstützt
  //#include <sys/eventfd.h>
}

#include <string>
#include <iostream>
#include <functional>
#include <fstream>
#include <csignal>
// Von der IDE nicht unterstützt
//#include <thread>

#define HOST "localhost"
#define PORT 1884
#define READ_BUFFER_SIZE 1024

#define API_PATH "/usr/bin/Client-API"
#define API_PROC_NAME "Client-API"
#define BROKER_HOST "192.168.1.79"
#define AUTO_START_API
#define KILL_API
#define TEST_NETWORK

using namespace std;

int getProcIdByName(std::string procName)
{
  int pid = -1;

  // Open directory "/porc"
  DIR *dp = opendir("/proc");

  if (dp != 0)
  {
    dirent *dir;

    while (pid < 0 && (dir = readdir(dp)))
    {
      // Skip non numeric directories
      int id = atoi(dir->d_name);
      if (id > 0)
      {
        std::string cmdPath = std::string("/proc/") + dir->d_name + "/cmdline";
        std::ifstream cmdFile(cmdPath.c_str());
        std::string cmdLine;
        getline(cmdFile, cmdLine);

        if (!cmdLine.empty())
        {
          // Keep first cmdline item which contains the program path
          size_t pos = cmdLine.find('\0');

          if (pos != std::string::npos) cmdLine = cmdLine.substr(0, pos);

          if (procName == cmdLine) pid = id;
        }
      }
    }
  }

  closedir(dp);

  return pid;
}

class MqttClient
{
  private:
    int sock = 0;
    int eventSock = 0;
    sockaddr_in serv_addr;
    hostent * server;
    bool listening = true;

    pthread_t listenThread;

    void SendData(std::string data);
    static void * Listen(void * caller);
  public:
    MqttClient();
    ~MqttClient();

    void Publish(std::string topic, std::string message);
    void Subscribe(std::string topic);

    virtual void MessageReceivedCB(std::string topic, std::string message);
};

MqttClient::MqttClient()
{
#ifdef AUTO_START_API
  cout << "Auto start:" << endl;

#ifdef TEST_NETWORK
  {
    // Send a ping to our Broker
    std::string ping("ping -c 1 -W 2 ");
    ping += BROKER_HOST;
    ping += " >/dev/null";

    while (system(ping.c_str()) != 0) {
      cout << "\t Could not reach " << BROKER_HOST << endl;
      cout << "\t Did you plug the Ethernet cable into the device?" << endl;
      sleep(3);
    }
  }
#endif

#ifdef KILL_API
  // Kill API process
  {
    int const apiPid = getProcIdByName(API_PROC_NAME);

    if (apiPid > 0)
    {
      cout << "\tKilling process: " << apiPid << endl;
      kill(apiPid, SIGTERM);
      cout << "\tAPI process killed" << endl;
    }
    else cout << "\tAPI process not found (kill)" << endl;
  }
#endif
  if (getProcIdByName(API_PROC_NAME) < 0)
  {
    // Auto start API
    pid_t pid = fork();

    if (pid == -1) // On error ...
    {
      cout << "\tFork failed" << endl;
      exit(1);
    }
    else if (pid > 0) // For parent process ...
    {
      cout << "\tAPI process id: " << pid << endl;

      delay(500);
    }
    else // For the child process ...
    {
      cout << "\tStarting API" << endl;
      int res = execl(API_PATH, API_PROC_NAME, BROKER_HOST, (char*) 0);

      if (res < 0)
      {
        cout << "\tExec failed with: " << strerror(errno) << endl;
        exit(1);
      }
      else exit(0);
    }
  }
#endif

  // Create socket
  sock = socket(AF_INET, SOCK_STREAM, 0);

  if (sock < 0)
  {
    cout << "Socket could not be created" << endl;
    exit(1);
  }

  // Initialize server data
  server = gethostbyname(HOST);

  if (server == NULL)
  {
    cout << "Host could not be resolved" << endl;
    exit(1);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
  serv_addr.sin_port = htons(PORT);

  // Connect to proxy
  if (connect(sock, (sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    cout << "Error while connecting" << endl;
    exit(1);
  }

  // Init event socket (Vom Galileo nicht unterstützt)
  //eventSock = eventfd(0, EFD_NONBLOCK);

  // Start listening thread
  pthread_create(&listenThread, NULL, MqttClient::Listen, this);
}

MqttClient::~MqttClient()
{
  // Disconnect from proxy
  close(sock);

  listening = false;
  pthread_join(listenThread, NULL);
  // vom Galileo nicht unterstützt
  //eventfd_write(eventSock, (eventfd_t) 1);
}

void MqttClient::SendData(std::string data)
{
#ifdef DBG
  Serial.println((std::string("Sent: ") + data).c_str());
#endif

  uint32_t const size = data.length();
  data = std::string((char const *) &size, sizeof(size)) + data;

  send(sock, data.c_str(), data.length(), 0);
}

void MqttClient::Publish(std::string topic, std::string message)
{
  // Construct data to send to proxy
  std::string const data = "p" + topic + " " + message;

  // Send data to proxy
  SendData(data);
}

void MqttClient::Subscribe(std::string topic)
{
  // Constrcuct data to send to proxy
  std::string const data = "s" + topic;

  // Send to proxy
  SendData(data);
}

void * MqttClient::Listen(void * c)
{
  MqttClient * caller = (MqttClient *) c;
  char * const buf = new char[READ_BUFFER_SIZE];
  //int res;
  //fd_set readset;

  while (caller->listening)
  {
    /* Vom Galileo nicht unterstützt
    FD_ZERO(&readset);
    FD_SET(sock, &readset);
    FD_SET(eventSock, &readset);

    res = select(sock + 1, &readset);

    if(res > 0)
    {
      if(FD_ISSET(sock, &readset))
      {
        //TODO Daten lesen
      }

      if(FD_ISSET(eventSock, &readset))
      {
        eventfd_t d;
        eventfd_read(eventSock, &d);
      }
    }
    //*/

    std::string received;

    while (true)
    {
      size_t const recBytes = recv(caller->sock, buf, READ_BUFFER_SIZE, 0);

      if (recBytes > 0)
      {
        received.append(buf, recBytes);

        if (recBytes < READ_BUFFER_SIZE) break;
      }
      else if (recBytes < 0)
      {
        if (errno != EAGAIN) cout << "Error while reading data from socket" << endl;
        received.clear();
        break;
      }
      else
      {
        cout << "Read buffer empty" << endl;
        cout << "API disconnected" << endl;
        break;
      }
    }

#ifdef DBG
    Serial.println("Message received: ");
    Serial.println(received.c_str());
#endif

    // If any data was received ...
    if (!received.empty())
    {
      // While more messages are received ...
      while (received.length() > sizeof(uint32_t))
      {
        uint32_t size;

        memcpy(&size, received.c_str(), sizeof(uint32_t));

        // If message complete ...
        if (received.length() >= size + sizeof(uint32_t))
        {
          std::string msg = received.substr(sizeof(uint32_t), size);

          // Use the message
          if (msg[0] == 'm')
          {
            size_t spacePos = msg.find(' ');

            if (spacePos != std::string::npos)
            {
              // Call callback for received message
              caller->MessageReceivedCB(msg.substr(1, spacePos - 1), msg.substr(spacePos + 1));
            }
            else
            {
              Serial.println("Received message corrupted");
              received.clear();
            }
          }
          /*
          else
          {
            Serial.println("Received data unprocessable");
            Serial.print("Data: ");
            Serial.println(received.c_str());
            received.clear();
          }
          //*/

          received = received.substr(size + sizeof(uint32_t));
        }
        else break;
      }
    }
  }
}

//// !!!!! AB HIER KOMMEN DIE FUNKTIONEN, DIE IHR EDITIEREN SOLLT !!!!!

//pins and global vars

int stp = 2;
int dir = 1;
int hall = 3;
int greenLED = 5;
int redLED = 4;
bool isEdge;
int dirval = LOW; 
int button = 8;
int disable = 7;

boolean manuellOperation = false;

int hallVal;
int hcount = 0;
unsigned long elapsed;

enum DoorStatus {
  CLOSED = 0,
  OPEN = 1
};

DoorStatus doorState; //curent state of the door
DoorStatus nextState; // next state, if someone requests a change

void MqttClient::MessageReceivedCB(std::string topic, std::string message)
{
  if(topic == "sdo/state" && !manuellOperation)
  {
    nextState = (message == "0") ? CLOSED : OPEN;
  }
  
  Serial.print("Message");
  Serial.println(message.c_str());
}

MqttClient mqttClient;

void setup()
{
  pinMode(dir, OUTPUT);
  pinMode(stp, OUTPUT);
  pinMode(hall,INPUT_PULLUP);
  digitalWrite(dir, dirval);
  pinMode(button, INPUT_PULLUP);
  pinMode(disable, OUTPUT);
  //pinMode(13, OUTPUT);
  
  pinMode(greenLED, OUTPUT);
  pinMode(redLED, OUTPUT);
  
  digitalWrite(disable, HIGH);
  isEdge = false;
  doorState = CLOSED;
  
  elapsed = millis();
  
  Serial.begin(9600);
  
  Serial.println("Program started");
  
  mqttClient.Subscribe("sdo/state");
  
  Serial.println("Subscribed");
  
  mqttClient.Publish("sdo/state", "0");
  
}

void lock()
{
  //enable stepper
  digitalWrite(disable, LOW);
  elapsed = millis();
  
  isEdge = true;
  
  //counter for LED
  int counter = 0;
  for(;;)
  {
    //finish after 5 half turns
   if (hcount == 5)
   {
     //set attributes for next round and currnt states + disable stepper
     hcount = 0;
     dirval = (dirval == HIGH) ? LOW : HIGH;
     doorState = (doorState == OPEN) ? CLOSED : OPEN;
     digitalWrite(disable, HIGH);
     digitalWrite(dir, dirval);
     Serial.println("ready");
     delay(5);
     return;    
   }
     
     bool isStarting = ((millis() - elapsed) <= 50);
     
     //turn
     if(hcount < 5)
     {
        //one step on edge
        delayMicroseconds(10);
        digitalWrite(stp, HIGH);
        delayMicroseconds(10);
        digitalWrite(stp, LOW);
        
        if(!isStarting)
        { 
          //process hall sensor data   
          hallVal = digitalRead(hall);
            
          if(hallVal == LOW && isEdge)
          {
            ++hcount;
            Serial.println("first touch of hall sensor");
            isEdge = false;
          }
          else if(hallVal == HIGH)
          {
            //Serial.println("last touch of hall sensor");
            isEdge = true;
          }
        }
     }
     //green flash during opening
     if((nextState==OPEN) && (counter == 0 || counter == 200))
     {
       digitalWrite(greenLED, HIGH);
       counter = 0;
     }
     //red flash during closing
     else if(nextState==CLOSED && (counter == 0 || counter == 200))
     {
       digitalWrite(redLED, HIGH);
       counter = 0;
     }
     ++counter;
     
     // Important delay: regulates power + speed
     delay(2);
     
     //led flashing
     if(counter == 100)
     {
       digitalWrite(greenLED, LOW);
       digitalWrite(redLED, LOW);
     }
   }
   
   isEdge = false;
}

int manuellHall = 0;
int counter = 0;

void loop()
{

  //process hall sensor data   
  hallVal = digitalRead(hall);
    
  if(hallVal == LOW && isEdge) {
    manuellOperation = true;
    manuellHall++;
    isEdge = false;
  }
  else if(hallVal == HIGH) {
    isEdge = true;
  }
  
  if(manuellHall >= 4) {
   manuellOperation = false;
   manuellHall = 0;
   dirval = (dirval == HIGH) ? LOW : HIGH;
   doorState = (doorState == OPEN) ? CLOSED : OPEN;
   nextState = doorState;
   if(doorState == OPEN) {
     mqttClient.Publish("sdo/state", "1");
   }
   else {
     mqttClient.Publish("sdo/state", "0");
   }
   
   digitalWrite(disable, HIGH);
   digitalWrite(dir, dirval);
  }
  
  
  
  if(manuellOperation) {
    counter++;
        //led flashing
     if(counter == 100)
     {
       digitalWrite(greenLED, LOW);
       digitalWrite(redLED, LOW);
     } 
     
             //led flashing
     if(counter == 200)
     {
       digitalWrite(greenLED, HIGH);
       digitalWrite(redLED, HIGH);
     } 
     if(counter > 300) {
       counter = 0; 
     }
  }
  
  // do we need to un-/lock ?
  if(doorState != nextState && !manuellOperation)
  {
    digitalWrite(greenLED, LOW);
    digitalWrite(redLED, LOW);
    lock();
  }
  //green light while open
  if(doorState == OPEN && !manuellOperation)
  {
    digitalWrite(greenLED, HIGH);
    digitalWrite(redLED, LOW);
  }
  //red light while closed
  else if(!manuellOperation)
  {
    digitalWrite(greenLED, LOW);
    digitalWrite(redLED, HIGH);
  }
  
  //manual un-/lock with button
  if(digitalRead(button) == LOW && !manuellOperation)
  {
    while(digitalRead(button) == LOW);
    if(doorState == OPEN) {
       mqttClient.Publish("sdo/state", "0");
     }
     else {
       mqttClient.Publish("sdo/state", "1");
     }
  }
    
  delay(1);
}
